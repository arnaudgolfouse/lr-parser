//pub mod ast;
mod from_raw_to_typed_ast;

#[path = "EXPAND.rs"]
mod inner;

pub use self::{from_raw_to_typed_ast::AstError, inner::*};

#[cfg(test)]
mod tests {
    use super::*;
    use lr_parser::parser::TokenKind;

    #[test]
    fn raw_string_literal() {
        let source = r###"
no_hash: r"hello world",
one_hash: r#""(?:[^"\\]|\\.)*""#,
two_hash: r##" raw_str = r#"s"# "##,
"###;

        let tokens = Tokenizer::new(source)
            .filter_map(|(token, _)| match token {
                TokenKind::Skip(_) => None,
                _ => Some(token),
            })
            .collect::<Vec<_>>();
        assert_eq!(
            &tokens,
            &[
                TokenKind::Normal(Token::ID),
                TokenKind::Normal(Token::COLON),
                TokenKind::Normal(Token::STRING),
                TokenKind::Normal(Token::COMMA),
                TokenKind::Normal(Token::ID),
                TokenKind::Normal(Token::COLON),
                TokenKind::Normal(Token::STRING),
                TokenKind::Normal(Token::COMMA),
                TokenKind::Normal(Token::ID),
                TokenKind::Normal(Token::COLON),
                TokenKind::Normal(Token::STRING),
                TokenKind::Normal(Token::COMMA),
            ]
        )
    }

    #[test]
    fn raw_identifiers() {
        let source = r#"
id1
r#id2
r##notid3
        "#;

        let tokens = Tokenizer::new(source)
            .filter_map(|(token, _)| match token {
                TokenKind::Skip(_) => None,
                _ => Some(token),
            })
            .collect::<Vec<_>>();
        assert_eq!(
            &tokens,
            &[
                TokenKind::Normal(Token::ID), // id1
                TokenKind::Normal(Token::ID), // r#id2
                TokenKind::Error,             // r##
                TokenKind::Normal(Token::ID), // notid3
            ]
        )
    }
}
