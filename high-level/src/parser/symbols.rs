use std::fmt;

use logos::{Lexer, Logos};
use lr_parser::parser::{RemappedToken, TokenKind};

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Logos)]
#[allow(clippy::enum_variant_names)]
pub enum Token {
    /// An unknown token
    #[error]
    Unknown,
    /// `// ...`
    #[regex("//[^\n]*")]
    Comment,
    /// `/* ... */`
    #[regex(r"/\*", Token::multiline_comment)]
    MultilineComment,
    /// Any whitespace repetition
    #[regex(r"[\s]+")]
    Whitespace,
    /// A doc comment
    #[regex(r"///[^\n]*")]
    DocComment,
    /// `include`
    #[token("include")]
    IncludeKw,
    /// `tokens`
    #[token("tokens")]
    TokensKw,
    /// `regex`
    #[token("regex")]
    RegexKw,
    /// `external`
    #[token("external")]
    ExternalKw,
    /// `verbatim`
    #[token("verbatim")]
    VerbatimKw,
    /// `node`
    #[token("node")]
    NodeKw,
    /// `test`
    #[token("test")]
    TestKw,
    /// `conflict`
    #[token("conflict")]
    ConflictKw,
    /// `prefer`
    #[token("prefer")]
    PreferKw,
    /// `over`
    #[token("over")]
    OverKw,
    /// `lookahead`
    #[token("lookahead")]
    LookaheadKw,
    /// `shift`
    #[token("shift")]
    ShiftKw,
    /// `reduce`
    #[token("reduce")]
    ReduceKw,
    /// `;`
    #[token(";")]
    SemiColon,
    /// `:`
    #[token(":")]
    Colon,
    /// `::`
    #[token("::")]
    ColonColon,
    /// `,`
    #[token(",")]
    Comma,
    /// `*`
    #[token("*")]
    Star,
    /// `*_`
    #[token("*_")]
    StarUnderscore,
    /// `_*`
    #[token("_*")]
    UnderscoreStar,
    /// `+`
    #[token("+")]
    Plus,
    /// `+_`
    #[token("+_")]
    PlusUnderscore,
    /// `_+`
    #[token("_+")]
    UnderscorePlus,
    /// `?`
    #[token("?")]
    Question,
    /// `#`
    #[token("#")]
    Sharp,
    /// `(`
    #[token("(")]
    LPar,
    /// `)`
    #[token(")")]
    RPar,
    /// `[`
    #[token("[")]
    LBracket,
    /// `]`
    #[token("]")]
    RBracket,
    /// `{`
    #[token("{")]
    LBrace,
    /// `}`
    #[token("}")]
    RBrace,
    /// `|`
    #[token("|")]
    Bar,
    /// `=`
    #[token("=")]
    Equal,
    /// `=>`
    #[token("=>")]
    Arrow,
    /// `_`
    #[token("_")]
    Underscore,
    /// Identifier
    ///
    /// Can begin with, but not be only `_`.
    ///
    /// # Raw identifiers
    ///
    /// You can use a keyword by prefixing it with `r#`:
    /// ```text
    /// node R {
    ///     r#node: /* ... */
    /// }
    /// ```
    #[regex("([a-zA-Z_][0-9a-zA-Z_]*|r#[a-zA-Z_][0-9a-zA-Z_]*)")]
    Id,
    /// Single-quoted string : `'...'`
    #[regex(r"'(?:[^'\\]|\\.)*'")]
    TokenStr,
    /// Double-quoted string : `"..."`
    ///
    /// You can also use raw string literals: `r##"..."##`
    #[regex(r#"("(?:[^"\\]|\\.)*"|r#*")"#, Token::raw_string_literal)]
    String,
    /// Integer
    ///
    /// Can contain `_`, except in the first position.
    #[regex("[0-9][0-9_]*")]
    Int,
}

impl From<Token> for &'static str {
    fn from(token: Token) -> Self {
        match token {
            Token::Unknown => "UNKNOWN",
            Token::Comment => "COMMENT",
            Token::MultilineComment => "COMMENT",
            Token::Whitespace => "WHITESPACE",
            Token::DocComment => "DOC_COMMENT",
            Token::IncludeKw => "include",
            Token::TokensKw => "tokens",
            Token::RegexKw => "regex",
            Token::ExternalKw => "external",
            Token::VerbatimKw => "verbatim",
            Token::NodeKw => "node",
            Token::TestKw => "test",
            Token::ConflictKw => "conflict",
            Token::PreferKw => "prefer",
            Token::OverKw => "over",
            Token::LookaheadKw => "lookahead",
            Token::ShiftKw => "shift",
            Token::ReduceKw => "reduce",
            Token::SemiColon => ";",
            Token::Colon => ":",
            Token::ColonColon => "::",
            Token::Comma => ",",
            Token::Star => "*",
            Token::StarUnderscore => "*_",
            Token::UnderscoreStar => "_*",
            Token::Plus => "+",
            Token::PlusUnderscore => "+_",
            Token::UnderscorePlus => "_+",
            Token::Question => "?",
            Token::Sharp => "#",
            Token::LPar => "(",
            Token::RPar => ")",
            Token::LBracket => "[",
            Token::RBracket => "]",
            Token::LBrace => "{",
            Token::RBrace => "}",
            Token::Bar => "|",
            Token::Equal => "=",
            Token::Arrow => "=>",
            Token::Underscore => "_",
            Token::Id => "ID",
            Token::TokenStr => "TOKEN_STR",
            Token::String => "STRING",
            Token::Int => "INT",
        }
    }
}

impl fmt::Display for Token {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str(<&'static str>::from(*self))
    }
}

impl Token {
    pub fn kind(self) -> TokenKind<Self> {
        match self {
            Token::Comment | Token::Whitespace | Token::MultilineComment => TokenKind::Skip(self),
            Token::ERROR => TokenKind::Error,
            _ => TokenKind::Normal(self),
        }
    }

    /// Token remapping
    pub fn remap(self) -> RemappedToken<'static, Self> {
        match self {
            Token::RegexKw
            | Token::ExternalKw
            | Token::VerbatimKw
            | Token::PreferKw
            | Token::OverKw
            | Token::LookaheadKw
            | Token::ShiftKw
            | Token::ReduceKw => RemappedToken::Transform(Self::Id),
            _ => RemappedToken::None,
        }
    }

    fn raw_string_literal(lexer: &mut Lexer<Self>) -> Result<(), ()> {
        if !lexer.slice().starts_with('r') {
            return Ok(());
        }
        let hash_len = lexer.slice().len().saturating_sub(2);
        let pattern = String::from("\"") + &"#".repeat(hash_len);
        if let Some(end) = lexer.remainder().find(&pattern) {
            lexer.bump(end + hash_len + 1);
            Ok(())
        } else {
            lexer.bump(lexer.remainder().len());
            Err(())
        }
    }

    fn multiline_comment(lexer: &mut Lexer<Self>) -> Result<(), ()> {
        let mut level = 1;
        let mut chars_indices = lexer.remainder().char_indices().peekable();
        while let Some((_, c)) = chars_indices.next() {
            match c {
                '/' => {
                    if let Some((_, '*')) = chars_indices.peek() {
                        chars_indices.next();
                        level += 1;
                    }
                }
                '*' => {
                    if let Some((pos, '/')) = chars_indices.peek().copied() {
                        chars_indices.next();
                        level -= 1;
                        if level == 0 {
                            lexer.bump(pos + 1);
                            return Ok(());
                        }
                    }
                }
                _ => {}
            }
        }
        lexer.bump(lexer.remainder().len());
        Err(())
    }
}

#[repr(u8)]
#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Node {
    // Nodes listed in `grammar`
    /// Starting node
    File,
    TopLevelItem,
    Token,
    TokenDef,
    Block,
    Variant,
    Fields,
    Field,
    Label,
    Item,
    Primary,
    Alternative,
    Modifier,
    Attribute,
    NormalAttribute,
    AttributeItem,
    AttributeArgs,
    Literal,
    TestValue,
    TestField,
    TestItem,
    Rule,
    Conflict,
    ConflictAction,
    IdOrTokenStr,

    // Technicalities: repetition, optionals, ...
    /// `TopLevelItem*`
    TopLevelItemRepeat,
    /// `Attribute*`
    AttributeRepeat,
    /// `(Token ',')*_`
    TokenTrailing,
    /// `('verbatim' STRING)?`
    VerbatimOpt,
    /// `Field*`
    FieldRepeat,
    /// `('|' Variant)+`
    VariantPlus,
    /// `Item+`
    ItemPlus,
    /// `(Alternative '|')_*`
    AlternativeBarSeparate,
    /// `('|' Alternative)*`
    BarAlternativeRepeat,
    /// `Modifier?`
    ModifierOpt,
    /// `(AttributeItem ',')+_`
    AttributeItemPlusTrailing,
    /// `(AttributeItem ',')*_`
    AttributeItemTrailing,
    /// `TestField*`
    TestFieldRepeat,
    /// `TestItem*`
    TestItemRepeat,
    /// `('lookahead' ':' IdOrTokenStr)?`
    LookaheadOpt,
}

impl Node {
    pub fn silent(self) -> bool {
        self as u8 >= Self::TopLevelItemRepeat as u8
    }
}

impl fmt::Display for Node {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let s = match self {
            Node::TopLevelItemRepeat => "TopLevelItem*",
            Node::AttributeRepeat => "Attribute*",
            Node::TokenTrailing => "(Token ',')*_",
            Node::VerbatimOpt => "('verbatim' STRING)?",
            Node::FieldRepeat => "Field*",
            Node::VariantPlus => "('|' Variant)+",
            Node::ItemPlus => "Item+",
            Node::AlternativeBarSeparate => "(Alternative '|')_*",
            Node::BarAlternativeRepeat => "('|' Alternative)*",
            Node::ModifierOpt => "Modifier?",
            Node::AttributeItemPlusTrailing => "(AttributeItem ',')+_",
            Node::AttributeItemTrailing => "(AttributeItem ',')*_",
            Node::TestFieldRepeat => "TestField*",
            Node::TestItemRepeat => "TestItem*",
            Node::LookaheadOpt => "('lookahead' ':' IdOrTokenStr)?",
            _ => "",
        };
        if s.is_empty() {
            fmt::Debug::fmt(self, f)
        } else {
            f.write_str(s)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn raw_string_literal() {
        let source = r###"
no_hash: r"hello world",
one_hash: r#""(?:[^"\\]|\\.)*""#,
two_hash: r##" raw_str = r#"s"# "##,
        "###;

        let tokens: Vec<Token> = Token::lexer(source)
            .into_iter()
            .filter(|t| !matches!(t.kind(), TokenKind::Skip(_)))
            .collect();
        assert_eq!(
            &tokens,
            &[
                Token::Id,
                Token::Colon,
                Token::String,
                Token::Comma,
                Token::Id,
                Token::Colon,
                Token::String,
                Token::Comma,
                Token::Id,
                Token::Colon,
                Token::String,
                Token::Comma,
            ]
        )
    }

    #[test]
    fn raw_identifiers() {
        let source = r#"
id1
r#id2
r##notid3
        "#;

        let tokens: Vec<Token> = Token::lexer(source)
            .into_iter()
            .filter(|t| !matches!(t.kind(), TokenKind::Skip(_)))
            .collect();
        assert_eq!(
            &tokens,
            &[
                Token::Id,      // id1
                Token::Id,      // r#id2
                Token::Unknown, // r##
                Token::Id,      // notid3
            ]
        )
    }
}
