use lr_parser::{syntax_tree::SyntaxElement, text_ref::TextRc};
use std::{fmt, hash::Hash, ops::Deref, sync::Arc};

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Ast<T> {
    /// Reference in the [`SyntaxTree`](lr_parser::syntax_tree::SyntaxElement).
    pub element: Arc<SyntaxElement<super::Token, super::Node, TextRc>>,
    pub item: T,
}

impl<T> Deref for Ast<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        &self.item
    }
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct File {
    pub top_level_items: Vec<Ast<TopLevelItem>>,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum TopLevelItem {
    Include {
        path: Ast<String>,
    },
    Tokens {
        attributes: Vec<Ast<Attribute>>,
        tokens: Vec<Ast<Token>>,
        verbatim: Option<(Ast<()>, Ast<String>)>,
    },
    Node {
        attributes: Vec<Ast<Attribute>>,
        name: Ast<String>,
        block: Ast<Block>,
    },
    Test {
        attributes: Vec<Ast<Attribute>>,
        source: Ast<String>,
        value: Ast<TestValue>,
    },
    Conflict(Ast<Conflict>),
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Token {
    pub attributes: Vec<Ast<Attribute>>,
    pub name: Ast<String>,
    pub definition: Ast<TokenDef>,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum TokenDef {
    Raw(Ast<String>),
    Regex(Ast<String>),
    External,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Block {
    Single(Ast<Fields>),
    Multiple(Vec<Ast<Variant>>),
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Variant {
    pub attributes: Vec<Ast<Attribute>>,
    pub name: Ast<String>,
    pub fields: Ast<Fields>,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Fields(pub Vec<Ast<Field>>);

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Field {
    Anonymous {
        attributes: Vec<Ast<Attribute>>,
        item: Ast<Item>,
    },
    Named {
        attributes: Vec<Ast<Attribute>>,
        name: Ast<Label>,
        item: Ast<Item>,
    },
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Label {
    Id(Ast<String>),
    Number(Ast<u32>),
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Item {
    pub primary: Ast<Primary>,
    pub modifier: Option<Ast<Modifier>>,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Primary {
    Ident(Ast<String>),
    TokenStr(Ast<String>),
    Parent(Vec<Ast<Alternative>>),
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Alternative(pub Vec<Ast<Item>>);

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Modifier {
    Repeat,
    Trailing,
    Separate,
    Plus,
    PlusTrailing,
    PlusSeparate,
    Optional,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Attribute {
    Doc(Ast<String>),
    Normal(Ast<NormalAttribute>),
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct NormalAttribute {
    pub attribute_list: Vec<Ast<AttributeItem>>,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum AttributeItem {
    Id(Ast<String>),
    Literal(Ast<Literal>),
    Function {
        name: Ast<String>,
        args: Ast<AttributeArgs>,
    },
    Equal(Ast<String>, Ast<Literal>),
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct AttributeArgs(pub Vec<Ast<AttributeItem>>);

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Literal {
    Int(Ast<u32>),
    String(Ast<String>),
    TokenStr(Ast<String>),
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct TestValue {
    pub rule_name: Ast<Rule>,
    pub fields: Vec<Ast<TestField>>,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct TestField {
    pub attributes: Vec<Ast<Attribute>>,
    pub name: Ast<Label>,
    pub item: Ast<TestItem>,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum TestItem {
    Token(Ast<String>),
    Node(Ast<TestValue>),
    Compound(Vec<Ast<TestItem>>),
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Rule {
    Node(Ast<String>),
    Variant {
        base: Ast<String>,
        variant: Ast<String>,
    },
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Conflict {
    pub prefer: Ast<ConflictAction>,
    pub over: Ast<ConflictAction>,
    pub lookahead: Option<(Ast<()>, Ast<()>, Ast<IdOrTokenStr>)>,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum ConflictAction {
    ShiftAny,
    ShiftToken { token: Ast<IdOrTokenStr> },
    Reduce(Ast<Rule>),
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum IdOrTokenStr {
    Id(Ast<String>),
    TokenStr(Ast<String>),
}

impl fmt::Display for Modifier {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(match self {
            Modifier::Repeat => "*",
            Modifier::Trailing => "*_",
            Modifier::Separate => "_*",
            Modifier::Plus => "+",
            Modifier::PlusTrailing => "+_",
            Modifier::PlusSeparate => "_+",
            Modifier::Optional => "?",
        })
    }
}
