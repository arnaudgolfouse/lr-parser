pub mod ast;
mod from_raw_to_typed_ast;
mod symbols;

pub use self::{
    from_raw_to_typed_ast::AstError,
    symbols::{Node, Token},
};
use lr_parser::{grammar::RuntimeGrammar, parser::TokenKind, text_ref::Span};

/// Wrapper around a [`logos`] lexer.
///
/// This is an iterator that return ([`TokenKind<T>`], [`Span`]), where the `Span` is
/// the span of the token in the source code.
pub struct Tokenizer<'source> {
    lexer: logos::Lexer<'source, Token>,
}

impl<'source> Tokenizer<'source> {
    pub fn new(source: &'source str) -> Self {
        use logos::Logos;
        Self {
            lexer: Token::lexer(source),
        }
    }
}

impl<'source> Iterator for Tokenizer<'source> {
    type Item = (TokenKind<Token>, Span);

    fn next(&mut self) -> Option<Self::Item> {
        let next_token = self.lexer.next()?;
        let span = self.lexer.span();
        Some((next_token.kind(), span.start..span.end))
    }
}

#[must_use]
pub fn get_grammar() -> RuntimeGrammar<Token, Node> {
    use self::Token::*;
    use Node::*;

    lr_parser::make_grammar!(
        File -> { N(TopLevelItemRepeat) }
        TopLevelItemRepeat -> {} | { N(TopLevelItem) N(TopLevelItemRepeat) }
        TopLevelItem ->
            { IncludeKw String SemiColon }
            | { N(AttributeRepeat) TokensKw LBrace N(TokenTrailing) N(VerbatimOpt) RBrace }
            | { N(AttributeRepeat) NodeKw Id LBrace N(Block) RBrace }
            | { N(AttributeRepeat) TestKw String N(TestValue) }
            | { N(Conflict) }
        TokenTrailing -> {}
            | { N(Token) }
            | { N(Token) Comma N(TokenTrailing) }
        VerbatimOpt -> {} | { VerbatimKw String }
        Token -> { N(AttributeRepeat) Id Colon N(TokenDef) }
        TokenDef -> { TokenStr } | { RegexKw LPar String RPar } | { ExternalKw }
        Block -> { N(Fields) }
            | { N(VariantPlus) }
        VariantPlus -> { N(Variant) }
            | { N(Variant) N(VariantPlus) }
        Variant -> { Bar N(AttributeRepeat) Id Arrow N(Fields) }
        Fields -> { N(FieldRepeat) }
        FieldRepeat -> {} | { N(Field) N(FieldRepeat) }
        Field -> { N(AttributeRepeat) N(Item) }
            | { N(AttributeRepeat) N(Label) Colon N(Item) }
        Label -> { Id } | { Int }
        ItemPlus -> { N(Item) } | { N(Item) N(ItemPlus) }
        Item -> { N(Primary) N(ModifierOpt) }
        Primary -> { Id } | { TokenStr } | { LPar N(AlternativeBarSeparate) RPar }
        AlternativeBarSeparate -> {} | { N(Alternative) N(BarAlternativeRepeat) }
        BarAlternativeRepeat -> {} | { Bar N(Alternative) N(BarAlternativeRepeat) }
        Alternative -> { N(ItemPlus) }
        ModifierOpt -> {} | { N(Modifier) }
        Modifier -> { Star } | { Plus } | { StarUnderscore } | { UnderscoreStar } | { PlusUnderscore } | { UnderscorePlus } | { Question }
        AttributeRepeat ->  {} | { N(Attribute) N(AttributeRepeat) }
        Attribute -> { DocComment } | { N(NormalAttribute) }
        NormalAttribute -> { Sharp LBracket N(AttributeItemPlusTrailing) RBracket }
        AttributeItemTrailing -> {}
            | { N(AttributeItem) }
            | { N(AttributeItem) Comma N(AttributeItemTrailing) }
        AttributeItemPlusTrailing -> { N(AttributeItem) }
            | { N(AttributeItem) Comma }
            | { N(AttributeItem) Comma N(AttributeItemPlusTrailing) }
        AttributeItem -> { Id }
            | { N(Literal) }
            | { Id LPar N(AttributeArgs) RPar }
            | { Id Equal N(Literal) }
        Literal -> { Int } | { String } | { TokenStr }
        AttributeArgs -> { N(AttributeItemTrailing) }
        TestValue -> { N(Rule) LBrace N(TestFieldRepeat) RBrace }
        TestFieldRepeat -> {} | { N(TestField) N(TestFieldRepeat) }
        TestField -> { N(AttributeRepeat) N(Label) Colon N(TestItem) }
        TestItem -> { String } | { N(TestValue) }
            | { LBracket N(TestItemRepeat) RBracket }
        TestItemRepeat -> {} | { N(TestItem) N(TestItemRepeat) }
        Rule -> { Id } | { Id ColonColon Id }
        Conflict -> {
            ConflictKw LBrace
            PreferKw Colon N(ConflictAction)
            OverKw Colon N(ConflictAction)
            N(LookaheadOpt)
            RBrace
        }
        ConflictAction -> { ShiftKw LPar Underscore RPar }
        | { ShiftKw LPar N(IdOrTokenStr) RPar }
        | { ReduceKw LPar N(Rule) RPar }
        LookaheadOpt -> {} | { LookaheadKw Colon N(IdOrTokenStr) }
        IdOrTokenStr -> { Id } | { TokenStr }
    )
}
