//! Errors and warnings

use super::{FileId, GrammarFiles, NodeType, TokenType};
use crate::parser::{ast::Modifier, AstError, Node, Token};
use codespan_reporting::{
    diagnostic::{Diagnostic, Label},
    files::Files,
    term::{self, termcolor::WriteColor},
};
use lr_parser::{
    syntax_tree::{SyntaxElement, SyntaxError},
    text_ref::{TextRc, TextReference as _},
};
use std::path::PathBuf;

/// Combination of a file and an index in the corresponding [`SyntaxElement`].
#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct SyntaxTreeLocation {
    pub file: FileId,
    /// Element in the syntax tree corresponding to [`file`](Self::file).
    pub element: std::sync::Arc<SyntaxElement<Token, Node, TextRc>>,
}

impl SyntaxTreeLocation {
    pub fn span(&self) -> std::ops::Range<usize> {
        self.element.span()
    }

    fn primary_label(&self) -> Label<FileId> {
        Label::primary(self.file, self.span())
    }

    fn secondary_label(&self) -> Label<FileId> {
        Label::secondary(self.file, self.span())
    }
}

/// Errors that can appear while parsing the grammar file.
///
/// Use the [`display`](MappingError::display) method to get a printable
/// representation of the error.
#[derive(Debug)]
pub enum MappingError {
    /// Error while manipulating files.
    Io {
        /// `Some` if the file which triggered the error was included from another
        /// file.
        included_from: Option<SyntaxTreeLocation>,
        /// Path of the file which triggered the error.
        path: PathBuf,
        error: std::io::Error,
    },
    /// Errors while building the syntax tree.
    SyntaxErrors {
        /// `Some` if the file in which we found the errors was included from
        /// another file.
        included_from: Option<SyntaxTreeLocation>,
        /// File in which the errors occurred.
        file: FileId,
        /// All encountered syntax errors.
        errors: Vec<SyntaxError<Token, Node, TextRc>>,
    },
    /// Error while building the AST.
    AstError {
        /// `Some` if the file in which we found the error was included from
        /// another file.
        included_from: Option<SyntaxTreeLocation>,
        /// File in which the error occurred.
        file: FileId,
        /// Encountered AST error.
        error: AstError,
    },
    /// The file we tried to add was the same as the one corresponding to
    /// `already_present`.
    ///
    /// # Example
    /// ```text
    /// include "some_file";
    /// include "some_file";
    /// ```
    RedundantFile {
        /// First time the file was included.
        ///
        /// If `None`, the file is the root file.
        previously_included: Option<SyntaxTreeLocation>,
        /// Second time the file was included.
        included_from: SyntaxTreeLocation,
        /// ID of the file we tried to include multiple times.
        already_present: FileId,
    },
    /// Tried to redefine a name that was already taken by a token.
    AlreadyDefinedToken {
        token_definition: SyntaxTreeLocation,
        new_definition: SyntaxTreeLocation,
    },
    /// Tried to redefine a token expression.
    AlreadyDefinedTokenStr {
        old_definition: SyntaxTreeLocation,
        new_definition: SyntaxTreeLocation,
    },
    /// Tried to redefine a name that was already taken by a node.
    AlreadyDefinedNode {
        node_definition: SyntaxTreeLocation,
        new_definition: SyntaxTreeLocation,
    },
    /// An identifier was encountered, that did not correspond to any node or token.
    ///
    /// # Example
    /// ```text
    /// tokens {}
    /// #[start] node Start { UNKNOWN_TOKEN }
    ///                       ^^^^^^^^^^^^^
    /// ```
    UnknownIdent(SyntaxTreeLocation),
    /// The given identifier was not a token.
    /// # Example
    /// ```text
    /// conflict { prefer: shift(Node) over: reduce(OtherNode) }
    ///                          ^^^^
    /// ```
    UnknownToken(SyntaxTreeLocation),
    /// An unknown token expression was encountered.
    ///
    /// # Example
    /// ```text
    /// tokens {}
    ///
    /// #[start] node Start { '+' }
    ///                       ^^^
    /// ```
    UnknownTokenStr(SyntaxTreeLocation),
    /// A node variant was requested using `Node::Variant`, but it does not exists.
    ///
    /// # Example
    /// ```text
    /// conflict { prefer: shift('*') over: reduce(Node::UnknownVariant) }
    ///                                                  ^^^^^^^^^^^^^^
    /// ```
    UnknownNodeVariant {
        /// Location of the `Node::Variant` call.
        unknown: SyntaxTreeLocation,
        /// Location of the node.
        node: SyntaxTreeLocation,
    },
    /// A modifier was applied to an empty block.
    ///
    /// # Example
    /// ```text
    /// empty_block: ()*
    ///              ^^^
    /// ```
    EmptyModifier(SyntaxTreeLocation),
    /// A modifier was applied to a block with a single item (or a standalone item),
    /// but at least two were expected.
    ///
    /// # Example
    /// ```text
    /// missing_comma: (Arg)*_
    ///                ^^^^^
    /// ```
    SingleItemModifier {
        /// Location of the block with one item.
        location: SyntaxTreeLocation,
        /// The modifier that required at least two items.
        modifier: Modifier,
    },
    /// No node is marked with the `#[start]` attribute.
    NoStart,
    /// The number of nodes overflowed [`NodeType`].
    NodeOverflow,
    /// The number of tokens overflowed [`TokenType`].
    TokenOverflow,
    /// The number of variants in a node overflowed [`u32`].
    VariantOverflow {
        /// Location of the node
        node: SyntaxTreeLocation,
    },
    /// Two nodes are marked with `#[start]`.
    MultipleStart {
        old_node: SyntaxTreeLocation,
        new_node: SyntaxTreeLocation,
    },
    /// An attribute expected an identifier.
    ///
    /// # Example
    /// ```text
    /// #[callback(5)]
    ///            ^
    /// ```
    ExpectedId(SyntaxTreeLocation),
    /// Expected a simple token field, got something else (like a node, or an optional token, or a block...).
    ///
    /// # Example
    /// ```text
    /// #[prefix(0)] invalid: (Node '+')*
    ///                       ^^^^^^^^^^^
    /// ```
    FieldIsNotToken(SyntaxTreeLocation),
    /// An argument given to an attribute function was incorrect.
    ///
    /// # Example
    /// ```text
    /// tokens {
    ///     #[priority("incorrect")] RETURN_KW: 'return'
    ///                ^^^^^^^^^^^
    /// }
    /// ```
    IncorrectAttrArguments {
        function_name: &'static str,
        location: SyntaxTreeLocation,
        additional_msg: &'static str,
    },
    /// The attribute function required an argument, but none were given.
    AttrRequireArgument(&'static str, SyntaxTreeLocation),
    /// The second argument of `#[infix(...)]` was neither `left`, `right` or
    /// `none`.
    UnknownPrecedenceSide(SyntaxTreeLocation),
}

/// Used to display a [`MappingError`].
pub struct MappingErrorDisplay<'files> {
    /// Reference to the grammar's files.
    grammar_files: &'files GrammarFiles,
    /// Error to display.
    error: MappingError,
}

/// Error while writing a [`MappingErrorDisplay`].
#[derive(Debug)]
pub enum WriterError {
    Files(codespan_reporting::files::Error),
    Fmt(core::fmt::Error),
}

impl From<codespan_reporting::files::Error> for WriterError {
    fn from(err: codespan_reporting::files::Error) -> Self {
        Self::Files(err)
    }
}

impl From<core::fmt::Error> for WriterError {
    fn from(err: core::fmt::Error) -> Self {
        Self::Fmt(err)
    }
}

impl MappingError {
    /// Returns a representation of the error that can be used for pretty-printing.
    pub fn display(self, grammar_files: &GrammarFiles) -> MappingErrorDisplay {
        MappingErrorDisplay {
            grammar_files,
            error: self,
        }
    }
}

impl MappingErrorDisplay<'_> {
    /// Write the error on the given colored output.
    pub fn write_on(
        self,
        writer: &mut dyn WriteColor,
    ) -> Result<(), codespan_reporting::files::Error> {
        let term_config = term::Config::default();

        match self.error {
            MappingError::Io {
                included_from,
                path,
                error,
            } => {
                if let Some(included_from) = included_from {
                    let diagnostic = Diagnostic::error()
                        .with_message(format!(
                            "error while including file {}: {}",
                            path.display(),
                            error
                        ))
                        .with_labels(vec![included_from
                            .primary_label()
                            .with_message("included here")]);
                    term::emit(writer, &term_config, self.grammar_files, &diagnostic)
                } else {
                    write!(writer, "At {}: {}", path.display(), error).map_err(Into::into)
                }
            }
            MappingError::SyntaxErrors { file, errors, .. } => {
                for error in errors {
                    match error {
                        SyntaxError::IncompleteNode {
                            node,
                            text_ref,
                            expected,
                            ..
                        } => {
                            let mut labels = vec![Label::primary(file, text_ref.get_span())];
                            if !expected.is_empty() {
                                let mut expected_tokens = String::from("expected ");
                                for (idx, token) in expected.into_iter().enumerate() {
                                    if idx == 0 {
                                        expected_tokens.push_str(&format!("'{}'", token))
                                    } else {
                                        expected_tokens.push_str(&format!(", '{}'", token))
                                    }
                                }
                                labels.push(
                                    Label::secondary(file, text_ref.get_span())
                                        .with_message(expected_tokens),
                                )
                            }
                            term::emit(
                                writer,
                                &term_config,
                                self.grammar_files,
                                &Diagnostic::error()
                                    .with_message(format!("Incomplete node {}", node))
                                    .with_labels(labels),
                            )?
                        }
                        SyntaxError::Unexpected { token, expected } => {
                            use lr_parser::grammar::Lookahead;
                            let (token, span) = match token {
                                Lookahead::Eof => {
                                    let source = self.grammar_files.files_manager.source(file)?;
                                    (Lookahead::Eof, source.len()..source.len())
                                }
                                Lookahead::Token((token, text_ref)) => {
                                    (Lookahead::Token(token), text_ref.get_span())
                                }
                            };
                            let mut labels = vec![Label::primary(file, span.clone())];
                            if !expected.is_empty() {
                                let mut expected_tokens = String::from("expected ");
                                for (idx, token) in expected.into_iter().enumerate() {
                                    if idx == 0 {
                                        expected_tokens.push_str(&format!("'{}'", token))
                                    } else {
                                        expected_tokens.push_str(&format!(", '{}'", token))
                                    }
                                }
                                labels.push(
                                    Label::secondary(file, span).with_message(expected_tokens),
                                )
                            }
                            term::emit(
                                writer,
                                &term_config,
                                self.grammar_files,
                                &Diagnostic::error()
                                    .with_message(format!("Unexpected token: {}", token))
                                    .with_labels(labels),
                            )?
                        }
                    }
                }
                Ok(())
            }
            MappingError::AstError {
                included_from,
                file,
                error,
            } => {
                let mut labels = if let Some(location) = included_from {
                    vec![Label::secondary(location.file, location.span())
                        .with_message("file included here")]
                } else {
                    Vec::new()
                };
                let diagnostic = match error {
                    AstError::Conversion(conversion_error) => {
                        use lr_parser::syntax_tree::abstract_tree::EncounteredSymbol;
                        let display_error = format!(
                            "encountered {:?}, but expected {:?}",
                            match &conversion_error.encountered {
                                EncounteredSymbol::Normal(_, text_ref)
                                | EncounteredSymbol::Error(text_ref) => text_ref,
                                EncounteredSymbol::Eof => "EOF",
                            },
                            conversion_error.expected
                        );
                        Diagnostic::bug()
                            .with_message(format!("Unexpected error:\n{}", display_error))
                            .with_labels(labels)
                    }
                    AstError::InvalidRange(span) => Diagnostic::bug()
                        .with_message(format!(
                            "The range {:?} is invalid for the file {}",
                            span,
                            self.grammar_files.files_manager.name(file)?
                        ))
                        .with_labels(labels)
                        .with_notes(vec![String::from("This is a bug: please report this !")]),
                    AstError::ParseNumber(text_ref, error) => {
                        labels.push(
                            Label::primary(file, text_ref.get_span())
                                .with_message("error ocurred here"),
                        );
                        Diagnostic::error()
                            .with_message(format!("Error while parsing integer: {}", error))
                            .with_labels(labels)
                    }
                    AstError::InvalidEscape(text_ref, c) => {
                        labels.push(
                            Label::primary(file, text_ref.get_span())
                                .with_message("error ocurred here"),
                        );
                        Diagnostic::error()
                            .with_message(format!("'\\{}' is not a valid escape", c))
                            .with_labels(labels)
                    }
                    AstError::InvalidByteEscape(text_ref, c) => {
                        labels.push(
                            Label::primary(file, text_ref.get_span())
                                .with_message("error ocurred here"),
                        );
                        if let Some(c) = c {
                            Diagnostic::error()
                                .with_message(format!("'{}' is not an hexadecimal character", c))
                                .with_labels(labels)
                                .with_notes(vec![String::from(
                                    "Only hexadecimal characters are allowed in \\x**",
                                )])
                        } else {
                            Diagnostic::error()
                                .with_message("There need to be two numbers in a '\\x' escape")
                                .with_labels(labels)
                        }
                    }
                };
                term::emit(writer, &term_config, self.grammar_files, &diagnostic)
            }
            MappingError::RedundantFile {
                previously_included,
                included_from,
                already_present,
            } => {
                let mut labels = vec![included_from.primary_label().with_message("included here")];
                let is_root_file = previously_included.is_none();
                if let Some(previously_included) = previously_included {
                    labels.push(
                        Label::secondary(previously_included.file, previously_included.span())
                            .with_message("previously included here"),
                    )
                }
                let mut diagnostic = Diagnostic::error()
                    .with_message(format!(
                        "The file {} is included multiple times",
                        self.grammar_files.files_manager.name(already_present)?
                    ))
                    .with_labels(labels);
                if is_root_file {
                    diagnostic = diagnostic.with_notes(vec![format!(
                        "{} was previously included as the root file",
                        self.grammar_files.files_manager.name(already_present)?
                    )]);
                }
                term::emit(writer, &term_config, self.grammar_files, &diagnostic)
            }
            MappingError::AlreadyDefinedToken {
                token_definition,
                new_definition,
            } => {
                // FIXME: emphasis that the conflict is on the name
                let diagnostic = Diagnostic::error()
                    .with_message("Trying to redefine a token")
                    .with_labels(vec![
                        token_definition
                            .primary_label()
                            .with_message("new definition"),
                        new_definition
                            .secondary_label()
                            .with_message("already defined here"),
                    ]);
                term::emit(writer, &term_config, self.grammar_files, &diagnostic)
            }
            MappingError::AlreadyDefinedTokenStr {
                old_definition,
                new_definition,
            } => {
                // FIXME: emphasis that the conflict is on the expression
                let diagnostic = Diagnostic::error()
                    .with_message("Trying to redefine a token")
                    .with_labels(vec![
                        old_definition
                            .primary_label()
                            .with_message("new definition"),
                        new_definition
                            .secondary_label()
                            .with_message("already defined here"),
                    ]);
                term::emit(writer, &term_config, self.grammar_files, &diagnostic)
            }
            MappingError::AlreadyDefinedNode {
                node_definition,
                new_definition,
            } => {
                let diagnostic = Diagnostic::error()
                    .with_message("Trying to redefine a node")
                    .with_labels(vec![
                        new_definition
                            .primary_label()
                            .with_message("new definition"),
                        node_definition
                            .secondary_label()
                            .with_message("already defined here"),
                    ]);
                term::emit(writer, &term_config, self.grammar_files, &diagnostic)
            }
            MappingError::UnknownIdent(location) => {
                let diagnostic = Diagnostic::error()
                    .with_message("Unknown identifier")
                    .with_labels(vec![location.primary_label()]);
                term::emit(writer, &term_config, self.grammar_files, &diagnostic)
            }
            MappingError::UnknownToken(location) => {
                let diagnostic = Diagnostic::error()
                    .with_message("Unknown token")
                    .with_labels(vec![location.primary_label()]);
                term::emit(writer, &term_config, self.grammar_files, &diagnostic)
            }
            MappingError::UnknownTokenStr(location) => {
                let diagnostic = Diagnostic::error()
                    .with_message("Unknown token expression")
                    .with_labels(vec![location.primary_label()]);
                term::emit(writer, &term_config, self.grammar_files, &diagnostic)
            }
            MappingError::UnknownNodeVariant { unknown, node } => {
                let diagnostic = Diagnostic::error()
                    .with_message("Unknown node variant")
                    .with_labels(vec![
                        unknown.primary_label(),
                        node.secondary_label().with_message("node defined here"),
                    ]);
                term::emit(writer, &term_config, self.grammar_files, &diagnostic)
            }
            MappingError::EmptyModifier(location) => {
                let diagnostic = Diagnostic::error()
                    .with_message("A modifier cannot apply to an empty block")
                    .with_labels(vec![location
                        .primary_label()
                        .with_message("you need to put something in this block")]);
                term::emit(writer, &term_config, self.grammar_files, &diagnostic)
            }
            MappingError::SingleItemModifier { location, modifier } => {
                let diagnostic = Diagnostic::error()
                    .with_message(format!(
                        "The '{}' modifier needs a second element",
                        modifier
                    ))
                    .with_labels(vec![location.primary_label().with_message(
                        "This needs to be a parenthesized block with at least 2 elements",
                    )]);
                term::emit(writer, &term_config, self.grammar_files, &diagnostic)
            }
            MappingError::NoStart => write!(
                writer,
                "At least one node must be marked with the `#[start]` attribute"
            )
            .map_err(Into::into),
            MappingError::NodeOverflow => {
                write!(writer, "Too many nodes defined: max is {}", NodeType::MAX)
                    .map_err(Into::into)
            }
            MappingError::TokenOverflow => {
                write!(writer, "Too many tokens defined: max is {}", TokenType::MAX)
                    .map_err(Into::into)
            }
            MappingError::VariantOverflow { node } => {
                let diagnostic = Diagnostic::error()
                    .with_message("The number of variants overflowed")
                    .with_labels(vec![node.primary_label().with_message("defined here")])
                    .with_notes(vec![format!(
                        "The maximum number of variants for a given node is {}",
                        u32::MAX
                    )]);
                term::emit(writer, &term_config, self.grammar_files, &diagnostic)
            }
            MappingError::MultipleStart { old_node, new_node } => {
                let diagnostic = Diagnostic::error()
                    .with_message("Multiple nodes are marked with the `#[start]` attribute")
                    .with_labels(vec![
                        new_node
                            .primary_label()
                            .with_message("new definition of `#[start]`"),
                        old_node
                            .secondary_label()
                            .with_message("previous definition here"),
                    ]);
                term::emit(writer, &term_config, self.grammar_files, &diagnostic)
            }
            MappingError::ExpectedId(location) => {
                let diagnostic = Diagnostic::error()
                    .with_message("Incorrect argument")
                    .with_labels(vec![location
                        .primary_label()
                        .with_message("expected an identifier")]);
                term::emit(writer, &term_config, self.grammar_files, &diagnostic)
            }
            MappingError::FieldIsNotToken(location) => {
                let diagnostic = Diagnostic::error()
                    .with_message("Expected token")
                    .with_labels(vec![location
                        .primary_label()
                        .with_message("expected a single token here")]);
                term::emit(writer, &term_config, self.grammar_files, &diagnostic)
            }
            MappingError::IncorrectAttrArguments {
                function_name,
                location,
                additional_msg,
            } => {
                let diagnostic = Diagnostic::error()
                    .with_message(format!(
                        "Incorrect arguments for attribute function '{}'",
                        function_name
                    ))
                    .with_labels(vec![location.primary_label().with_message(additional_msg)]);
                term::emit(writer, &term_config, self.grammar_files, &diagnostic)
            }
            MappingError::AttrRequireArgument(function_name, location) => {
                let diagnostic = Diagnostic::error()
                    .with_message(format!(
                        "The attribute function '{}' requires at least one argument, but none were given",
                        function_name
                    ))
                    .with_labels(vec![location.primary_label()]);
                term::emit(writer, &term_config, self.grammar_files, &diagnostic)
            }
            MappingError::UnknownPrecedenceSide(location) => {
                let diagnostic = Diagnostic::error()
                    .with_message("Unknown precedence argument")
                    .with_labels(vec![location
                        .primary_label()
                        .with_message("accepted arguments are left, right or none")]);
                term::emit(writer, &term_config, self.grammar_files, &diagnostic)
            }
        }
    }
}

/// Some warning emitted while parsing the grammar file.
#[derive(Debug, Clone)]
pub enum Warning {
    /// The highlighted attribute is not recognized.
    UnknownAttribute(SyntaxTreeLocation),
    /// An attribute was ignored, with a reason.
    IgnoredAttribute(SyntaxTreeLocation, &'static str),
}

impl Warning {
    /// Returns a representation of the warning that can be used for
    /// pretty-printing.
    pub fn display(self, grammar_files: &GrammarFiles) -> WarningDisplay {
        WarningDisplay {
            grammar_files,
            warning: self,
        }
    }
}

/// Used to display a [`Warning`].
#[derive(Clone)]
pub struct WarningDisplay<'files> {
    /// Reference to the grammar's files.
    grammar_files: &'files GrammarFiles,
    /// Warning to display.
    warning: Warning,
}

impl WarningDisplay<'_> {
    /// Write the warning on the given colored output.
    pub fn write_on(
        self,
        writer: &mut dyn WriteColor,
    ) -> Result<(), codespan_reporting::files::Error> {
        let term_config = term::Config::default();

        let diagnostic = match self.warning {
            Warning::UnknownAttribute(location) => Diagnostic::warning()
                .with_message("Unknown attribute")
                .with_labels(vec![location.primary_label()]),
            Warning::IgnoredAttribute(location, reason) => Diagnostic::warning()
                .with_message(format!("Attribute is ignored: {}", reason))
                .with_labels(vec![location.primary_label()]),
        };
        term::emit(writer, &term_config, self.grammar_files, &diagnostic)
    }
}
