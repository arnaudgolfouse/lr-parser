use super::{error::SyntaxTreeLocation, NodeType, TokenType};
use crate::parser::ast::{self, Ast};
use lr_parser::grammar::RuleIdx;

/// Information attached to a node in the [`GrammarMap`](super::GrammarMap).
#[derive(Debug, Clone)]
pub struct NodeInMap {
    /// Index in the grammar.
    pub(crate) id: NodeType,
    /// Location of the node in the source code.
    pub(crate) location: SyntaxTreeLocation,
    /// Origin of the node: user-defined or internally generated.
    pub(crate) origin: NodeOrigin,
}

/// Origin of the node: user-defined or internally generated.
#[derive(Debug, Clone)]
pub(crate) enum NodeOrigin {
    /// Node explicitly defined by the user.
    UserDefined {
        /// Name of the node.
        name: Ast<String>,
        /// User-provided documentation for this node.
        ///
        /// Will `None` if no documentation was provided.
        documentation: Option<String>,
        /// `true` if the node is 'silent' (see the
        /// [silent nodes](lr_parser::LR1Parser#silent-nodes) section of
        /// `LR1Parser`).
        silent: bool,
        content: NodeContent,
    },
    /// Node created internally for the parser, to implement things like
    /// repetitions.
    Internal {
        /// Textual representation of the node.
        expression: String,
    },
}

impl NodeInMap {
    /// Returns the ID of this node.
    #[inline]
    pub fn id(&self) -> NodeType {
        self.id
    }

    /// Returns the location of the node in the source code.
    #[inline]
    pub fn location(&self) -> &SyntaxTreeLocation {
        &self.location
    }

    /// Name of the node if it is user-defined.
    ///
    /// The parser auto-generates some nodes: those will have no name.
    pub fn name(&self) -> Option<&str> {
        match &self.origin {
            NodeOrigin::UserDefined { name, .. } => Some(name.as_str()),
            NodeOrigin::Internal { .. } => None,
        }
    }

    /// Get the variant at `index` if the node is user-defined, has multiple
    /// variants and `index` is in bounds.
    pub fn variant(&self, index: u16) -> Option<&NodeVariant> {
        match &self.origin {
            NodeOrigin::UserDefined { content, .. } => match content {
                NodeContent::Single { .. } => None,
                NodeContent::Multiple { variants } => variants.get(index as usize),
            },
            NodeOrigin::Internal { .. } => None,
        }
    }

    /// Returns `true` if the node is silent.
    pub fn is_silent(&self) -> bool {
        match &self.origin {
            NodeOrigin::UserDefined { silent, .. } => *silent,
            NodeOrigin::Internal { .. } => true,
        }
    }

    /// Returns a printable representation of the node.
    ///
    /// This is either the node name if it is user-defined, or the expression used
    /// to define it.
    pub fn representation(&self) -> &str {
        match &self.origin {
            NodeOrigin::UserDefined { name, .. } => name.as_str(),
            NodeOrigin::Internal { expression } => expression.as_str(),
        }
    }

    /// Returns the user-provided documentation for this node.
    pub fn documentation(&self) -> Option<&str> {
        match &self.origin {
            NodeOrigin::UserDefined { documentation, .. } => documentation.as_deref(),
            NodeOrigin::Internal { .. } => None,
        }
    }

    /// Returns the content of the node if it is user-defined.
    pub fn content(&self) -> Option<&NodeContent> {
        match &self.origin {
            NodeOrigin::UserDefined { content, .. } => Some(content),
            NodeOrigin::Internal { .. } => None,
        }
    }
}

/// Representation of the content of the `node { ... }` block.
#[derive(Debug, Clone)]
pub enum NodeContent {
    /// A `node` block without alternatives.
    ///
    /// # Example
    /// ```text
    /// node SingleNode {
    ///     INT '+' ID
    /// }
    /// ```
    Single {
        /// Index of the single rule in the grammar.
        rule_idx: RuleIdx<NodeType>,
        /// Fields of the block.
        fields: Vec<Field>,
    },
    /// A `node` block with multiple alternatives.
    ///
    /// # Example
    /// ```text
    /// node MultipleNode {
    ///     | INT
    ///     | ID
    ///     | '+'
    /// }
    /// ```
    Multiple {
        /// Variants of the node.
        variants: Vec<NodeVariant>,
    },
}

#[derive(Debug, Clone)]
pub struct NodeVariant {
    /// Documentation of one of the variants.
    ///
    /// This is `None` is no documentation was found.
    pub documentation: Option<String>,
    /// Index of this variant in the grammar.
    pub rule_idx: RuleIdx<NodeType>,
    /// Name of this variant.
    pub name: Ast<String>,
    /// Fields of this variant.
    pub fields: Vec<Field>,
}

#[derive(Clone, Debug)]
pub struct Field {
    /// Reference of the field in a syntax tree.
    pub location: SyntaxTreeLocation,
    /// Documentation of the field.
    ///
    /// This is `None` is no documentation was found.
    pub documentation: Option<String>,
    /// Optional label of the field.
    ///
    /// # Example
    /// - `operation: '+'`: label is `Some(Label::Id("operation"))`
    /// - `0: INT`: label is `Some(Label::Number(0))`
    /// - `ID`: label is `None`
    pub label: Option<Ast<ast::Label>>,
    /// What this field is made of.
    pub item: Ast<FieldItem>,
}

/// What a [`Field`] is made of.
///
/// This reflects [`ast::Item`].
#[derive(Clone, Debug)]
pub struct FieldItem {
    pub primary: Ast<Primary>,
    pub modifier: Option<Ast<ast::Modifier>>,
}

/// Primary part of a [`FieldItem`] (without the modifier).
///
/// This reflects [`ast::Primary`].
#[derive(Clone, Debug)]
pub enum Primary {
    Token(TokenType),
    Node(NodeType),
    Parent(Vec<Ast<Alternative>>),
}

/// Alternative in a parenthesized item.
///
/// This reflects [`ast::Alternative`].
#[derive(Clone, Debug)]
pub struct Alternative(pub Vec<Ast<FieldItem>>);
