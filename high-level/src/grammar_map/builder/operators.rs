//! Generates conflict solutions from a precedence table.

use super::node_attributes::PrecedenceSide;
use crate::{NodeType, TokenType};
use lr_parser::grammar::{ConflictSolution, ConflictingAction, RuleIdx};
use std::collections::HashMap;

#[derive(Default)]
/// Associated with a token.
pub(super) struct Operator {
    pub(super) prefix: Vec<(RuleIdx<NodeType>, u32)>,
    pub(super) infix: Vec<(RuleIdx<NodeType>, u32, PrecedenceSide)>,
    pub(super) suffix: Vec<(RuleIdx<NodeType>, u32)>,
}

pub(super) enum OperatorKind {
    Prefix(u32),
    Infix(u32, PrecedenceSide),
    Suffix(u32),
}

/// Holds the precedence of operators.
pub(super) struct PrecedenceTable {
    pub(super) operators: HashMap<TokenType, Operator>,
}

impl PrecedenceTable {
    /// Creates an empty precedence table.
    pub(super) fn new() -> Self {
        Self {
            operators: HashMap::new(),
        }
    }

    /// Add a new operator to the table.
    ///
    /// # Parameters
    /// - `token`: token corresponding to the new operator.
    /// - `rule`: rule in which the token is.
    /// - `kind`: prefix, infix or suffix.
    pub(super) fn add_operator(
        &mut self,
        token: TokenType,
        rule: RuleIdx<NodeType>,
        kind: OperatorKind,
    ) {
        let operator = self.operators.entry(token).or_default();
        match kind {
            OperatorKind::Prefix(precedence) => operator.prefix.push((rule, precedence)),
            OperatorKind::Infix(precedence, side) => operator.infix.push((rule, precedence, side)),
            OperatorKind::Suffix(precedence) => operator.suffix.push((rule, precedence)),
        }
    }

    /// Generates the conflicts solutions for this precedence table.
    pub(super) fn conflict_solutions(&self) -> Vec<ConflictSolution<TokenType, NodeType>> {
        use std::cmp::Ordering;

        let mut solutions = Vec::new();

        for (token, operator) in &self.operators {
            for (token2, operator2) in &self.operators {
                for (prefix_rule, prefix_precedence) in &operator.prefix {
                    // prefix vs suffix
                    for (_, suffix_precedence) in &operator2.suffix {
                        match prefix_precedence.cmp(suffix_precedence) {
                            Ordering::Equal => {}
                            Ordering::Less => solutions.push(ConflictSolution {
                                prefer: ConflictingAction::Shift(*token2),
                                over: ConflictingAction::Reduce(*prefix_rule),
                                lookahead: None,
                            }),
                            Ordering::Greater => solutions.push(ConflictSolution {
                                prefer: ConflictingAction::Reduce(*prefix_rule),
                                over: ConflictingAction::Shift(*token2),
                                lookahead: None,
                            }),
                        }
                    }
                    // prefix vs infix
                    for (_, infix_precedence, _) in &operator2.infix {
                        match prefix_precedence.cmp(infix_precedence) {
                            Ordering::Equal => {}
                            Ordering::Less => solutions.push(ConflictSolution {
                                prefer: ConflictingAction::Shift(*token2),
                                over: ConflictingAction::Reduce(*prefix_rule),
                                lookahead: None,
                            }),
                            Ordering::Greater => solutions.push(ConflictSolution {
                                prefer: ConflictingAction::Reduce(*prefix_rule),
                                over: ConflictingAction::Shift(*token2),
                                lookahead: None,
                            }),
                        }
                    }
                }
                // suffix vs infix
                for (_, suffix_precedence) in &operator.suffix {
                    for (infix_rule, infix_precedence, _) in &operator2.infix {
                        match suffix_precedence.cmp(infix_precedence) {
                            Ordering::Equal => {}
                            Ordering::Less => solutions.push(ConflictSolution {
                                prefer: ConflictingAction::Reduce(*infix_rule),
                                over: ConflictingAction::Shift(*token),
                                lookahead: None,
                            }),
                            Ordering::Greater => solutions.push(ConflictSolution {
                                prefer: ConflictingAction::Shift(*token),
                                over: ConflictingAction::Reduce(*infix_rule),
                                lookahead: None,
                            }),
                        }
                    }
                }
                // (prefix + infix) vs (suffix + infix)
                // Check that the prefix/infix precedences agree
                // [`Ordering::Less`] if infix is preferred over prefix.
                let mut prefix_infix_ordering: Option<Ordering> = None;
                'outer: for (_, prefix_precedence) in &operator.prefix {
                    for (_, prefix_infix_precedence, _) in &operator.infix {
                        let new_ordering = prefix_precedence.cmp(prefix_infix_precedence);
                        match prefix_infix_ordering {
                            Some(old_ordering) => {
                                if old_ordering != new_ordering {
                                    prefix_infix_ordering = None;
                                    break 'outer;
                                }
                            }
                            None => prefix_infix_ordering = Some(new_ordering),
                        }
                    }
                }
                if let Some(prefix_infix_ordering) = prefix_infix_ordering {
                    for (suffix_rule, suffix_precedence) in &operator2.suffix {
                        let mut push_solution = |prefix_is_infix: bool| {
                            if prefix_is_infix {
                                solutions.push(ConflictSolution {
                                    prefer: ConflictingAction::Reduce(*suffix_rule),
                                    over: ConflictingAction::Shift(*token),
                                    lookahead: None,
                                })
                            } else {
                                solutions.push(ConflictSolution {
                                    prefer: ConflictingAction::Shift(*token),
                                    over: ConflictingAction::Reduce(*suffix_rule),
                                    lookahead: None,
                                })
                            }
                        };
                        // Check that the suffix/infix precedences agree
                        // [`Ordering::Less`] if suffix is preferred over infix
                        let mut infix_suffix_ordering: Option<Ordering> = None;
                        for (suffix_infix_rule, suffix_infix_precedence, _) in &operator2.infix {
                            if suffix_rule == suffix_infix_rule {
                                let new_ordering = suffix_infix_precedence.cmp(suffix_precedence);
                                match infix_suffix_ordering {
                                    Some(old_ordering) => {
                                        if old_ordering != new_ordering {
                                            infix_suffix_ordering = None;
                                            break;
                                        }
                                    }
                                    None => infix_suffix_ordering = Some(new_ordering),
                                }
                            }
                        }
                        if let Some(infix_suffix_ordering) = infix_suffix_ordering {
                            let prefix_is_infix = match infix_suffix_ordering {
                                Ordering::Less => {
                                    if matches!(
                                        prefix_infix_ordering,
                                        Ordering::Less | Ordering::Equal
                                    ) {
                                        Some(true)
                                    } else {
                                        None
                                    }
                                }
                                Ordering::Equal => match prefix_infix_ordering {
                                    Ordering::Less => Some(true),
                                    Ordering::Equal => None,
                                    Ordering::Greater => Some(false),
                                },
                                Ordering::Greater => {
                                    if matches!(
                                        prefix_infix_ordering,
                                        Ordering::Greater | Ordering::Equal
                                    ) {
                                        Some(false)
                                    } else {
                                        None
                                    }
                                }
                            };
                            if let Some(prefix_is_infix) = prefix_is_infix {
                                push_solution(prefix_is_infix)
                            }
                        }
                    }
                }

                // infix vs infix
                for (rule, precedence, side) in &operator.infix {
                    for (_, precedence2, side2) in &operator2.infix {
                        match precedence.cmp(precedence2) {
                            Ordering::Equal => match side {
                                PrecedenceSide::Left if side2 == &PrecedenceSide::Left => solutions
                                    .push(ConflictSolution {
                                        prefer: ConflictingAction::Reduce(*rule),
                                        over: ConflictingAction::Shift(*token2),
                                        lookahead: None,
                                    }),

                                PrecedenceSide::Right if side2 == &PrecedenceSide::Right => {
                                    solutions.push(ConflictSolution {
                                        prefer: ConflictingAction::Shift(*token2),
                                        over: ConflictingAction::Reduce(*rule),
                                        lookahead: None,
                                    })
                                }

                                _ => {}
                            },
                            Ordering::Less => solutions.push(ConflictSolution {
                                prefer: ConflictingAction::Shift(*token2),
                                over: ConflictingAction::Reduce(*rule),
                                lookahead: None,
                            }),
                            Ordering::Greater => solutions.push(ConflictSolution {
                                prefer: ConflictingAction::Reduce(*rule),
                                over: ConflictingAction::Shift(*token2),
                                lookahead: None,
                            }),
                        }
                    }
                }
            }
        }
        solutions
    }
}
