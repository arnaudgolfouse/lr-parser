use crate::{parser, SyntaxTreeLocation};
use codespan_reporting::files::Error as FilesError;
use lr_parser::{
    syntax_tree::SyntaxElement,
    text_ref::{Span, TextRc},
};
use std::{
    collections::HashMap,
    io::Error as IoError,
    path::{Path, PathBuf},
    rc::Rc,
    sync::Arc,
};

/// ID of the file in which an item was found
pub type FileId = u32;

/// Error while [adding] a file to the [files manager].
///
/// [adding]: FilesManager::add_file
/// [files manager]: FilesManager
#[derive(Debug)]
pub enum AddFileError {
    /// IO-related error.
    ///
    /// This is usually due to a non-existent file.
    Io(PathBuf, IoError),
    /// The given ID does not correspond to any file in the [files manager](FilesManager).
    InvalidId(FileId),
    /// Tried to create a new file relative to an inline file (that does not have a
    /// path in the first place).
    InlineFile(FileId),
    /// The file we tried to add was the same as the one corresponding to
    /// `already_present`.
    RedundantFile {
        /// ID of the already present file.
        already_present: FileId,
        /// If `None`, this is the root file.
        ///
        /// Else, `included_from` is where the file was previously included.
        included_from: Option<SyntaxTreeLocation>,
    },
}

/// File managed by a [`FilesManager`].
#[derive(Clone, Debug)]
struct ManagedFile {
    /// Path of the file if it originated from the file-system.
    path: Option<PathBuf>,
    /// Printable name of the file.
    name: String,
    /// Contents of the file.
    content: Rc<str>,
    /// Information about the span of each line of the file.
    lines_ranges: Vec<Span>,
    /// `Some` if the file was included using the `include "..."` directive.
    included_from: Option<SyntaxTreeLocation>,
}

/// Simple structure for managing a grammar split across multiple files.
#[derive(Clone, Debug)]
pub struct FilesManager {
    /// Simple list of managed files.
    files: Vec<ManagedFile>,
}

impl<'a> codespan_reporting::files::Files<'a> for FilesManager {
    type FileId = FileId;

    type Name = &'a str;

    type Source = Rc<str>;

    fn name(&'a self, id: Self::FileId) -> Result<Self::Name, FilesError> {
        self.files
            .get(id as usize)
            .map(|s| s.name.as_str())
            .ok_or(FilesError::FileMissing)
    }

    fn source(&'a self, id: Self::FileId) -> Result<Self::Source, FilesError> {
        self.files
            .get(id as usize)
            .map(|s| s.content.clone())
            .ok_or(FilesError::FileMissing)
    }

    fn line_index(&'a self, id: Self::FileId, byte_index: usize) -> Result<usize, FilesError> {
        let lines_ranges = &self
            .files
            .get(id as usize)
            .ok_or(FilesError::FileMissing)?
            .lines_ranges;
        // FIXME: check that we compute the right line (this is important, as error reporting might panic later on !)
        match lines_ranges.binary_search_by(|range| range.end.cmp(&byte_index)) {
            Ok(line) => Ok(line),
            Err(line) => Ok(std::cmp::min(line, lines_ranges.len().saturating_sub(1))),
        }
    }

    fn line_range(
        &'a self,
        id: Self::FileId,
        line_index: usize,
    ) -> Result<std::ops::Range<usize>, FilesError> {
        let lines_ranges = &self
            .files
            .get(id as usize)
            .ok_or(FilesError::FileMissing)?
            .lines_ranges;
        lines_ranges
            .get(line_index)
            .cloned()
            .ok_or(FilesError::FileMissing)
    }
}

impl FilesManager {
    /// Creates an empty `FilesManager`
    pub fn new() -> Self {
        Self { files: Vec::new() }
    }

    /// Add a root file to the `FilesManager`.
    ///
    /// This will returns the ID of the root file for future reference.
    pub fn add_root(&mut self, root_path: PathBuf) -> Result<FileId, (IoError, PathBuf)> {
        let content: Rc<str> = match std::fs::read_to_string(&root_path) {
            Ok(content) => content,
            Err(err) => return Err((err, root_path)),
        }
        .into();
        let name = root_path.display().to_string();
        let lines_ranges = content
            .lines()
            .map(|line| {
                let start = line.as_ptr() as usize - content.as_ref().as_ptr() as usize;
                std::ops::Range {
                    start,
                    end: start + line.len(),
                }
            })
            .collect();
        let id = self.files.len() as FileId;
        self.files.push(ManagedFile {
            path: Some(root_path),
            name,
            content,
            lines_ranges,
            included_from: None,
        });
        Ok(id)
    }

    /// Add a file a pure content (that is, a file that does not originate from the
    /// filesystem).
    pub fn add_inline_file(&mut self, name: String, content: Rc<str>) -> FileId {
        let id = self.files.len() as FileId;
        let lines_ranges = content
            .lines()
            .map(|line| {
                let start = line.as_ptr() as usize - content.as_ptr() as usize;
                std::ops::Range {
                    start,
                    end: start + line.len(),
                }
            })
            .collect();
        self.files.push(ManagedFile {
            path: None,
            name,
            content,
            lines_ranges,
            included_from: None,
        });
        id
    }

    /// Add a file to the files manager.
    ///
    /// The path of the new file is `path` taken relative to `parent`.
    ///
    /// # Errors
    /// Returns an error if:
    /// - `parent_id` does not exists in the file manager
    /// - `parent_id` is an inline file
    /// - Some IO-related error happened (e.g. the file was not readable)
    /// - The file was already present in the files manager
    pub fn add_file<P: AsRef<Path>>(
        &mut self,
        parent_id: FileId,
        included_from: Option<SyntaxTreeLocation>,
        path: P,
    ) -> Result<FileId, AddFileError> {
        let parent = &self
            .files
            .get(parent_id as usize)
            .ok_or(AddFileError::InvalidId(parent_id))?;
        let parent_path = match &parent.path {
            Some(path) => path,
            None => return Err(AddFileError::InlineFile(parent_id)),
        };
        let new_path = parent_path.parent().unwrap().join(path);
        let new_id = self.files.len();
        let content: Rc<str> = match std::fs::read_to_string(&new_path) {
            Ok(content) => content.into(),
            Err(error) => return Err(AddFileError::Io(new_path, error)),
        };
        let lines_ranges = content
            .lines()
            .map(|line| {
                let start = line.as_ptr() as usize - content.as_ref().as_ptr() as usize;
                std::ops::Range {
                    start,
                    end: start + line.len(),
                }
            })
            .collect();
        let new_path_canon = match new_path.as_path().canonicalize() {
            Ok(path) => path,
            Err(io_err) => return Err(AddFileError::Io(new_path, io_err)),
        };

        for (file_id, file) in self.files.iter().enumerate() {
            if let Some(file_path) = &file.path {
                let file_path_canon = match file_path.as_path().canonicalize() {
                    Ok(path) => path,
                    Err(io_err) => return Err(AddFileError::Io(file_path.clone(), io_err)),
                };
                if file_path_canon == new_path_canon {
                    return Err(AddFileError::RedundantFile {
                        already_present: file_id as FileId,
                        included_from: file.included_from.clone(),
                    });
                }
            }
        }
        let name = new_path.display().to_string();
        self.files.push(ManagedFile {
            path: Some(new_path),
            name,
            content,
            lines_ranges,
            included_from,
        });

        Ok(new_id as FileId)
    }
}

impl Default for FilesManager {
    fn default() -> Self {
        Self::new()
    }
}

/// Wrapper around [`FilesManager`], that also holds syntax trees.
#[derive(Clone, Debug)]
pub struct GrammarFiles {
    pub files_manager: FilesManager,
    pub(super) syntax_trees:
        HashMap<FileId, Arc<SyntaxElement<parser::Token, parser::Node, TextRc>>>,
}

impl<'a> codespan_reporting::files::Files<'a> for GrammarFiles {
    type FileId = <FilesManager as codespan_reporting::files::Files<'a>>::FileId;
    type Name = <FilesManager as codespan_reporting::files::Files<'a>>::Name;
    type Source = <FilesManager as codespan_reporting::files::Files<'a>>::Source;

    fn name(&'a self, id: Self::FileId) -> Result<Self::Name, FilesError> {
        self.files_manager.name(id)
    }

    fn source(&'a self, id: Self::FileId) -> Result<Self::Source, FilesError> {
        self.files_manager.source(id)
    }

    fn line_index(&'a self, id: Self::FileId, byte_index: usize) -> Result<usize, FilesError> {
        self.files_manager.line_index(id, byte_index)
    }

    fn line_range(
        &'a self,
        id: Self::FileId,
        line_index: usize,
    ) -> Result<std::ops::Range<usize>, FilesError> {
        self.files_manager.line_range(id, line_index)
    }
}

impl GrammarFiles {
    /// Creates a new, empty `GrammarFiles`.
    pub fn new() -> Self {
        Self {
            files_manager: FilesManager::new(),
            syntax_trees: HashMap::new(),
        }
    }

    /// Creates a new `GrammarFiles` that contains no syntax trees.
    ///
    /// # Note
    /// The resulting structure should not be used to display [errors](super::MappingError),
    /// as these might require looking at the syntax trees.
    pub fn from_files_manager(files_manager: FilesManager) -> Self {
        Self {
            files_manager,
            syntax_trees: HashMap::new(),
        }
    }

    /// Get the syntax tree corresponding to `file`.
    pub fn get_syntax_tree(
        &self,
        file: FileId,
    ) -> Option<&Arc<SyntaxElement<parser::Token, parser::Node, TextRc>>> {
        self.syntax_trees.get(&file)
    }
}

impl Default for GrammarFiles {
    fn default() -> Self {
        Self::new()
    }
}
