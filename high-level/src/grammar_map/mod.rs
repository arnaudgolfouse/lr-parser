//! # TODOS:
//! - Careful: we lose a **lot** of info right now, we might want to keep more !
//!   (in particular, what structures to create ?)
//! - Generate a (unique !) name for silent nodes ?

mod builder;
mod error;
mod files_manager;
mod node_in_map;

pub use self::{
    error::{MappingError, MappingErrorDisplay, SyntaxTreeLocation, Warning, WarningDisplay},
    files_manager::{FileId, FilesManager, GrammarFiles},
    node_in_map::{Alternative, Field, FieldItem, NodeContent, NodeInMap, NodeVariant, Primary},
};
/// Reexport of `codespan_reporting::files::Files`.
///
pub use codespan_reporting::files::Files;

use crate::parser::{
    self,
    ast::{Ast, File, TopLevelItem},
    Tokenizer,
};
use files_manager::AddFileError;
use lr_parser::{
    compute::{ComputeLR1States, TransitionTable},
    grammar::RuntimeGrammar,
    parser::RemappedToken,
    syntax_tree::SyntaxElement,
    text_ref::TextRc,
    LR1Parser,
};
use std::{collections::HashMap, convert::TryFrom as _, num::NonZeroU16, path::PathBuf};

/// Type of a token identifier
#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct TokenType(NonZeroU16);

impl std::fmt::Debug for TokenType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}

impl std::fmt::Display for TokenType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        std::fmt::Display::fmt(&self.0, f)
    }
}

/*impl From<TokenType> for usize {
    fn from(token: TokenType) -> Self {
        usize::from(u16::from(token.0))
    }
}*/

impl std::convert::TryFrom<u16> for TokenType {
    type Error = <NonZeroU16 as std::convert::TryFrom<u16>>::Error;

    fn try_from(value: u16) -> Result<Self, Self::Error> {
        Ok(Self(std::convert::TryFrom::<u16>::try_from(value)?))
    }
}

impl std::convert::TryFrom<usize> for TokenType {
    type Error = <NonZeroU16 as std::convert::TryFrom<u16>>::Error;

    fn try_from(value: usize) -> Result<Self, Self::Error> {
        let inner = u16::try_from(value)?;
        Ok(Self(std::convert::TryFrom::<u16>::try_from(inner)?))
    }
}

impl TokenType {
    pub const MAX: Self = Self(unsafe { NonZeroU16::new_unchecked(u16::MAX) });

    fn as_index(self) -> usize {
        (u16::from(self.0) - 1) as usize
    }
}

/// Type of a node identifier
pub type NodeType = u16;

/// Stores the token remapping in [`TokenInMap`]. See [`RemappedToken`].
#[derive(Debug, Clone)]
enum RemappedTokenOwned {
    None,
    Transform(TokenType),
    Split(Box<[(TokenType, usize)]>),
}

impl Default for RemappedTokenOwned {
    fn default() -> Self {
        RemappedTokenOwned::None
    }
}

/// Expression used to define a token.
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum ExpressionKind {
    /// A simple piece of text, like `RETURN_KW: 'return'`.
    Raw(String),
    /// Defined by a regex, like `INT: regex("[0-9]+")`.
    Regex(String),
    /// External token, will be provided by the lexer.
    External,
}

/// Collection of tokens with all the necessary information.
#[derive(Debug)]
pub struct TokensInMap {
    /// Documentation of the `tokens` block.
    ///
    /// This is `None` is no documentation was found.
    pub documentation: Option<String>,
    /// context for the lexer: `(field_name, type)`.
    pub context: Vec<(String, String)>,
    /// optional `verbatim` block.
    pub verbatim: Option<String>,
    /// Tokens
    ///
    /// The index of a token is its symbol ID.
    ///
    /// # Note
    ///
    /// The first token is the error/unknown token !
    pub tokens: Vec<TokenInMap>,
    /// Map a location in source code to a token.
    pub location_to_token: HashMap<SyntaxTreeLocation, TokenType>,
    /// Map a raw token string to its id.
    pub token_str_to_token: HashMap<String, TokenType>,
    /// Map a token name to its id.
    pub name_to_token: HashMap<String, TokenType>,
}

/// Token node, stored in a [`GrammarMap`]
#[derive(Debug, Clone)]
pub struct TokenInMap {
    /// ID for this token.
    id: TokenType,
    /// Location of the token in the source.
    location: SyntaxTreeLocation,
    /// Documentation of the token.
    ///
    /// This is `None` is no documentation was found.
    pub documentation: Option<String>,
    /// Name of the token.
    ///
    /// # Example
    /// If the source contains
    /// ```text
    /// tokens {
    ///     EQUAL: '='
    /// }
    /// ```
    /// Then the name of the (only) token is `"EQUAL"`.
    name: String,
    /// Definition of the token.
    ///
    /// # Example
    /// If the source contains
    /// ```text
    /// tokens {
    ///     EQUAL: '='
    /// }
    /// ```
    /// Then the expression of the EQUAL is `ExpressionKind::Raw("=")`.
    expression: ExpressionKind,
    /// Callback function for the lexer.
    callback: Option<String>,
    /// Conversion function for the AST.
    ///
    /// Keeps the `into` function name, as well as its return type.
    into: Option<(String, String)>,
    /// Whether this token is [skippable](lr_parser::parser::TokenKind).
    ///
    /// It will be `false` by default, and `true` if the token has the `#[skip]`
    /// attribute.
    skip: bool,
    /// This token's [remapping](lr_parser::LR1Parser#token-remapping).
    ///
    /// This is defined by the `#[remap(...)]` attribute.
    remap: RemappedTokenOwned,
    /// User-defined priority.
    priority: Option<u32>,
}

impl TokenInMap {
    /// ID for this token.
    pub fn id(&self) -> TokenType {
        self.id
    }

    /// Location of the token in the source code.
    #[inline]
    pub fn location(&self) -> &SyntaxTreeLocation {
        &self.location
    }

    /// Name of the token.
    ///
    /// # Example
    /// If the token was defined by
    /// ```text
    /// RETURN_KW: 'return',
    /// ```
    /// The name of the token is `"RETURN_KW"`.
    #[inline]
    pub fn name(&self) -> &str {
        &self.name
    }

    /// The regex/text used to define the token
    ///
    /// # Example
    /// If the source file contained (in the `tokens` section):
    /// ```text
    /// RETURN_KW: 'return',
    /// INT: regex("[0-9]+"),
    /// IDENT: external,
    /// ```
    /// - the expression of RETURN_KW is `ExpressionKind::Raw("return")`.
    /// - the expression of INT is `ExpressionKind::Regex("[0-9]+")`.
    /// - the expression of IDENT is `ExpressionKind::External`.
    #[inline]
    pub fn expression(&self) -> &ExpressionKind {
        &self.expression
    }

    /// Callback function
    #[inline]
    pub fn callback(&self) -> Option<&str> {
        self.callback.as_deref()
    }

    /// Returns the 'into' function if one was defined.
    #[inline]
    pub fn function_into(&self) -> Option<&str> {
        self.into.as_ref().map(|(function, _)| function.as_str())
    }

    /// Returns the return type of the 'into' function if one was defined.
    #[inline]
    pub fn return_type_into(&self) -> Option<&str> {
        self.into
            .as_ref()
            .map(|(_, return_type)| return_type.as_str())
    }

    /// Whether this token is [skippable](lr_parser::parser::TokenKind).
    ///
    /// It will be `false` by default, and `true` if the token has the `#[skip]`
    /// attribute.
    #[inline]
    pub fn skip(&self) -> bool {
        self.skip
    }

    /// Token remapping.
    ///
    /// See the [Token remapping](lr_parser::LR1Parser#token-remapping) section in `lr_parser`.
    #[inline]
    pub fn remapped(&self) -> RemappedToken<TokenType> {
        match &self.remap {
            RemappedTokenOwned::None => RemappedToken::None,
            RemappedTokenOwned::Transform(transformed) => RemappedToken::Transform(*transformed),
            RemappedTokenOwned::Split(split) => RemappedToken::Split(split.as_ref()),
        }
    }
}

/// High-level structure for holding the content of a grammar file.
#[derive(Debug)]
pub struct GrammarMap {
    /// Holds the grammar rules
    grammar: RuntimeGrammar<TokenType, NodeType>,
    tokens: TokensInMap,
    /// Nodes
    ///
    /// The index of a node is its symbol ID.
    nodes: Vec<NodeInMap>,
    /// Map a location in source code to a node.
    location_to_node: HashMap<SyntaxTreeLocation, NodeType>,
    /// Starting node.
    starting_node: NodeType,
    /// Map a name to a node.
    name_to_node: HashMap<String, NodeType>,
}

impl GrammarMap {
    /// Create a `GrammarMap` from the file at `root_path`.
    ///
    /// # Panics
    /// Panics if `algorithm` cannot create the parse tables.
    pub fn from_root_path<Algorithm>(
        grammar_files: &mut GrammarFiles,
        root_path: PathBuf,
        algorithm: Algorithm,
    ) -> Result<(Self, Vec<Warning>), MappingError>
    where
        Algorithm: for<'a> ComputeLR1States<RuntimeGrammar<parser::Token, parser::Node>>,
    {
        let grammar = crate::parser::get_grammar();
        let table = algorithm
            .compute_states_with_start(parser::Node::File, &grammar)
            .unwrap();
        let file_node_state = table.starting_state(parser::Node::File).unwrap();
        let root_id =
            grammar_files
                .files_manager
                .add_root(root_path)
                .map_err(|(error, path)| MappingError::Io {
                    included_from: None,
                    error,
                    path,
                })?;
        let mut top_level_items = Vec::new();
        Self::add_file(
            &mut top_level_items,
            grammar_files,
            root_id,
            &grammar,
            &table,
            file_node_state,
            None,
        )?;

        let mut warnings = Vec::new();
        let builder = builder::Builder::new(&top_level_items, &mut warnings)?;
        let processed = builder
            .process_tokens()?
            .process_nodes()?
            .process_conflicts()?;

        Ok((
            Self {
                grammar: processed.grammar,
                tokens: processed.tokens_in_map,
                nodes: processed.nodes,
                location_to_node: processed.location_to_node,
                name_to_node: processed.nodes_names,
                starting_node: processed.start_node_id,
            },
            warnings,
        ))
    }

    /// Parse a file, and add its `TopLevelItem`s to `result`.
    fn add_file<Table>(
        result: &mut Vec<(Ast<crate::parser::ast::TopLevelItem>, FileId)>,
        grammar_files: &mut GrammarFiles,
        file: FileId,
        grammar: &RuntimeGrammar<parser::Token, parser::Node>,
        table: &Table,
        file_node_state: Table::State,
        included_from: Option<SyntaxTreeLocation>,
    ) -> Result<(), MappingError>
    where
        Table: TransitionTable<Token = parser::Token, Node = parser::Node>,
    {
        let source = TextRc::new_full(grammar_files.files_manager.source(file).unwrap());
        let mut parser = LR1Parser::new(
            table,
            file_node_state,
            Tokenizer::new(source.as_ref()),
            parser::Node::silent,
            parser::Token::remap,
        );
        let (syntax_tree, errors) =
            SyntaxElement::from_parser(source.clone(), &mut parser, grammar, parser::Node::File);
        let syntax_tree = grammar_files
            .syntax_trees
            .entry(file)
            .or_insert(syntax_tree);
        if !errors.is_empty() {
            return Err(MappingError::SyntaxErrors {
                included_from,
                file,
                errors,
            });
        }
        let ast = match Ast::<File>::try_from(&*syntax_tree) {
            Ok(ast) => ast,
            Err(error) => {
                return Err(MappingError::AstError {
                    included_from,
                    file,
                    error,
                })
            }
        };

        for item in ast.item.top_level_items {
            if let TopLevelItem::Include { path } = item.item {
                let included_from = SyntaxTreeLocation {
                    file,
                    element: item.element.clone(),
                };
                match grammar_files.files_manager.add_file(
                    file,
                    Some(included_from.clone()),
                    path.as_str(),
                ) {
                    Ok(new_file) => {
                        Self::add_file(
                            result,
                            grammar_files,
                            new_file,
                            grammar,
                            table,
                            file_node_state,
                            Some(included_from),
                        )?;
                    }
                    Err(error) => {
                        return Err(match error {
                            AddFileError::Io(path, error) => MappingError::Io {
                                included_from: Some(included_from),
                                path,
                                error,
                            },
                            AddFileError::InvalidId(id) => {
                                unreachable!("invalid id: {}", id)
                            }
                            AddFileError::InlineFile(id) => {
                                unreachable!(
                                    "inline file: {} (id = {})",
                                    grammar_files.files_manager.name(id).unwrap(),
                                    id
                                )
                            }
                            AddFileError::RedundantFile {
                                already_present,
                                included_from: previously_included,
                            } => MappingError::RedundantFile {
                                previously_included,
                                included_from,
                                already_present,
                            },
                        })
                    }
                }
            } else {
                result.push((item, file))
            }
        }

        Ok(())
    }

    /// Returns the token corresponding to `id`.
    pub fn get_token(&self, id: TokenType) -> Option<&TokenInMap> {
        self.tokens.tokens.get(id.as_index())
    }

    /// Returns the tokens of this map.
    pub fn tokens(&self) -> &TokensInMap {
        &self.tokens
    }

    /// Get a token by name
    pub fn token_by_name(&self, name: &str) -> Option<&TokenInMap> {
        self.tokens
            .name_to_token
            .get(name)
            .and_then(|id| self.tokens.tokens.get(id.as_index()))
    }

    /// Returns the nodes of this map.
    pub fn nodes(&self) -> impl Iterator<Item = &NodeInMap> {
        self.nodes.iter()
    }

    /// Get a node by its ID.
    pub fn get_node(&self, node_id: NodeType) -> Option<&NodeInMap> {
        self.nodes.get(node_id as usize)
    }

    /// Returns the grammar of this map
    pub fn grammar(&self) -> &RuntimeGrammar<TokenType, NodeType> {
        &self.grammar
    }

    /// Returns the starting node.
    pub fn starting_node(&self) -> NodeType {
        self.starting_node
    }
}
