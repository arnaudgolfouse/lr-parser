//! Defines the format of the grammar file
//!
//! This provides facilities to parse a grammar file.

mod grammar_map;
mod parser;
//pub mod parser_alt;

pub use grammar_map::*;
pub use parser::ast;

#[cfg(test)]
mod tests;
