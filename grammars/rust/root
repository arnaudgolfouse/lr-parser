// See 'The Rust Programming Language'
tokens {
	// keywords
	AS_KW: 'as',
	ASYNC_KW: 'async',
	AWAIT_KW: 'await',
	BREAK_KW: 'break',
	CONST_KW: 'const',
	CONTINUE_KW: 'continue',
	CRATE_KW: 'crate',
	DYN_KW: 'dyn',
	ELSE_KW: 'else',
	ENUM_KW: 'enum',
	EXTERN_KW: 'extern',
	FALSE_KW: 'false',
	FN_KW: 'fn',
	FOR_KW: 'for',
	IF_KW: 'if',
	IMPL_KW: 'impl',
	IN_KW: 'in',
	LET_KW: 'let',
	LOOP_KW: 'loop',
	MATCH_KW: 'match',
	MOD_KW: 'mod',
	MOVE_KW: 'move',
	MUT_KW: 'mut',
	PUB_KW: 'pub',
	REF_KW: 'ref',
	RETURN_KW: 'return',
	#[remap(IDENT)]
	UPPERCASE_SELF_KW: 'Self',
	SELF_KW: 'self',
	STATIC_KW: 'static',
	STRUCT_KW: 'struct',
	SUPER_KW: 'super',
	TRAIT_KW: 'trait',
	TRUE_KW: 'true',
	TYPE_KW: 'type',
	UNION_KW: 'union',
	UNSAFE_KW: 'unsafe',
	USE_KW: 'use',
	WHERE_KW: 'where',
	WHILE_KW: 'while',

	// reserved keywords: not used now, but may be in the future
	ABSTRACT_KW: 'abstract',
	BECOME_KW: 'become',
	BOX_KW: 'box',
	DO_KW: 'do',
	FINAL_KW: 'final',
	MACRO_KW: 'macro',
	OVERRIDE_KW: 'override',
	PRIV_KW: 'priv',
	TRY_KW: 'try',
	TYPEOF_KW: 'typeof',
	UNSIZED_KW: 'unsized',
	VIRTUAL_KW: 'virtual',
	YIELD_KW: 'yield',

	// operators
	BANG: '!',
	NOT_EQUAL: '!=',
	PERCENT: '%',
	MOD_EQUAL: '%=',
	AMPERSAND: '&',
	AND_EQUAL: '&=',
	#[remap('&', '&')] // Example: `let _ = &&();`
	LOGIC_AND: '&&',
	STAR: '*',
	MUL_EQUAL: '*=',
	PLUS: '+',
	ADD_EQUAL: '+=',
	COMMA: ',',
	MINUS: '-',
	SUB_EQUAL: '-=',
	ARROW: '->',
	DOT: '.',
	DOT_DOT: '..',
	DOT_DOT_EQUAL: '..=',
	DOT_DOT_DOT: '...',
	SLASH: '/',
	DIV_EQUAL: '/=',
	COLON: ':',
	SEMICOLON: ';',
	SHIFT_LEFT: '<<',
	SHLEFT_EQUAL: '<<=',
	LESS: '<',
	LESS_EQUAL: '<=',
	EQUAL: '=',
	EQUAL_EQUAL: '==',
	THICK_ARROW: '=>',
	MORE: '>',
	#[remap('>', '=')] // Example: `let Vec<()>= vec![];`
	MORE_EQUAL: '>=',
	#[remap('>', '>')] // Example: `let Vec<Vec<()>>;`
	SHIFT_RIGHT: '>>',
	#[remap('>', '>', '=')] // Example: `let Vec<Vec<()>>= vec![];`
	SHRIGHT_EQUAL: '>>=',
	AT: '@',
	HAT: '^',
	XOR_EQUAL: '^=',
	BAR: '|',
	OR_EQUAL: '|=',
	#[remap('|', '|')] // Example: `let _ = || {};`
	LOGIC_OR: '||',
	QUESTION: '?',

	// other symbols
	#[into(parse_lifetime)]
	LIFETIME: external,
	#[into(parse_int)]
	INT: regex("[0-9][0-9a-zA-Z_]*"),
	#[into(parse_float)]
	FLOAT: regex(r"[0-9][0-9_]*\.[0-9_]*"),
	#[into(parse_string)]
	#[callback(raw_string_literal)] // TODO: handle byte strings (`b"hello"` and `br#"hello"`)
	STRING: regex(r#"b?("(\\"|[^"])*"|r#*")"#), // " <- trick
	#[into(parse_char)]
	#[callback(char_or_lifetime)]
	CHAR: regex("b?'"),
	//CHAR: regex(r"b?'(?:[^'\\]|\\.)*'"),
	UNDERSCORE: '_',
	COLON_COLON: '::',
	LPAR: '(',
	RPAR: ')',
	LBRACKET: '[',
	RBRACKET: ']',
	LBRACE: '{',
	RBRACE: '}',
	SHARP: '#',
	#[into(parse_ident)]
	IDENT: regex(r"(\p{XID_Start}|_)\p{XID_Continue}*"), // ??? Unicode ???
	#[skip]
	#[callback(multiline_comment)] // handle nested `/* */`
	COMMENT: regex(r"(//[^\n]*|/\*)"),
	#[callback(multiline_comment)] // handle nested `/** */`
	OUTER_DOC_COMMENT: regex(r"(///[^\n]*|/\*\*)"),
	#[callback(multiline_comment)] // handle nested `/*! */`
	INNER_DOC_COMMENT: regex(r"(//![^\n]*|/\*!)"),
	#[skip]
	WHITESPACE: regex(r"[\s]+"),
}

include "attr";
include "items";
include "type";

#[start]
node Start {
	attrs: InnerAttr*
	items: ModItem*
}

node Literal {
	| Int => 0: INT
	| String => 0: STRING
}