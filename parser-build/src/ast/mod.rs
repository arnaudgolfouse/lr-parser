// TODO: error handling

use crate::Result;
use high_level::{ast, Field, FieldItem, NodeContent, NodeType, Primary};
use proc_macro2::{Ident, TokenStream};
use quote::quote;
use std::{
    collections::HashSet,
    fmt::{self, Write},
};

pub(crate) struct AstStructs<'info, 'map> {
    /// `pub struct Ast<T> { ... }`
    ast_structure: TokenStream,
    /// `Node { field: Ast<...>, ... }`
    structures: Vec<AstStruct<'info, 'map>>,
}

struct AstStruct<'info, 'map> {
    name: &'info Ident,
    doc: Option<&'map str>,
    struct_or_enum: AstStructOrEnum<'info, 'map>,
}

enum AstStructOrEnum<'info, 'map> {
    Struct(Vec<AstField<'info>>, AstShape),
    Enum(Vec<AstVariant<'info, 'map>>),
}

#[derive(Clone, Copy)]
enum AstShape {
    /// `struct S { ... }`
    Struct,
    /// `struct S ( ... );`
    Enum,
    /// `struct S;`
    Empty,
}

struct AstVariant<'info, 'map> {
    doc: Option<&'map str>,
    name: &'map str,
    fields: Vec<AstField<'info>>,
    shape: AstShape,
}

struct AstField<'info> {
    doc: Option<&'info str>,
    label: Option<&'info ast::Label>,
    boxed: bool,
    item: &'info FieldItem,
}

impl<'info, 'map> AstStructs<'info, 'map> {
    pub(crate) fn new(info: &'info super::Info<'map>) -> Result<Self> {
        AstStructsBuilder::new(info).build()
    }

    pub(crate) fn display<'s>(
        &'s self,
        info: &'info super::Info<'map>,
    ) -> AstStructsDisplay<'info, 'map, 's> {
        AstStructsDisplay { inner: self, info }
    }
}

struct AstStructsBuilder<'map, 'info> {
    info: &'info super::Info<'map>,
    /// Nodes already visited.
    ///
    /// Hence boxing has already been applied.
    processed_nodes: HashSet<NodeType>,
}

impl<'map, 'info> AstStructsBuilder<'map, 'info> {
    fn new(info: &'info super::Info<'map>) -> Self {
        Self {
            info,
            processed_nodes: HashSet::new(),
        }
    }

    fn build(mut self) -> Result<AstStructs<'info, 'map>> {
        let structures = {
            let mut structures = Vec::new();
            for (node, node_name) in self.info.nodes.values() {
                if node.is_silent() {
                    continue;
                }
                let doc = node.documentation();
                let struct_or_enum = match node.content() {
                    None => continue,
                    Some(NodeContent::Single { fields, .. }) => {
                        let (fields, shape) = self.get_fields(node.id(), fields)?;
                        AstStructOrEnum::Struct(fields, shape)
                    }
                    Some(NodeContent::Multiple { variants }) => {
                        let mut enum_variants = Vec::new();
                        for variant in variants {
                            let (fields, shape) = self.get_fields(node.id(), &variant.fields)?;
                            enum_variants.push(AstVariant {
                                doc: variant.documentation.as_deref(),
                                name: variant.name.as_str(),
                                fields,
                                shape,
                            });
                        }
                        AstStructOrEnum::Enum(enum_variants)
                    }
                };
                structures.push(AstStruct {
                    name: node_name,
                    doc,
                    struct_or_enum,
                });
            }

            structures
        };

        let ast_structure = {
            let lr_parser_crate_name = &self.info.lr_parser_crate_name;
            let syntax_index_doc = format!(
                "Reference in the [`SyntaxTree`](::{}::syntax_tree::SyntaxTree)",
                lr_parser_crate_name
            );
            quote! {
                #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
                pub struct Ast<T> {
                    #[doc = #syntax_index_doc]
                    pub syntax_index: ::#lr_parser_crate_name::syntax_tree::SyntaxIdx,
                    pub item: T,
                }

                impl<T> std::ops::Deref for Ast<T> {
                    type Target = T;
                    fn deref(&self) -> &Self::Target {
                        &self.item
                    }
                }
            }
        };

        Ok(AstStructs {
            ast_structure,
            structures,
        })
    }

    /// Quote the list of fields corresponding to a node/node variant.
    ///
    /// This returns the quotation result, and
    /// - `Some(true)` if this is a `struct`-like construct (like
    /// `struct S { a: i32 }`).
    /// - `Some(false)` if this is a `enum`-like construct (like `struct S(i32);`).
    /// - `None` if this is an empty construct (like `struct S;`).
    fn get_fields(
        &mut self,
        node: NodeType,
        fields: &'info [Field],
    ) -> Result<(Vec<AstField<'info>>, AstShape)> {
        let mut shape = AstShape::Empty;
        let mut result = Vec::new();
        for field in fields {
            let boxed = if let Some(label) = &field.label {
                match &**label {
                    ast::Label::Id(id) => {
                        crate::check_ident(id)?;
                        match shape {
                            AstShape::Enum => todo!("error handling here"),
                            _ => shape = AstShape::Struct,
                        }
                    }
                    // TODO: error if the number does not match ?
                    ast::Label::Number(_) => match shape {
                        AstShape::Struct => todo!("error handling here"),
                        _ => shape = AstShape::Enum,
                    },
                }
                self.detect_cycle_from_field_item(node, &mut HashSet::new(), &field.item)
            } else {
                false
            };
            result.push(AstField {
                doc: field.documentation.as_deref(),
                label: field.label.as_deref(),
                boxed,
                item: &field.item,
            });
        }
        Ok((result, shape))
    }

    /// Detect a cycle leading to `root_node` again.
    fn detect_cycle(
        &self,
        root_node: NodeType,
        visited_nodes: &mut HashSet<NodeType>,
        node: NodeType,
    ) -> bool {
        visited_nodes.insert(node);
        let node = self.info.nodes[&node].0;
        match node.content() {
            Some(NodeContent::Single { fields, .. }) => {
                for field in fields {
                    if field.label.is_some()
                        && self.detect_cycle_from_field_item(root_node, visited_nodes, &field.item)
                    {
                        return true;
                    }
                }
            }
            Some(NodeContent::Multiple { variants }) => {
                for variant in variants {
                    for field in &variant.fields {
                        if field.label.is_some()
                            && self.detect_cycle_from_field_item(
                                root_node,
                                visited_nodes,
                                &field.item,
                            )
                        {
                            return true;
                        }
                    }
                }
            }
            None => {}
        }
        false
    }

    fn detect_cycle_from_field_item(
        &self,
        root_node: NodeType,
        visited_nodes: &mut HashSet<NodeType>,
        field_item: &FieldItem,
    ) -> bool {
        let mut to_analyze = vec![field_item];
        while let Some(field_item) = to_analyze.pop() {
            match field_item.modifier.as_deref() {
                Some(ast::Modifier::Optional) | None => {}
                _ => continue,
            }
            match &*field_item.primary {
                Primary::Token(_) => {}
                Primary::Node(node) => {
                    if *node == root_node
                        || (!self.processed_nodes.contains(node)
                            && !visited_nodes.contains(node)
                            && self.detect_cycle(root_node, visited_nodes, *node))
                    {
                        return true;
                    }
                }
                Primary::Parent(alternatives) => {
                    for alt in alternatives {
                        for field_item in &alt.0 {
                            to_analyze.push(field_item);
                        }
                    }
                }
            }
        }
        false
    }
}

pub(crate) struct AstStructsDisplay<'info, 'map, 's> {
    inner: &'s AstStructs<'info, 'map>,
    info: &'info super::Info<'map>,
}

impl AstStructsDisplay<'_, '_, '_> {
    fn fmt_doc(f: &mut fmt::Formatter, doc: &str, offset: &str) -> fmt::Result {
        for line in doc.lines() {
            write!(f, "\n{}///{}", offset, line)?;
        }
        Ok(())
    }

    fn fmt_field_item(&self, f: &mut fmt::Formatter, field_item: &FieldItem) -> fmt::Result {
        let pop_last: bool;
        if let Some(modifier) = &field_item.modifier {
            f.write_str(match &**modifier {
                ast::Modifier::Optional => {
                    pop_last = false;
                    "Option<"
                }
                ast::Modifier::Repeat | ast::Modifier::Plus => {
                    pop_last = false;
                    "Vec<"
                }
                ast::Modifier::Trailing
                | ast::Modifier::Separate
                | ast::Modifier::PlusTrailing
                | ast::Modifier::PlusSeparate => {
                    pop_last = true;
                    "Vec<"
                }
            })?;
        } else {
            pop_last = false;
        }
        match &*field_item.primary {
            Primary::Token(token) => write!(f, "{}", self.info.tokens[token].1)?,
            Primary::Node(node) => write!(f, "{}", self.info.nodes[node].1)?,
            Primary::Parent(alternatives) => match alternatives.as_slice() {
                [alt] => {
                    let take = if pop_last {
                        alt.item.0.len() - 1
                    } else {
                        alt.item.0.len()
                    };
                    let need_parent = take != 1;
                    if need_parent {
                        f.write_char('(')?;
                    }
                    for field_item in alt.item.0.iter().take(take) {
                        self.fmt_field_item(f, field_item)?;
                        if need_parent {
                            f.write_char(',')?;
                        }
                    }
                    if need_parent {
                        f.write_char(')')?
                    }
                }
                [] => f.write_str("()")?,
                _ => panic!("only one or zero alternatives are supported"),
            },
        }
        if field_item.modifier.is_some() {
            f.write_char('>')?
        }
        Ok(())
    }

    fn fmt_field(
        &self,
        f: &mut fmt::Formatter,
        struct_or_enum: bool,
        field: &AstField,
        struct_shaped: bool,
    ) -> fmt::Result {
        let offset = if struct_or_enum { "    " } else { "        " };
        let new_line = |f: &mut fmt::Formatter| {
            f.write_char('\n')?;
            f.write_str(offset)
        };
        if let Some(label) = &field.label {
            if let Some(doc) = field.doc {
                Self::fmt_doc(f, doc, offset)?;
            }
            if struct_shaped || field.doc.is_some() {
                new_line(f)?;
            }
            match label {
                ast::Label::Id(id) => {
                    if struct_or_enum {
                        f.write_str("pub ")?;
                    }
                    write!(f, "{}: ", id.as_str())?;
                }
                ast::Label::Number(_) => {}
            }
            if field.boxed {
                f.write_str("Box<")?;
            }
            self.fmt_field_item(f, field.item)?;
            if field.boxed {
                f.write_str(">")?;
            }
        }
        Ok(())
    }

    fn fmt_ast_struct(&self, ast_struct: &AstStruct, f: &mut fmt::Formatter) -> fmt::Result {
        if let Some(doc) = ast_struct.doc {
            Self::fmt_doc(f, doc, "")?;
            f.write_char('\n')?;
        }
        f.write_str("#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]\n")?;
        match &ast_struct.struct_or_enum {
            AstStructOrEnum::Struct(fields, shape) => {
                write!(f, "pub struct {}", ast_struct.name)?;
                let struct_shaped = match shape {
                    AstShape::Struct => {
                        f.write_str(" {")?;
                        true
                    }
                    AstShape::Enum => {
                        f.write_char('(')?;
                        false
                    }
                    AstShape::Empty => return f.write_char(';'),
                };
                let mut fields = fields.iter();
                for field in &mut fields {
                    if field.label.is_some() {
                        self.fmt_field(f, true, field, struct_shaped)?;
                        break;
                    }
                }
                for field in fields {
                    if field.label.is_some() {
                        f.write_char(',')?;
                        self.fmt_field(f, true, field, struct_shaped)?;
                    }
                }
                match shape {
                    AstShape::Struct => f.write_str(",\n}")?,
                    AstShape::Enum => f.write_str(");")?,
                    AstShape::Empty => {}
                }
            }
            AstStructOrEnum::Enum(variants) => {
                write!(f, "pub enum {} {{", ast_struct.name)?;
                for variant in variants {
                    if let Some(doc) = variant.doc {
                        Self::fmt_doc(f, doc, "    ")?;
                    }
                    write!(f, "\n    {}", variant.name)?;
                    let struct_shaped = match variant.shape {
                        AstShape::Struct => {
                            f.write_str(" {")?;
                            true
                        }
                        AstShape::Enum => {
                            f.write_char('(')?;
                            false
                        }
                        AstShape::Empty => {
                            f.write_char(',')?;
                            continue;
                        }
                    };
                    let mut fields = variant.fields.iter();
                    for field in &mut fields {
                        if field.label.is_some() {
                            self.fmt_field(f, false, field, struct_shaped)?;
                            break;
                        }
                    }
                    for field in fields {
                        if field.label.is_some() {
                            f.write_char(',')?;
                            self.fmt_field(f, false, field, struct_shaped)?;
                        }
                    }
                    match variant.shape {
                        AstShape::Struct => f.write_str(",\n    },")?,
                        AstShape::Enum => f.write_str("),")?,
                        AstShape::Empty => {}
                    }
                }
                f.write_str("\n}")?;
            }
        }
        Ok(())
    }
}

impl fmt::Display for AstStructsDisplay<'_, '_, '_> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "
pub mod ast {{
{}

",
            self.inner.ast_structure,
        )?;
        for strukt in &self.inner.structures {
            self.fmt_ast_struct(strukt, f)?;
            f.write_char('\n')?;
        }

        f.write_char('}')
    }
}
