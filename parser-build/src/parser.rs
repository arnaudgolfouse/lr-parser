use crate::Result;
use canonical_lr::CanonicalLR;
use high_level::{GrammarMap, NodeInMap, NodeType, TokenInMap, TokenType};
use lr_parser::{
    compute::{ComputeLR1States as _, LR1Action, LR1Item, TransitionTable as _},
    grammar::{ConflictingAction, Grammar as _, Lookahead, Symbol},
};
use proc_macro2::{Ident, Literal, TokenStream};
use quote::{format_ident, quote};
use std::{
    collections::{BTreeMap, BTreeSet, HashMap},
    fmt,
};

pub(crate) struct Parser {
    /// `pub enum Node { ... }`
    // TODO: do like `Token`: have some sort of `NodeInternal` enum for the parser, but only expose `Node`.
    pub enum_node: TokenStream,
    /// `impl Node { fn silent(self) -> bool { ... } }`
    ///
    /// `impl Display for Node { ... }`
    pub node_impl: TokenStream,
    /// `pub struct Grammar;`
    ///
    /// `impl lr_parser::grammar::Grammar for Grammar { ... }`
    pub struct_impl_grammar: TokenStream,
    /// `enum ParserState { ... }`
    ///
    /// `struct TransitionTable;`
    ///
    /// `impl lr_parser::compute::TransitionTable for TransitionTable { ... }`
    // TODO: hide this structure, and provide its functionality through `Parser`.
    pub transition_table: TokenStream,
    /// `pub struct Parser { ... }`
    pub struct_parser: TokenStream,
    /// `impl Iterator for Parser { ... }`
    ///
    /// `impl Parser { ... }`
    pub parser_impl: TokenStream,
}

impl Parser {
    pub(crate) fn new(info: &super::Info) -> Result<Self> {
        let (enum_node, node_impl) = Self::make_nodes(&info.nodes);
        let lr_parser_crate_name = &info.lr_parser_crate_name;

        let struct_parser = {
            let parser_doc = format!(
                "Wrapper around a [`lr_parser::LR1Parser`](::{}::LR1Parser).",
                lr_parser_crate_name
            );
            quote! {
            type ParserInner<'src> = ::#lr_parser_crate_name::LR1Parser<
                'static,
                Tokenizer<'src>,
                TransitionTable,
                fn(Node) -> bool,
                fn(Token) -> ::#lr_parser_crate_name::parser::RemappedToken<'static, Token>
            >;

            #[doc = #parser_doc]
            pub struct Parser<'src> {
                inner: ParserInner<'src>
            }
            }
        };
        let (transition_table, lr1_table) = Self::make_transition_table(
            info.grammar_map,
            lr_parser_crate_name,
            &info.tokens,
            &info.nodes,
        );
        let starting_state = format_ident!(
            "S{}",
            lr1_table
                .starting_state(info.grammar_map.starting_node())
                .unwrap()
        );
        let parser_impl = quote! {
            impl<'src> Iterator for Parser<'src> {
                type Item = ::#lr_parser_crate_name::parser::ParseResult<TransitionTable>;

                fn next(&mut self) -> Option<Self::Item> {
                    self.inner.next()
                }
            }

            impl<'src> Parser<'src> {
                /// Create a new `Parser` from `source`.
                ///
                /// You must create a new parser for each source you want to parse: it is not possible to reuse the current parser (note that creating the parser is very cheap, so you should not worry).
                pub fn new(source: &'src str) -> Parser<'src> {
                    Self {
                        inner: ::lr_parser::LR1Parser::new(
                            &TransitionTable,
                            ParserState::#starting_state,
                            Tokenizer::new(source),
                            Node::silent,
                            Token::remap,
                        )
                    }
                }

                /// Consume `self` to create a `SyntaxTree`.
                pub fn syntax_tree(mut self) -> (
                    ::#lr_parser_crate_name::syntax_tree::SyntaxTree<Token, Node>,
                    Vec<::#lr_parser_crate_name::syntax_tree::SyntaxError<Token, Node>>
                ) {
                    ::#lr_parser_crate_name::syntax_tree::SyntaxTree::from_parser(&mut self.inner, &Grammar)
                }
            }
        };
        Ok(Self {
            enum_node,
            node_impl,
            struct_impl_grammar: Self::make_grammar(
                info.grammar_map,
                lr_parser_crate_name,
                &info.tokens,
                &info.nodes,
            ),
            transition_table,
            struct_parser,
            parser_impl,
        })
    }

    /// Returns `pub enum Node { ... }` and `impl Node { ... }`
    pub fn make_nodes(
        nodes: &BTreeMap<NodeType, (&NodeInMap, Ident)>,
    ) -> (TokenStream, TokenStream) {
        let enum_node = {
            let nodes = nodes.iter().map(|(_, (node, node_name))| {
                let ignore = if node.is_silent() {
                    quote! { #[doc(hidden)] }
                } else {
                    quote! {}
                };
                let documentation = if let Some(doc) = node.documentation() {
                    quote! { #[doc = #doc] }
                } else {
                    quote! {}
                };
                quote! { #ignore #documentation #node_name }
            });
            quote! {
                #[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
                pub enum Node {
                    #( #nodes, )*
                }
            }
        };
        let nodes_impl = {
            let silent_match_arms = nodes.iter().map(|(_, (node, node_name))| {
                let silent = node.is_silent();
                quote! { Node::#node_name => #silent }
            });
            let display_match_arms = nodes.iter().map(|(_, (node, node_name))| {
                let representation = node.representation();
                quote! { Node::#node_name => #representation }
            });
            quote! { impl Node {
                fn silent(self) -> bool {
                    match self { #( #silent_match_arms, )* }
                }
            }

            impl std::fmt::Display for Node {
                fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
                    f.write_str(match self { #( #display_match_arms, )* })
                }
            } }
        };
        (enum_node, nodes_impl)
    }

    pub fn make_grammar(
        grammar_map: &GrammarMap,
        lr_parser_crate_name: &Ident,
        tokens: &HashMap<TokenType, (&TokenInMap, Ident)>,
        nodes: &BTreeMap<NodeType, (&NodeInMap, Ident)>,
    ) -> TokenStream {
        let grammar_rules = {
            let symbols = grammar_map.grammar().rules().iter().map(|rule| {
                let name;
                let token_or_node = crate::make_ident(match rule {
                    Symbol::Token(token_id) => {
                        name = &tokens[token_id].1;
                        "Token"
                    }
                    Symbol::Node(node_id) => {
                        name = &nodes[node_id].1;
                        "Node"
                    }
                })
                .unwrap();
                quote! { Symbol::#token_or_node(#token_or_node::#name) }
            });
            quote! { mod grammar_rules {
                use ::#lr_parser_crate_name::grammar::Symbol;
                use super::{Token, Node};
                pub(super) static RULES: &[Symbol<Token, Node>] = &[#(#symbols,)*];
            } }
        };

        let fn_tokens = {
            let tokens_names = tokens.values().map(|(_, name)| name);
            quote! { fn tokens(&self) -> Self::TokensIter {
                [
                    #( Token::#tokens_names, )*
                ].iter().copied()
            } }
        };

        let fn_nodes = {
            let nodes_names = nodes.values().map(|(_, name)| name);
            quote! { fn nodes(&self) -> Self::NodesIter {
                [ #( Node::#nodes_names, )* ].iter().copied()
            } }
        };

        let fn_rules = {
            quote! { fn rules(&self) -> &'static  [::#lr_parser_crate_name::grammar::Symbol<Token, Node>] {
                grammar_rules::RULES
            } }
        };

        let fn_rules_indices = {
            let indices = {
                let rules_indices = grammar_map
                    .grammar()
                    .get_all_rules_indices()
                    .iter()
                    .collect::<BTreeMap<_, _>>();
                rules_indices.into_iter().map(|(node, indices)| {
                    let node_name = &nodes[node].1;
                    let indices_start = indices
                        .iter()
                        .map(|(start, _)| Literal::usize_unsuffixed(*start));
                    let indices_end = indices
                        .iter()
                        .map(|(_, end)| Literal::usize_unsuffixed(*end));
                    quote! {
                        Node::#node_name => Some(&[ #( ( #indices_start , #indices_end ) , )* ])
                    }
                })
            };
            quote! { fn rules_indices(&self, node: Node) -> Option<&'static [(usize, usize)]> {
                match node {
                    #( #indices , )*
                    _ => None
                }
            } }
        };

        let fn_conflict_solutions = {
            let make_conflicting_action =
                |action: &ConflictingAction<TokenType, NodeType>| match action {
                    ConflictingAction::Shift(token) => {
                        let token = &tokens[token].1;
                        quote! { ConflictingAction::Shift(Token::#token) }
                    }
                    ConflictingAction::Reduce(rule_idx) => {
                        let lhs = &nodes[&rule_idx.lhs].1;
                        let index = Literal::u16_unsuffixed(rule_idx.index);
                        quote! { ConflictingAction::Reduce(RuleIdx {
                            lhs: Node::#lhs,
                            index: #index,
                        }) }
                    }
                    ConflictingAction::ShiftAny => quote! { ConflictingAction::ShiftAny },
                    ConflictingAction::ReduceNode(node) => {
                        let node = &nodes[node].1;
                        quote! { ConflictingAction::ReduceNode(Node::#node) }
                    }
                };
            let solutions = grammar_map
                .grammar()
                .conflict_solutions()
                .iter()
                .map(|solution| {
                    let prefer = make_conflicting_action(&solution.prefer);
                    let over = make_conflicting_action(&solution.over);
                    quote! { ConflictSolution {
                        prefer: #prefer,
                        over: #over,
                    } }
                });

            quote! { fn conflict_solutions(&self) -> &'static [::#lr_parser_crate_name::grammar::ConflictSolution<Token, Node>] {
                use ::#lr_parser_crate_name::grammar::{ConflictSolution, ConflictingAction, RuleIdx};
                &[ #( #solutions, )* ]
            } }
        };

        let grammar_doc = format!(
            "Structure that implements [`lr_parser::grammar::Grammar`](::{}::grammar::Grammar).",
            lr_parser_crate_name
        );

        quote! {
        #[doc = #grammar_doc]
        pub struct Grammar;

        #grammar_rules

        impl ::#lr_parser_crate_name::grammar::Grammar for Grammar {
            type Token = Token;
            type Node = Node;
            type TokensIter = std::iter::Copied<std::slice::Iter<'static, Token>>;
            type NodesIter = std::iter::Copied<std::slice::Iter<'static, Node>>;

            #fn_tokens

            #fn_nodes

            #fn_rules

            #fn_rules_indices

            #fn_conflict_solutions
        }
        }
    }

    /// Returns the `TransitionTable` structure, as well as the computed `LR1Table`.
    fn make_transition_table(
        grammar_map: &GrammarMap,
        lr_parser_crate_name: &Ident,
        tokens: &HashMap<TokenType, (&TokenInMap, Ident)>,
        nodes: &BTreeMap<NodeType, (&NodeInMap, Ident)>,
    ) -> (TokenStream, canonical_lr::LR1Table<TokenType, NodeType>) {
        let starting_nodes = nodes.iter().filter_map(|(node_id, (node, _))| {
            if node.is_silent() {
                None
            } else {
                Some(*node_id)
            }
        });
        let lr1_table = match CanonicalLR::new()
            .compute_states_with_starts(starting_nodes, grammar_map.grammar())
        {
            Ok(tables) => tables,
            Err(error) => {
                use codespan_reporting::term::termcolor::{ColorChoice, StandardStream};

                crate::helpers::ComputeErrorDisplay::new(error)
                    .write_on(&mut StandardStream::stderr(ColorChoice::Always))
                    .unwrap();
                panic!()
            }
        };
        let states_names = lr1_table
            .states()
            .map(|state| format_ident!("S{}", state))
            .collect::<Vec<_>>();

        let fn_starting_state = {
            let start_states = nodes.iter().filter_map(|(node_id, (_, node_name))| {
                lr1_table.starting_state(*node_id).map(|state| {
                    let state = &states_names[state];
                    quote! { Node::#node_name => Some(#state) }
                })
            });
            quote! { fn starting_state(&self, node: Self::Node) -> Option<Self::State> {
                use ParserState::*;
                match node {
                    #(#start_states,)*
                    _ => None
                }
            } }
        };

        let fn_states = quote! { fn states(&self) -> Self::StatesIter {
            use ParserState::*;
            [#( #states_names, )*].iter().copied()
        } };

        let fn_state_rules = {
            let match_arms = states_names.iter().enumerate().map(|(state, state_name)| {
                let mut rules = lr1_table.state_items(state).iter().collect::<Vec<_>>();
                rules.sort_unstable();
                let rules = rules
                    .into_iter()
                    .map(|indexed_rule| Self::quote_indexed_rules(indexed_rule, tokens, nodes));
                quote! { #state_name => &[#( #rules, )*] }
            });

            quote! { fn state_rules(&self, state: Self::State) -> &[#lr_parser_crate_name::compute::IndexedRule<Token, Node>] {
                use #lr_parser_crate_name::{compute::IndexedRule, grammar::{Lookahead, RuleIdx}};
                // This function is usually quite big, so we add `use` and functions to reduce the memory footprint.
                use Node as N;
                use Token as T;
                use Lookahead::{Token as LookT, Eof};
                use ParserState::*;
                #[allow(non_snake_case)]
                macro_rules! Norm {
                    ($lhs:expr, $rule_idx_index:expr, $index:expr, $lookahead:expr) => {
                        IndexedRule::Normal {
                            rule_idx: RuleIdx { lhs: $lhs, index: $rule_idx_index }, index: $index, lookahead: $lookahead
                        }
                    };
                }
                match state {
                    #( #match_arms, )*
                }
            } }
        };

        let fn_available_lookaheads = {
            let match_arms = states_names.iter().enumerate().map(|(state, state_name)| {
                let lookaheads =
                    lr1_table
                        .available_lookaheads(state)
                        .map(|lookahead| match lookahead {
                            Lookahead::Eof => quote! { Eof },
                            Lookahead::Token(token) => {
                                let token_name = &tokens[&token].1;
                                quote! { LookT(T::#token_name) }
                            }
                        });
                quote! { #state_name => &[#( #lookaheads, )*] as &[_] }
            });
            quote! { fn available_lookaheads(&self, state: Self::State) -> Self::Lookaheads {
                use #lr_parser_crate_name::grammar::Lookahead;
                use Token as T;
                use Lookahead::{Token as LookT, Eof};
                use ParserState::*;
                match state {
                    #( #match_arms, )*
                }.iter().copied()
            } }
        };

        let fn_action = {
            let match_arms = (0..states_names.len())
                .map(|state| {
                    lr1_table
                        .available_lookaheads(state)
                        .map(move |lookahead| (state, lookahead))
                })
                .flatten()
                .collect::<BTreeSet<_>>()
                .into_iter()
                .filter_map(|(state, lookahead)| {
                    let action = lr1_table.action(state, lookahead)?;
                    let action = match action {
                        LR1Action::Shift { state } => {
                            let state = &states_names[state];
                            quote! { Shift { state: #state } }
                        }
                        LR1Action::Reduce {
                            rhs_size,
                            rule_index,
                        } => {
                            let lhs = &nodes[&rule_index.lhs].1;
                            let rhs_size = Literal::u16_unsuffixed(rhs_size);
                            let index = Literal::u16_unsuffixed(rule_index.index);
                            quote! { Reduce {
                                rhs_size: #rhs_size,
                                rule_index: RuleIdx { lhs: Node::#lhs, index: #index }
                            } }
                        }
                        LR1Action::Accept(accept) => {
                            let accept = &nodes[&accept].1;
                            quote! { Accept(Node::#accept) }
                        }
                    };
                    let state = &states_names[state];
                    let lookahead = match lookahead {
                        Lookahead::Eof => quote! { Eof },
                        Lookahead::Token(token) => {
                            let token = &tokens[&token].1;
                            quote! { LookT(Token::#token) }
                        }
                    };
                    Some(quote! {
                        (#state, #lookahead) => Some(#action)
                    })
                });

            quote! { fn action(
                &self,
                state: Self::State,
                lookahead: ::#lr_parser_crate_name::grammar::Lookahead<Token>,
            ) -> Option<::#lr_parser_crate_name::compute::LR1Action<Node, Self::State>> {
                use ::#lr_parser_crate_name::{grammar::{Lookahead, RuleIdx}, compute::LR1Action};
                use ParserState::*;
                use LR1Action::{Shift, Reduce, Accept};
                use Lookahead::{Token as LookT, Eof};
                match (state, lookahead) {
                    #( #match_arms, )*
                    _ => None
                }
            } }
        };

        let fn_goto = {
            let match_arms = lr1_table
                .goto_table()
                .iter()
                .map(|((state, node), goto_state)| {
                    let state = &states_names[*state];
                    let node = &nodes[node].1;
                    let goto_state = &states_names[*goto_state];
                    quote! { (#state, Node::#node) => Some(#goto_state) }
                });
            quote! { fn goto(&self, state: Self::State, node: Node) -> Option<Self::State> {
                use ParserState::*;
                match (state, node) {
                    #( #match_arms, )*
                    _ => None
                }
            } }
        };

        let transition_table_doc = format!("Transition table for the [`Parser`].\n\nThis implements the [`lr_parser::compute::TransitionTable`](::{}::compute::TransitionTable) trait.", lr_parser_crate_name);

        (
            quote! {
            #[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
            pub enum ParserState {
                #(#states_names,)*
            }

            #[doc = #transition_table_doc]
            pub struct TransitionTable;

            impl ::#lr_parser_crate_name::compute::TransitionTable for TransitionTable {
                type State = ParserState;
                type Token = Token;
                type Node = Node;
                type StatesIter = std::iter::Copied<std::slice::Iter<'static, ParserState>>;
                type Lookaheads = std::iter::Copied<std::slice::Iter<'static, ::#lr_parser_crate_name::grammar::Lookahead<Token>>>;

                #fn_starting_state

                #fn_states

                #fn_state_rules

                #fn_available_lookaheads

                #fn_action

                #fn_goto
            }
            },
            lr1_table,
        )
    }

    /// Returns the quoted form of an [`LR1Item`].
    ///
    /// `Node` will be named `N`, and `Token` will be named `T`.
    fn quote_indexed_rules(
        indexed_rule: &LR1Item<TokenType, NodeType>,
        tokens: &HashMap<TokenType, (&TokenInMap, Ident)>,
        nodes: &BTreeMap<NodeType, (&NodeInMap, Ident)>,
    ) -> TokenStream {
        let lhs = &nodes[&indexed_rule.rule_idx.lhs].1;
        let rule_idx_index = Literal::u16_unsuffixed(indexed_rule.rule_idx.index);
        let index = Literal::u16_unsuffixed(indexed_rule.index);
        let lookahead = match indexed_rule.lookahead {
            Lookahead::Eof => {
                quote! { Eof }
            }
            Lookahead::Token(t) => {
                let t_name = &tokens[&t].1;
                quote! { LookT(T::#t_name) }
            }
        };
        quote! { Norm!(
            N::#lhs, #rule_idx_index, #index, #lookahead
        ) }
    }
}

impl fmt::Display for Parser {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}\n\n{}\n\n{}\n\n{}\n\n{}\n\n{}",
            self.enum_node,
            self.node_impl,
            self.struct_impl_grammar,
            self.transition_table,
            self.struct_parser,
            self.parser_impl
        )
    }
}
