use canonical_lr::CanonicalLR;
use codespan_reporting::term::termcolor::{ColorChoice, StandardStream};
use high_level::{GrammarFiles, GrammarMap, NodeInMap, NodeType, TokenInMap, TokenType};
use proc_macro2::{Ident, Span};
use quote::format_ident;
use std::{
    collections::{BTreeMap, HashMap},
    path::{Path, PathBuf},
};

mod ast;
mod helpers;
mod parser;
mod tokens;

/// Various informations needed by the processing functions.
struct Info<'map> {
    grammar_map: &'map GrammarMap,
    lr_parser_crate_name: Ident,
    logos_crate_name: Ident,
    tokens: HashMap<TokenType, (&'map TokenInMap, Ident)>,
    tokens_max_leading_underscores: usize,
    nodes: BTreeMap<NodeType, (&'map NodeInMap, Ident)>,
}

#[derive(Debug)]
pub enum Error {
    ForbiddenIdentifier(&'static str),
}

type Result<T> = std::result::Result<T, Error>;

impl<'map> Info<'map> {
    fn new(
        grammar_map: &'map GrammarMap,
        lr_parser_crate_name: &str,
        logos_crate_name: &str,
    ) -> Result<Self> {
        let lr_parser_crate_name = make_ident(lr_parser_crate_name)?;
        let logos_crate_name = make_ident(logos_crate_name)?;
        let mut tokens_max_leading_underscores = 0usize;
        let tokens = grammar_map
            .tokens()
            .tokens
            .iter()
            .map(|token| {
                let name = token.name();
                let leading_underscores = name.len() - name.trim_start_matches('_').len();
                tokens_max_leading_underscores =
                    std::cmp::max(tokens_max_leading_underscores, leading_underscores);
                make_ident(name).map(|name| (token.id(), (token, name)))
            })
            .collect::<Result<HashMap<_, _>>>()?;
        let nodes = grammar_map
            .nodes()
            .map(|node| {
                let id = node.id();
                Ok((
                    id,
                    (
                        node,
                        match node.name() {
                            Some(name) => make_ident(name)?,
                            None => format_ident!("_{}", id),
                        },
                    ),
                ))
            })
            .collect::<Result<BTreeMap<NodeType, _>>>()?;
        Ok(Self {
            grammar_map,
            lr_parser_crate_name,
            logos_crate_name,
            tokens,
            tokens_max_leading_underscores,
            nodes,
        })
    }
}

pub fn generate_file(
    root_path: PathBuf,
    lr_parser_crate_name: &str,
    logos_crate_name: &str,
) -> Result<String> {
    let grammar_map = get_grammar_map(root_path);
    let info = Info::new(&grammar_map, lr_parser_crate_name, logos_crate_name)?;

    let tokens = tokens::Tokens::new(&info)?;
    let parser = parser::Parser::new(&info)?;
    let ast = ast::AstStructs::new(&info)?;
    Ok(format!(
        "{}\n\n{}\n\n{}",
        tokens,
        parser,
        ast.display(&info)
    ))
}

fn get_grammar_map(root_path: PathBuf) -> GrammarMap {
    let mut grammar_files = GrammarFiles::new();

    let canonical_lr = CanonicalLR::new();
    match GrammarMap::from_root_path(&mut grammar_files, root_path, canonical_lr) {
        Ok((map, warnings)) => {
            for warning in warnings {
                warning
                    .display(&grammar_files)
                    .write_on(&mut StandardStream::stderr(ColorChoice::Always))
                    .unwrap();
            }
            map
        }
        Err(err) => {
            err.display(&grammar_files)
                .write_on(&mut StandardStream::stderr(ColorChoice::Always))
                .unwrap();
            panic!()
        }
    }
}

pub fn run_rustfmt(input: &Path) {
    use std::{env, process::Command};
    let rustfmt_path = {
        match env::var_os("RUSTFMT") {
            Some(which) => {
                if which.is_empty() {
                    PathBuf::from("rustfmt")
                } else {
                    PathBuf::from(which)
                }
            }
            None => PathBuf::from("rustfmt"),
        }
    };
    let output = Command::new(&rustfmt_path)
        .arg("--edition=2018")
        .arg(input)
        .output();
    match output {
        Ok(output) => {
            if !output.status.success() {
                let stderr = String::from_utf8_lossy(&output.stderr);
                eprintln!("Error while running rustfmt:\n{}", stderr)
            }
        }
        Err(err) => eprintln!("Error while running rustfmt:\n{}", err),
    };
}

/// Check that the given identifier is not a forbidden Rust keyword.
fn check_ident(ident: &str) -> Result<()> {
    // Those can't be raw identifiers, and so are outright forbidden
    const FORBIDDEN_RUST_KEYWORDS: &[&str] = &["crate", "extern", "Self", "self", "super"];

    if let Some(forbidden) = FORBIDDEN_RUST_KEYWORDS
        .iter()
        .find(|keyword| **keyword == ident)
    {
        Err(Error::ForbiddenIdentifier(forbidden))
    } else {
        Ok(())
    }
}

/// Make an identifier, escaping Rust keywords if necessary.
fn make_ident(ident: &str) -> Result<Ident> {
    const RUST_KEYWORDS: &[&str] = &[
        "as", "async", "await", "break", "const", "continue", "dyn", "else", "enum", "false", "fn",
        "for", "if", "impl", "in", "let", "loop", "match", "mod", "move", "mut", "pub", "ref",
        "return", "static", "struct", "trait", "true", "type", "union", "unsafe", "use", "where",
        "while",
    ];
    check_ident(ident)?;
    let is_rust_keyword = RUST_KEYWORDS.contains(&ident);
    Ok(if is_rust_keyword {
        quote::format_ident!("r#{}", ident)
    } else {
        Ident::new(ident, Span::call_site())
    })
}
