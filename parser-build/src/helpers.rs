use canonical_lr::{ComputeError, ComputeErrorKind};
use codespan_reporting::term::termcolor::WriteColor;
use high_level::{NodeType, TokenType};

pub(crate) struct ComputeErrorDisplay {
    error: ComputeError<TokenType, NodeType>,
}

impl ComputeErrorDisplay {
    pub(crate) fn new(error: ComputeError<TokenType, NodeType>) -> Self {
        Self { error }
    }

    pub fn write_on(
        self,
        writer: &mut dyn WriteColor,
    ) -> Result<(), codespan_reporting::files::Error> {
        match &self.error.kind {
            ComputeErrorKind::CyclicConflictResolution { .. } => todo!(),
            ComputeErrorKind::TooManyStates(number) => write!(
                writer,
                "Computation of the parser states reached the limit of {} states",
                number
            )?,
            ComputeErrorKind::Conflict(_) => {
                todo!()
            }
        }
        Ok(())
    }
}
