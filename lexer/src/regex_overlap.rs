use regex_automata::{RegexBuilder, DFA};
use std::collections::HashSet;

pub(super) fn get_dfa(pattern: &str) -> Result<impl DFA, regex_automata::Error> {
    let mut builder = RegexBuilder::new();
    builder.minimize(true).anchored(true);
    Ok(builder.build_with_size::<u32>(pattern)?.forward().clone())
}

/// Returns `true` if the sets matched by `dfa1` and `dfa2` overlap.
pub(super) fn dfa_overlap(dfa1: &impl DFA, dfa2: &impl DFA) -> bool {
    let mut visited_states = HashSet::new();
    let mut to_process = vec![(dfa1.start_state(), dfa2.start_state())];
    visited_states.insert((dfa1.start_state(), dfa2.start_state()));
    if dfa1.is_match_state(dfa1.start_state()) && dfa2.is_match_state(dfa2.start_state()) {
        return true;
    }
    while let Some(current_state) = to_process.pop() {
        for input in 0..=u8::MAX {
            let next_state = (
                dfa1.next_state(current_state.0, input),
                dfa2.next_state(current_state.1, input),
            );
            if dfa1.is_match_state(next_state.0) && dfa2.is_match_state(next_state.1) {
                return true;
            }
            if !visited_states.contains(&next_state)
                && !dfa1.is_dead_state(next_state.0)
                && !dfa2.is_dead_state(next_state.1)
            {
                visited_states.insert(next_state);
                to_process.push(next_state);
            }
        }
    }

    false
}

#[cfg(test)]
mod tests {
    use super::*;

    fn regex_overlap(pattern1: &str, pattern2: &str) -> bool {
        dfa_overlap(&get_dfa(pattern1).unwrap(), &get_dfa(pattern2).unwrap())
    }

    #[test]
    fn overlapping_regexes() {
        /*const NUMBER_OF_TOKENS: u32 = 400;
        let overlapping = [
            (get_dfa(r"[a-zA-Z]+").unwrap(), get_dfa(r"[a-z]+").unwrap()),
            (get_dfa(r"a*").unwrap(), get_dfa(r"b*").unwrap()),
            (get_dfa(r"a*bba+").unwrap(), get_dfa(r"b*aaab+a").unwrap()),
            (get_dfa(r" ").unwrap(), get_dfa(r"\s").unwrap()),
            (get_dfa(r"[a-z]+").unwrap(), get_dfa(r"return").unwrap()),
        ];
        let non_overlapping = [
            (get_dfa(r"[A-Z]+").unwrap(), get_dfa(r"[a-z]+").unwrap()),
            (get_dfa(r"a").unwrap(), get_dfa(r"b").unwrap()),
            (get_dfa(r"a*bba+").unwrap(), get_dfa(r"b*aaabbb+a").unwrap()),
            (get_dfa(r"\s+").unwrap(), get_dfa(r"a+").unwrap()),
        ];
        for _ in 0..NUMBER_OF_TOKENS * NUMBER_OF_TOKENS / 2 / 5 {
            for (dfa1, dfa2) in &overlapping {
                assert!(dfa_overlap(dfa1, dfa2))
            }
            for (dfa1, dfa2) in &non_overlapping {
                assert!(!dfa_overlap(dfa1, dfa2))
            }
        }
        for _ in 0..NUMBER_OF_TOKENS {*/
        // OVERLAPPING
        let pattern1 = r"[a-zA-Z]+";
        let pattern2 = r"[a-z]+";
        assert!(regex_overlap(pattern1, pattern2));
        let pattern1 = r"a*";
        let pattern2 = r"b*";
        assert!(regex_overlap(pattern1, pattern2));
        let pattern1 = r"a*bba+";
        let pattern2 = r"b*aaab+a";
        assert!(regex_overlap(pattern1, pattern2));
        let pattern1 = r" ";
        let pattern2 = r"\s";
        assert!(regex_overlap(pattern1, pattern2));
        let pattern1 = r"[a-z]+";
        let pattern2 = r"return";
        assert!(regex_overlap(pattern1, pattern2));
        // NON-OVERLAPPING
        let pattern1 = r"[A-Z]+";
        let pattern2 = r"[a-z]+";
        assert!(!regex_overlap(pattern1, pattern2));
        let pattern1 = r"a";
        let pattern2 = r"b";
        assert!(!regex_overlap(pattern1, pattern2));
        let pattern1 = r"a*bba+";
        let pattern2 = r"b*aaabbb+a";
        assert!(!regex_overlap(pattern1, pattern2));
        let pattern1 = r"\s+";
        let pattern2 = r"a+";
        assert!(!regex_overlap(pattern1, pattern2));
        //}
    }
}
