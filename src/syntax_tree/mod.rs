//! Construction of a syntax tree from a [parser](LR1Parser).
//!
//! This uses the [`RecursiveReduce`] strategy to deal with parsing errors.

// Ideas from rust-analyzer:
// - during construction, keep trivia (Skip tokens) aside, and only consume them when ... reducing a node ?

pub mod abstract_tree;
mod display;
mod errors;

pub use self::{display::SyntaxElementDisplay, errors::SyntaxError};

use crate::{
    compute::TransitionTable,
    grammar::{Grammar, Lookahead, RuleIdx},
    parser::{
        recover_strategy::RecursiveReduce, ParseAction, ParseError, RemappedToken, TokenKind,
    },
    text_ref::{Span, TextReference},
    LR1Parser,
};
use std::sync::Arc;

/// Syntax tree built from a [parser](LR1Parser).
#[derive(Clone, Debug, Eq, PartialEq, PartialOrd, Ord, Hash)]
pub struct SyntaxElement<T, N, Ref: TextReference> {
    /// Text represented by this element.
    pub text_ref: Ref,
    /// Node or token.
    pub kind: NodeOrToken<T, N, Ref>,
}

#[derive(Clone, Debug, Eq, PartialEq, PartialOrd, Ord, Hash)]
pub enum NodeOrToken<T, N, Ref: TextReference> {
    /// A normal token.
    Token(T),
    /// A token that was marked as `skip`.
    Skip(T),
    /// Token that was marked as [error](TokenKind::Error), or was unexpected.
    ErrorToken(Option<T>),
    Node {
        /// Node kind, and reference in the [grammar](crate::grammar::Grammar).
        rule: RuleIdx<N>,
        /// Node's children.
        children: Vec<Arc<SyntaxElement<T, N, Ref>>>,
    },
}

impl<T, N, Ref: TextReference> SyntaxElement<T, N, Ref> {
    /// Get the text corresponding to this element.
    pub fn get_text(&self) -> &str {
        self.text_ref.as_ref()
    }

    /// Returns the span of this element in source code.
    pub fn span(&self) -> Span {
        self.text_ref.get_span()
    }
}

impl<T, N, Ref: TextReference> SyntaxElement<T, N, Ref>
where
    T: Eq + Copy,
    N: Eq + Copy,
{
    /// Build the syntax tree from the given `parser`.
    pub fn from_parser<'remap, Tokenizer, Tables, Silent, Remap, G>(
        source: Ref,
        parser: &mut LR1Parser<Tokenizer, Tables, Silent, Remap>,
        grammar: &G,
        start_node: N,
    ) -> (Arc<Self>, Vec<SyntaxError<T, N, Ref>>)
    where
        Tokenizer: Iterator<Item = (TokenKind<T>, Span)>,
        Tables: TransitionTable<Token = T, Node = N>,
        Silent: FnMut(N) -> bool,
        Remap: Fn(T) -> RemappedToken<'remap, T>,
        G: Grammar<Token = T, Node = N>,
        T: 'remap,
    {
        let mut stack = Vec::new();
        let mut errors = Vec::new();

        while let Some(result) = parser.next() {
            match result {
                Ok(action) => Self::parse_action(&source, &mut stack, parser, action),
                Err(error) => {
                    if Self::parse_error(&source, &mut stack, &mut errors, error, parser, grammar) {
                        break;
                    }
                }
            }
        }

        let root = if stack.len() != 1 {
            let mut root = match stack.first().map(|arc| arc.as_ref()) {
                Some(SyntaxElement {
                    kind: NodeOrToken::Node { rule, .. },
                    ..
                }) if rule.lhs == start_node => match Arc::try_unwrap(stack.remove(0)) {
                    Ok(root) => root,
                    Err(_) => {
                        unreachable!()
                    }
                },
                _ => SyntaxElement {
                    text_ref: source,
                    kind: NodeOrToken::Node {
                        rule: RuleIdx {
                            lhs: start_node,
                            index: 0,
                        },
                        children: Vec::new(),
                    },
                },
            };
            if let NodeOrToken::Node { children, .. } = &mut root.kind {
                *children = stack;
            }
            Arc::new(root)
        } else {
            stack.pop().unwrap()
        };

        (root, errors)
    }
}

impl<'remap, T, N, Ref: TextReference> SyntaxElement<T, N, Ref>
where
    T: Eq + Copy + 'remap,
    N: Eq + Copy,
{
    fn parse_action<Tokenizer, Tables, Silent, Remap>(
        source: &Ref,
        stack: &mut Vec<Arc<Self>>,
        parser: &mut LR1Parser<Tokenizer, Tables, Silent, Remap>,
        action: ParseAction<T, N>,
    ) where
        Tokenizer: Iterator<Item = (TokenKind<T>, Span)>,
        Tables: TransitionTable<Token = T, Node = N>,
        Silent: FnMut(N) -> bool,
        Remap: Fn(T) -> RemappedToken<'remap, T>,
    {
        match action {
            ParseAction::Shift { kind, span } => stack.push(Arc::new(Self {
                text_ref: source.new_subspan(span),
                kind: NodeOrToken::Token(kind),
            })),
            ParseAction::Skip { kind, span } => stack.push(Arc::new(Self {
                text_ref: source.new_subspan(span),
                kind: NodeOrToken::Skip(kind),
            })),
            ParseAction::Reduce {
                mut size,
                rule_index,
                ..
            } => {
                let mut children = Vec::new();
                let mut node_span: Option<Span> = None;
                while size > 0 {
                    if let Some(child) = stack.pop() {
                        match child.as_ref().kind {
                            NodeOrToken::Node { .. } | NodeOrToken::Token(..) => {
                                let span = child.span();
                                match &mut node_span {
                                    Some(node_span) => {
                                        node_span.start = span.start;
                                    }
                                    None => node_span = Some(span.clone()),
                                }
                                size -= 1;
                            }
                            _ => {}
                        }
                        children.push(child);
                    } else {
                        size -= 1;
                    }
                }
                let node_span = node_span
                    .or_else(|| {
                        parser
                            .next_span()
                            .map(|Span { start, end: _ }| start..start)
                    })
                    .unwrap_or_else(|| {
                        let pos = parser.current_span().end;
                        pos..pos
                    });
                // take remaining `Skip`
                while let Some(child) = stack.pop() {
                    if !matches!(
                        child.as_ref().kind,
                        NodeOrToken::Skip(..) | NodeOrToken::ErrorToken(..)
                    ) {
                        stack.push(child);
                        break;
                    }
                    children.push(child);
                }
                children.reverse();
                stack.push(Arc::new(Self {
                    text_ref: source.new_subspan(node_span),
                    kind: NodeOrToken::Node {
                        rule: rule_index,
                        children,
                    },
                }));
            }
            ParseAction::SilentReduce { .. } => {}
        }
    }

    /// Decide the course of action from the given error.
    ///
    /// Returns `true` when the parsing must stop.
    fn parse_error<Tokenizer, Tables, Silent, Remap, G>(
        source: &Ref,
        stack: &mut Vec<Arc<Self>>,
        errors: &mut Vec<SyntaxError<T, N, Ref>>,
        error: ParseError<T, Tables::State>,
        parser: &mut LR1Parser<Tokenizer, Tables, Silent, Remap>,
        grammar: &G,
    ) -> bool
    where
        Tokenizer: Iterator<Item = (TokenKind<T>, Span)>,
        Tables: TransitionTable<Token = T, Node = N>,
        Silent: FnMut(N) -> bool,
        Remap: Fn(T) -> RemappedToken<'remap, T>,
        G: Grammar<Token = T, Node = N>,
    {
        match error {
            ParseError::Tokenizer(span) => {
                parser.next_token();
                stack.push(Arc::new(Self {
                    text_ref: source.new_subspan(span),
                    kind: NodeOrToken::ErrorToken(None),
                }));
                false
            }
            ParseError::IncorrectToken(error) => {
                let expected = parser.expected_tokens(&error).collect::<Vec<_>>();
                if error.token.is_eof() {
                    // if this is EOF, we should stop.
                    // This actually avoid an infinite loop :)
                    errors.push(SyntaxError::Unexpected {
                        token: Lookahead::Eof,
                        expected,
                    });
                    return true;
                }
                match parser.apply_strategy::<RecursiveReduce, _>(&error, grammar) {
                    None => {
                        if let Some((token_kind, span)) = parser.next_token() {
                            stack.push(Arc::new(Self {
                                text_ref: source.new_subspan(span),
                                kind: NodeOrToken::ErrorToken(match token_kind {
                                    TokenKind::Normal(kind) | TokenKind::Skip(kind) => Some(kind),
                                    TokenKind::Error => None,
                                }),
                            }));
                        }
                        errors.push(SyntaxError::Unexpected {
                            token: error
                                .token
                                .map(|(token, span)| (token, source.new_subspan(span))),
                            expected,
                        });
                        false
                    }
                    Some((item, complete_rule)) => {
                        if !complete_rule {
                            let (token, span) = match error.token {
                                Lookahead::Eof => {
                                    let pos = parser.current_span().end;
                                    (Lookahead::Eof, pos..pos)
                                }
                                Lookahead::Token((t, span)) => (Lookahead::Token(t), span),
                            };
                            errors.push(SyntaxError::IncompleteNode {
                                node: item.node(),
                                token,
                                text_ref: source.new_subspan(span),
                                expected,
                            })
                        }
                        Self::parse_action(source, stack, parser, item.into());
                        false
                    }
                }
            }
        }
    }
}
