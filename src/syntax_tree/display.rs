use crate::grammar::RuleIdx;

use super::{NodeOrToken, SyntaxElement};
use crate::text_ref::TextReference;
use std::fmt::{self, Display, Write};

impl<T, N, Ref: TextReference> SyntaxElement<T, N, Ref> {
    /// Returns an object that implements [`Display`].
    ///
    /// It will use `print_node`/`print_token` to render a node/token.
    ///
    /// See [`SyntaxElementDisplay`] to see the displaying options.
    pub fn display<'tree, 'print>(
        &'tree self,
        print_node: &'print dyn Fn(&mut fmt::Formatter<'_>, &RuleIdx<N>) -> fmt::Result,
        print_token: &'print dyn Fn(&mut fmt::Formatter<'_>, &T) -> fmt::Result,
    ) -> SyntaxElementDisplay<'tree, 'print, T, N, Ref> {
        SyntaxElementDisplay {
            root: self,
            max_node_length: 40,
            print_node,
            print_token,
            print_skip_tokens: true,
        }
    }
}

/// Helper structure to display a [`SyntaxElement`].
#[derive(Clone, Copy)]
pub struct SyntaxElementDisplay<'tree, 'print, T, N, Ref: TextReference> {
    root: &'tree SyntaxElement<T, N, Ref>,
    print_node: &'print dyn Fn(&mut fmt::Formatter<'_>, &RuleIdx<N>) -> fmt::Result,
    print_token: &'print dyn Fn(&mut fmt::Formatter<'_>, &T) -> fmt::Result,
    print_skip_tokens: bool,
    max_node_length: usize,
}

impl<T, N, Ref: TextReference> SyntaxElementDisplay<'_, '_, T, N, Ref> {
    /// Sets the maximum length for a node.
    ///
    /// Nodes that exceed this length will be displayed as `...`.
    ///
    /// Default: `40`
    pub fn set_max_node_length(&mut self, length: usize) -> &mut Self {
        self.max_node_length = length;
        self
    }

    /// Sets whether or not token marked as 'skip' (usually whitespace) get printed.
    ///
    /// Default: `true`
    pub fn print_skip_tokens(&mut self, b: bool) -> &mut Self {
        self.print_skip_tokens = b;
        self
    }

    fn print_and_add_children<'elem>(
        &self,
        element: &'elem SyntaxElement<T, N, Ref>,
        mut tree_markers: String,
        last_item: bool,
        // TODO: replace Arc with reference
        stack: &mut Vec<(&'elem SyntaxElement<T, N, Ref>, String, bool)>,
        f: &mut fmt::Formatter,
    ) -> fmt::Result {
        let span = element.span();
        match &element.kind {
            NodeOrToken::Token(token) => {
                f.write_str(&tree_markers)?;
                (self.print_token)(f, token)?;
                f.write_char(' ')?;
                writeln!(
                    f,
                    "@{}..{} ({:?})",
                    span.start,
                    span.end,
                    element.text_ref.as_ref()
                )
            }
            NodeOrToken::Skip(token) => {
                if self.print_skip_tokens {
                    f.write_str(&tree_markers)?;
                    (self.print_token)(f, token)?;
                    f.write_char(' ')?;
                    writeln!(f, "@{}..{}", span.start, span.end)
                } else {
                    Ok(())
                }
            }
            NodeOrToken::ErrorToken(token) => {
                f.write_str(&tree_markers)?;
                f.write_str("ERROR ")?;
                match token {
                    Some(token) => (self.print_token)(f, token),
                    None => f.write_str("UNKNOWN"),
                }?;
                writeln!(
                    f,
                    " @{}..{} ({:?})",
                    span.start,
                    span.end,
                    element.text_ref.as_ref()
                )
            }
            NodeOrToken::Node { rule, children } => {
                f.write_str(&tree_markers)?;
                if !tree_markers.is_empty() {
                    tree_markers.pop();
                    tree_markers.pop();
                    if last_item {
                        tree_markers.push_str("  ");
                    } else {
                        tree_markers.push_str("│ ");
                    }
                }

                (self.print_node)(f, rule)?;
                f.write_char(' ')?;
                write!(f, "@{}..{} ", span.start, span.end)?;
                if span.end - span.start <= self.max_node_length {
                    writeln!(f, "({:?})", element.text_ref.as_ref())
                } else {
                    f.write_str("(...)\n")
                }?;
                let mut children = children.iter().rev();
                let last_child = loop {
                    let last_child = children.next();
                    match last_child {
                        None => break None,
                        Some(child) => {
                            if self.print_skip_tokens
                                || !matches!(&child.kind, NodeOrToken::Skip(..))
                            {
                                break last_child;
                            }
                        }
                    }
                };
                if let Some(last_child) = last_child {
                    stack.push((last_child.as_ref(), tree_markers.clone() + "└ ", true));
                }
                for child in children {
                    stack.push((child, tree_markers.clone() + "├ ", false));
                }

                Ok(())
            }
        }
    }
}

impl<T, N, Ref: TextReference> Display for SyntaxElementDisplay<'_, '_, T, N, Ref> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut stack = Vec::new();
        self.print_and_add_children(self.root, String::new(), true, &mut stack, f)?;
        while let Some((element, tree_markers, last_item)) = stack.pop() {
            self.print_and_add_children(element, tree_markers, last_item, &mut stack, f)?;
        }
        Ok(())
    }
}
