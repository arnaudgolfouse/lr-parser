//! Utilities for building abstract trees from a [`SyntaxElement`].

use super::{NodeOrToken, SyntaxElement};
use crate::{
    grammar::{RuleIdx, Symbol},
    text_ref::TextReference,
};
use std::{convert::TryFrom, iter::Peekable, sync::Arc};

/// Symbol encountered in [`ConversionError`]
#[derive(Clone, Debug, Eq, PartialEq)]
pub enum EncounteredSymbol<T, N, Ref: TextReference> {
    /// A normal symbol.
    Normal(Symbol<T, N>, Ref),
    /// The error token.
    Error(Ref),
    /// The End Of File.
    Eof,
}

#[derive(Clone, Debug, Eq, PartialEq)]
/// Error encountered when converting a [`SyntaxElement`] to an abstract tree.
pub struct ConversionError<T, N, Ref: TextReference> {
    /// Unexpected encountered symbol
    pub encountered: EncounteredSymbol<T, N, Ref>,
    /// Eventual expected symbol
    pub expected: Option<Symbol<T, N>>,
}

/// Indicate that the AST building should **not** crash on this error.
pub trait ContinuableError {
    fn continuable(&self) -> bool;
}

impl<T, N, Ref: TextReference> ContinuableError for ConversionError<T, N, Ref> {
    fn continuable(&self) -> bool {
        true
    }
}

impl<T: 'static, N, Ref: TextReference> SyntaxElement<T, N, Ref>
where
    T: Eq + Copy,
    N: Eq + Copy,
{
    /// If `self` is a [`Node`](NodeOrToken::Node) of type `node`, returns its
    /// index in the grammar and an iterator over its children.
    pub fn expect_node(
        &self,
        node: N,
    ) -> Result<(RuleIdx<N>, impl Iterator<Item = &Arc<Self>> + '_), ConversionError<T, N, Ref>>
    {
        match &self.kind {
            NodeOrToken::Node { rule, children } => {
                if rule.lhs == node {
                    Ok((*rule, children.iter()))
                } else {
                    Err(ConversionError {
                        encountered: EncounteredSymbol::Normal(
                            Symbol::Node(rule.lhs),
                            self.text_ref.clone(),
                        ),
                        expected: Some(Symbol::Node(node)),
                    })
                }
            }
            NodeOrToken::Token(token) | NodeOrToken::Skip(token) => Err(ConversionError {
                encountered: EncounteredSymbol::Normal(
                    Symbol::Token(*token),
                    self.text_ref.clone(),
                ),
                expected: Some(Symbol::Node(node)),
            }),
            NodeOrToken::ErrorToken(token) => Err(ConversionError {
                encountered: match token {
                    Some(token) => {
                        EncounteredSymbol::Normal(Symbol::Token(*token), self.text_ref.clone())
                    }
                    None => EncounteredSymbol::Error(self.text_ref.clone()),
                },
                expected: Some(Symbol::Node(node)),
            }),
        }
    }

    /// If `self` is a [`Token`](NodeOrToken::Token), returns its span.
    pub fn expect_token(&self, token: T) -> Result<Ref, ConversionError<T, N, Ref>> {
        match &self.kind {
            NodeOrToken::Token(t) => {
                if *t == token {
                    Ok(self.text_ref.clone())
                } else {
                    Err(ConversionError {
                        encountered: EncounteredSymbol::Normal(
                            Symbol::Token(*t),
                            self.text_ref.clone(),
                        ),
                        expected: Some(Symbol::Token(token)),
                    })
                }
            }
            NodeOrToken::Node { rule, .. } => Err(ConversionError {
                encountered: EncounteredSymbol::Normal(
                    Symbol::Node(rule.lhs),
                    self.text_ref.clone(),
                ),
                expected: Some(Symbol::Token(token)),
            }),
            NodeOrToken::Skip(token) => Err(ConversionError {
                encountered: EncounteredSymbol::Normal(
                    Symbol::Token(*token),
                    self.text_ref.clone(),
                ),
                expected: Some(Symbol::Token(*token)),
            }),
            NodeOrToken::ErrorToken(token) => Err(ConversionError {
                encountered: match token {
                    Some(token) => {
                        EncounteredSymbol::Normal(Symbol::Token(*token), self.text_ref.clone())
                    }
                    None => EncounteredSymbol::Error(self.text_ref.clone()),
                },
                expected: token.map(Symbol::Token),
            }),
        }
    }

    /// Try to consume a single `token` from `children`, and returns it.
    pub fn consume_token<'children>(
        children: &mut dyn Iterator<Item = &'children Arc<Self>>,
        token: T,
    ) -> Result<&'children Arc<Self>, ConversionError<T, N, Ref>>
    where
        T: PartialEq,
    {
        for child in children {
            match &child.kind {
                NodeOrToken::Skip(..) | NodeOrToken::ErrorToken(..) => {}
                NodeOrToken::Node { rule, .. } => {
                    return Err(ConversionError {
                        encountered: EncounteredSymbol::Normal(
                            Symbol::Node(rule.lhs),
                            child.text_ref.clone(),
                        ),
                        expected: Some(Symbol::Token(token)),
                    })
                }
                NodeOrToken::Token(t) => {
                    return if *t == token {
                        Ok(child)
                    } else {
                        Err(ConversionError {
                            encountered: EncounteredSymbol::Normal(
                                Symbol::Token(*t),
                                child.text_ref.clone(),
                            ),
                            expected: Some(Symbol::Token(token)),
                        })
                    }
                }
            }
        }
        Err(ConversionError {
            encountered: EncounteredSymbol::Eof,
            expected: Some(Symbol::Token(token)),
        })
    }

    /// Try to consume a single `token` from `children`.
    pub fn consume_token_opt<'children>(
        children: &mut Peekable<&mut dyn Iterator<Item = &'children Arc<Self>>>,
        token: T,
    ) -> Option<&'children Arc<Self>>
    where
        T: PartialEq,
    {
        while let Some(child) = children.peek().copied() {
            if matches!(&child.kind, NodeOrToken::Skip { .. }) {
                children.next();
            } else {
                break;
            }
        }
        let child = *children.peek()?;
        match &child.kind {
            NodeOrToken::Token(t) => {
                if *t == token {
                    children.next();
                    Some(child)
                } else {
                    None
                }
            }
            _ => None,
        }
    }

    /// Try to consume one child of `children` to make a `Leaf`.
    ///
    /// # Note
    ///
    /// This function will skip [skippable](NodeOrToken::Skip) nodes.
    pub fn consume<'children, 'src, Leaf>(
        children: &mut dyn Iterator<Item = &'children Arc<Self>>,
    ) -> Result<Leaf, <Leaf as TryFrom<&'children Arc<Self>>>::Error>
    where
        Leaf: TryFrom<&'children Arc<Self>>,
        <Leaf as TryFrom<&'children Arc<Self>>>::Error: From<ConversionError<T, N, Ref>>,
    {
        for child in children {
            match &child.kind {
                NodeOrToken::Skip(..) | NodeOrToken::ErrorToken(..) => {}
                NodeOrToken::Token(..) | NodeOrToken::Node { .. } => return Leaf::try_from(child),
            }
        }
        Err(ConversionError {
            encountered: EncounteredSymbol::Eof,
            expected: None,
        }
        .into())
    }

    /// Try to consume one child of `children` to make a `Leaf`.
    ///
    /// If the conversion is impossible, returns [`None`].
    ///
    /// # Note
    ///
    /// This function will skip [skippable](NodeOrToken::Skip) nodes.
    pub fn consume_opt<'children, 'src, Leaf>(
        children: &mut Peekable<&mut dyn Iterator<Item = &'children Arc<Self>>>,
    ) -> Result<Option<Leaf>, <Leaf as TryFrom<&'children Arc<Self>>>::Error>
    where
        Leaf: TryFrom<&'children Arc<Self>>,
        <Leaf as TryFrom<&'children Arc<Self>>>::Error:
            From<ConversionError<T, N, Ref>> + ContinuableError,
    {
        while let Some(child) = children.peek().copied() {
            match &child.kind {
                NodeOrToken::Token(..) | NodeOrToken::Node { .. } => {
                    return match Leaf::try_from(child) {
                        Ok(res) => {
                            children.next();
                            Ok(Some(res))
                        }
                        Err(err) => {
                            if err.continuable() {
                                Ok(None)
                            } else {
                                Err(err)
                            }
                        }
                    }
                }
                NodeOrToken::Skip(..) => {
                    children.next();
                }
                NodeOrToken::ErrorToken(token) => {
                    return Err(ConversionError {
                        encountered: match token {
                            Some(token) => EncounteredSymbol::Normal(
                                Symbol::Token(*token),
                                child.text_ref.clone(),
                            ),
                            None => EncounteredSymbol::Error(child.text_ref.clone()),
                        },
                        expected: None,
                    }
                    .into())
                }
            }
        }
        Ok(None)
    }

    /// Try to consume any number of `children` to make `Leaf`s.
    ///
    /// # Note
    ///
    /// This function will skip [skippable](NodeOrToken::Skip) nodes.
    pub fn consume_repeat<'children, 'src, Leaf, ConsumeOne>(
        children: &mut Peekable<&mut dyn Iterator<Item = &'children Arc<Self>>>,
        mut consume_one: ConsumeOne,
    ) -> Result<Vec<Leaf>, <Leaf as TryFrom<&'children Arc<Self>>>::Error>
    where
        ConsumeOne: FnMut(
            &mut Peekable<&mut dyn Iterator<Item = &Arc<Self>>>,
        )
            -> Result<Option<Leaf>, <Leaf as TryFrom<&'children Arc<Self>>>::Error>,
        Leaf: TryFrom<&'children Arc<Self>>,
        <Leaf as TryFrom<&'children Arc<Self>>>::Error: From<ConversionError<T, N, Ref>>,
    {
        let mut result = vec![];

        while let Some(child) = children.peek() {
            match &child.kind {
                NodeOrToken::Token(..) | NodeOrToken::Node { .. } => match consume_one(children)? {
                    Some(leaf) => result.push(leaf),
                    None => {
                        break;
                    }
                },
                NodeOrToken::Skip(..) => {
                    children.next();
                }
                NodeOrToken::ErrorToken(token) => {
                    return Err(ConversionError {
                        encountered: match token {
                            Some(token) => EncounteredSymbol::Normal(
                                Symbol::Token(*token),
                                child.text_ref.clone(),
                            ),
                            None => EncounteredSymbol::Error(child.text_ref.clone()),
                        },
                        expected: None,
                    }
                    .into())
                }
            }
        }
        Ok(result)
    }

    /// Assert that `children` only contains [skippable](NodeOrToken::Skip) nodes.
    pub fn assert_empty(
        children: &mut dyn Iterator<Item = &Arc<Self>>,
    ) -> Result<(), ConversionError<T, N, Ref>> {
        for child in children {
            match &child.kind {
                NodeOrToken::Skip(..) => {}
                NodeOrToken::Token(t) => {
                    return Err(ConversionError {
                        encountered: EncounteredSymbol::Normal(
                            Symbol::Token(*t),
                            child.text_ref.clone(),
                        ),
                        expected: None,
                    })
                }
                NodeOrToken::Node { rule, .. } => {
                    return Err(ConversionError {
                        encountered: EncounteredSymbol::Normal(
                            Symbol::Node(rule.lhs),
                            child.text_ref.clone(),
                        ),
                        expected: None,
                    })
                }
                NodeOrToken::ErrorToken(t) => {
                    return Err(ConversionError {
                        encountered: match t {
                            Some(token) => EncounteredSymbol::Normal(
                                Symbol::Token(*token),
                                child.text_ref.clone(),
                            ),
                            None => EncounteredSymbol::Error(child.text_ref.clone()),
                        },
                        expected: None,
                    })
                }
            }
        }

        Ok(())
    }
}
