use crate::{grammar::Lookahead, text_ref::TextReference};

/// Error emitted while constructing a [SyntaxElement](super::SyntaxElement).
#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub enum SyntaxError<T, N, Ref: TextReference> {
    /// Placed at the end of a node's [children](super::NodeOrToken::Node::children)
    /// if it was not parsed completely.
    IncompleteNode {
        /// Which node is incomplete
        node: N,
        /// Incorrect token that cause the node's parsing to stop
        token: Lookahead<T>,
        /// Location of this error.
        ///
        /// This is the span of `token`, or the position of the last parsed token
        /// if `token` is `EOF`.
        text_ref: Ref,
        /// List of expected tokens
        expected: Vec<Lookahead<T>>,
    },
    /// The encountered token was unexpected.
    Unexpected {
        /// Unexpected token
        token: Lookahead<(T, Ref)>,
        /// List of expected tokens
        expected: Vec<Lookahead<T>>,
    },
}

impl<T, N, Ref: TextReference> SyntaxError<T, N, Ref> {
    /// Return the reference of the error in the source text
    ///
    /// If the error occurred at `EOF`, returns [`None`].
    pub fn text_ref(&self) -> Option<&Ref> {
        match self {
            SyntaxError::IncompleteNode { text_ref, .. } => Some(text_ref),
            SyntaxError::Unexpected { token, .. } => {
                token.as_ref().map(|(_, text_ref)| text_ref).into()
            }
        }
    }
}
