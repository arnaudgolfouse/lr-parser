/// Nicer syntax for generating a [`Grammar`](super::Grammar).
///
/// # Usage
///
/// Arbitrary rules can be specified with the syntax
/// `A -> { x N(B) y ... }` \
/// where A, B are nodes, and x, y are tokens.
///
/// # Notes
/// - The right hand of a rule may be empty : `A -> {}` is a correct rule.
/// - `A -> { ... } | { ... }` is syntactic sugar for `A -> { ... } A -> { ... }`.
///
/// # Example
/// ```
/// use lr_parser::make_grammar;
/// #[derive(Clone, Copy, Hash, Eq, PartialEq)]
/// enum N {
///     Start, Expr
/// }
///
/// #[derive(Clone, Copy, Hash, Eq, PartialEq)]
/// enum T {
///     add, int
/// }
///
/// use T::*; use N::*;
///
/// let grammar = make_grammar!(
///     Start -> { N(Expr) }
///     Expr -> { N(Expr) add int } | { int }
/// );
/// ```
#[macro_export(local_inner_macros)]
macro_rules! make_grammar {
    (
        $(
            $lhs:ident -> $(
                { $( $rhs:tt )* }
            )|+
        )*
    ) => {
        {
            let mut grammar = $crate::grammar::RuntimeGrammar::new();
            $(
                $(
                    grammar.add_rule(
                        $lhs,
                        __grammar_internal!([] $( $rhs )*)
                    ).unwrap();
                )+
            )*
            grammar
        }
    };
}

#[macro_export(local_inner_macros)]
#[doc(hidden)]
macro_rules! __grammar_internal {
    (
        [ $($processed:tt)* ]
        N($rhs:ident)
        $($other:tt)*
    ) => {
        __grammar_internal!(
            [ $($processed)* Node($rhs) ]
            $($other)*
        )
    };

    (
        [ $($processed:tt)* ]
        $rhs:ident
        $($other:tt)*
    ) => {
        __grammar_internal!(
            [ $($processed)* Token($rhs) ]
            $($other)*
        )
    };

    ([ $( $kind:ident( $rhs:ident ) )* ]) => {
        std::vec![ $( $crate::grammar::Symbol:: $kind($rhs), )* ]
    };
}
