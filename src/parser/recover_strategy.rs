//! Module for recovering from parser errors.
//!
//! To recover from a parser error, you must use a type that implement
//! [`Strategy`], and call its [`apply`] method (or the [`apply_strategy`]
//! method of the parser).
//!
//! # TODO
//!
//! - Add examples to the various strategies.
//! - For now, this has full access to the parser's internal. Custom implementors
//! would not have that.
//! - **do we** want custom implementors ? Probably...
//!
//! [`apply`]: Strategy::apply
//! [`apply_strategy`]: LR1Parser::apply_strategy

use super::{IncorrectToken, LR1Parser, ReduceItem, RemappedToken, StackElement, TokenKind};
use crate::{
    compute::{LR1Item, TransitionTable},
    grammar::{Grammar, RuleIdx},
    text_ref::Span,
};

pub type ReduceResult<Tables> = Result<
    ReduceItem<<Tables as TransitionTable>::Node>,
    IncorrectToken<<Tables as TransitionTable>::Token, Tables>,
>;

/// Make a type suitable for parser error recovery.
pub trait Strategy<'remap, Tokenizer, Tables, Silent, Remap>
where
    Tokenizer: Iterator<Item = (TokenKind<Tables::Token>, Span)>,
    Tables: TransitionTable,
    Silent: FnMut(Tables::Node) -> bool,
    Remap: Fn(Tables::Token) -> RemappedToken<'remap, Tables::Token>,
    Tables::Token: 'remap,
{
    /// Result of the application of the strategy.
    type Result;

    /// Recover from an incorrect token.
    ///
    /// This function should be called after the parser returned an
    /// [`IncorrectToken`](crate::parser::ParseError::IncorrectToken) error.
    fn apply<G: Grammar<Token = Tables::Token, Node = Tables::Node>>(
        parser: &mut LR1Parser<Tokenizer, Tables, Silent, Remap>,
        error: &IncorrectToken<Tables::Token, Tables::State>,
        grammar: &G,
    ) -> Self::Result;
}

/// Parser error recovery strategy.
///
/// This simply consumes the next token.
///
/// [`apply`](SkipToken::apply) returns [`false`] is there was no more tokens to
/// consume.
pub struct SkipToken;

/// Parser error recovery strategy.
///
/// This will try to unambiguously reduce the first part of a rule that expects
/// the next token.
pub struct PartialReduce;

/// Parser error recovery strategy.
///
/// This will try to reduce the first part of a rule that expects the next
/// token, like [`PartialReduce`].
///
/// If multiple reductions apply, one is chosen arbitrarily.
pub struct AlwaysReduce;

/// Parser error recovery strategy.
///
/// This is like [`AlwaysReduce`], but if no rule is found expecting
/// the next token, an arbitrary reduction is chosen anyways.
pub struct AlwaysReduce2;

// TODO : improve this explanation.
/// Parser error recovery strategy.
///
/// This will search for a rule matching the next token unambiguously in all
/// previously parsed states.
pub struct RecursiveReduce;

/// Parser error recovery strategy.
///
/// If all elements of the current state have the form `[A → α⋅, _]` (where
/// `A, α` are the same), apply `A → α`.
pub struct CompleteRule;

impl<T, N: Copy> LR1Item<T, N> {
    fn get_rhs_len<G: Grammar<Token = T, Node = N>>(&self, grammar: &G) -> u16 {
        grammar.get_rhs(self.rule_idx).unwrap_or_default().len() as _
    }
}

impl<'remap, Tokenizer, Tables, Silent, Remap> Strategy<'remap, Tokenizer, Tables, Silent, Remap>
    for SkipToken
where
    Tokenizer: Iterator<Item = (TokenKind<Tables::Token>, Span)>,
    Tables: TransitionTable,
    Silent: FnMut(Tables::Node) -> bool,
    Remap: Fn(Tables::Token) -> RemappedToken<'remap, Tables::Token>,
    Tables::Token: 'remap,
{
    /// [`apply`](Self::apply) returns :
    ///
    /// - [`true`] if a token was consumed.
    /// - [`false`] if there was no more token to consume.
    type Result = bool;

    fn apply<G: Grammar<Token = Tables::Token, Node = Tables::Node>>(
        parser: &mut LR1Parser<Tokenizer, Tables, Silent, Remap>,
        _: &IncorrectToken<Tables::Token, Tables::State>,
        _: &G,
    ) -> Self::Result {
        parser.tokenizer.next().is_some()
    }
}

impl<'remap, Tokenizer, Tables, Silent, Remap> Strategy<'remap, Tokenizer, Tables, Silent, Remap>
    for PartialReduce
where
    Tokenizer: Iterator<Item = (TokenKind<Tables::Token>, Span)>,
    Tables: TransitionTable,
    Silent: FnMut(Tables::Node) -> bool,
    Remap: Fn(Tables::Token) -> RemappedToken<'remap, Tables::Token>,
    Tables::Token: Eq + Copy + 'remap,
    Tables::Node: Eq + Copy,
{
    /// [`apply`](Self::apply) returns :
    ///
    /// - `None` if no rule could be unambiguously applied.
    /// - `Some(result, false)` if a partial rule was applied.
    /// - `Some(result, true)` if a complete rule was applied.
    type Result = Option<(ReduceItem<Tables::Node>, bool)>;

    fn apply<G: Grammar<Token = Tables::Token, Node = Tables::Node>>(
        parser: &mut LR1Parser<Tokenizer, Tables, Silent, Remap>,
        error: &IncorrectToken<Tables::Token, Tables::State>,
        grammar: &G,
    ) -> Self::Result {
        let current_state = parser.get_current_state()?;
        let lookahead = error.token.as_ref().map(|(t, _)| *t);

        let items = parser.tables.state_items(current_state);
        let mut partial_rule: Option<(u16, RuleIdx<_>)> = None;
        let mut entire_rule = false;
        for item in items {
            let rhs_len = item.get_rhs_len(grammar);
            let r_lookahead = item.lookahead;
            let index = item.index;
            if lookahead == r_lookahead {
                entire_rule |= rhs_len == index;
                if let Some((size, rule_idx)) = partial_rule {
                    if rule_idx != item.rule_idx || size != index {
                        return None;
                    }
                } else {
                    partial_rule = Some((index, item.rule_idx));
                }
            }
        }
        partial_rule
            .map(|(rhs_size, rule_index)| (parser.reduce(rhs_size, rule_index), entire_rule))
    }
}

impl<'remap, Tokenizer, Tables, Silent, Remap> Strategy<'remap, Tokenizer, Tables, Silent, Remap>
    for AlwaysReduce
where
    Tokenizer: Iterator<Item = (TokenKind<Tables::Token>, Span)>,
    Tables: TransitionTable,
    Silent: FnMut(Tables::Node) -> bool,
    Remap: Fn(Tables::Token) -> RemappedToken<'remap, Tables::Token>,
    Tables::Token: Eq + Copy + 'remap,
    Tables::Node: Copy,
{
    /// [`apply`](Self::apply) returns :
    ///
    /// - `None` if parsing is over.
    /// - `Some(result, false)` if a partial rule was applied.
    /// - `Some(result, true)` if a complete rule was applied.
    type Result = Option<(ReduceItem<Tables::Node>, bool)>;

    fn apply<G: Grammar<Token = Tables::Token, Node = Tables::Node>>(
        parser: &mut LR1Parser<Tokenizer, Tables, Silent, Remap>,
        error: &IncorrectToken<Tables::Token, Tables::State>,
        grammar: &G,
    ) -> Self::Result {
        let current_state = parser.get_current_state()?;
        let lookahead = error.token.as_ref().map(|(t, _)| *t);

        let items = parser.tables.state_items(current_state);
        let mut partial_rule: Option<(u16, RuleIdx<_>)> = None;
        let mut entire_rule = false;
        for item in items {
            let rhs_len = item.get_rhs_len(grammar);
            let r_lookahead = item.lookahead;
            let index = item.index;
            if lookahead == r_lookahead {
                entire_rule |= rhs_len == index;
                partial_rule = Some((index, item.rule_idx));
                break;
            }
        }
        partial_rule
            .map(|(rhs_size, rule_index)| (parser.reduce(rhs_size, rule_index), entire_rule))
    }
}

impl<'remap, Tokenizer, Tables, Silent, Remap> Strategy<'remap, Tokenizer, Tables, Silent, Remap>
    for AlwaysReduce2
where
    Tokenizer: Iterator<Item = (TokenKind<Tables::Token>, Span)>,
    Tables: TransitionTable,
    Silent: FnMut(Tables::Node) -> bool,
    Remap: Fn(Tables::Token) -> RemappedToken<'remap, Tables::Token>,
    Tables::Token: Eq + Copy + 'remap,
    Tables::Node: Copy,
{
    /// [`apply`](Self::apply) returns :
    ///
    /// - `None` if parsing is over.
    /// - `Some(result, false)` if a partial rule was applied.
    /// - `Some(result, true)` if a complete rule was applied.
    type Result = Option<(ReduceItem<Tables::Node>, bool)>;

    fn apply<G: Grammar<Token = Tables::Token, Node = Tables::Node>>(
        parser: &mut LR1Parser<Tokenizer, Tables, Silent, Remap>,
        error: &IncorrectToken<Tables::Token, Tables::State>,
        grammar: &G,
    ) -> Self::Result {
        let current_state = parser.get_current_state()?;
        let lookahead = error.token.as_ref().map(|(t, _)| *t);

        let items = parser.tables.state_items(current_state);
        let mut partial_rule: Option<(u16, RuleIdx<_>)> = None;
        let mut entire_rule = false;
        for item in items {
            let rhs_len = item.get_rhs_len(grammar);
            let r_lookahead = item.lookahead;
            let index = item.index;
            if lookahead == r_lookahead {
                entire_rule |= rhs_len == index;
                partial_rule = Some((index, item.rule_idx));
                break;
            }
        }
        if partial_rule.is_none() {
            // ignore lookahead this time.
            if let Some(indexed_rule) = items.first() {
                let rhs_len = indexed_rule.get_rhs_len(grammar);
                let index = indexed_rule.index;
                entire_rule |= rhs_len == index;
                partial_rule = Some((index, indexed_rule.rule_idx));
            }
        }
        partial_rule
            .map(|(rhs_size, rule_index)| (parser.reduce(rhs_size, rule_index), entire_rule))
    }
}

impl<'remap, Tokenizer, Tables, Silent, Remap> Strategy<'remap, Tokenizer, Tables, Silent, Remap>
    for RecursiveReduce
where
    Tokenizer: Iterator<Item = (TokenKind<Tables::Token>, Span)>,
    Tables: TransitionTable,
    Silent: FnMut(Tables::Node) -> bool,
    Remap: Fn(Tables::Token) -> RemappedToken<'remap, Tables::Token>,
    Tables::Token: Eq + Copy + 'remap,
    Tables::Node: Eq + Copy,
{
    /// [`apply`](Self::apply) returns :
    ///
    /// - `None` if no rule could be unambiguously applied.
    /// - `Some(result, false)` if a partial rule was applied.
    /// - `Some(result, true)` if a complete rule was applied.
    type Result = Option<(ReduceItem<Tables::Node>, bool)>;

    fn apply<G: Grammar<Token = Tables::Token, Node = Tables::Node>>(
        parser: &mut LR1Parser<Tokenizer, Tables, Silent, Remap>,
        error: &IncorrectToken<Tables::Token, Tables::State>,
        grammar: &G,
    ) -> Self::Result {
        let mut current_state = parser.get_current_state()?;
        let lookahead = error.token.as_ref().map(|(t, _)| *t);
        let mut stack_index = parser.stack.len() as u16 - 1;
        let original_current_state = current_state;

        loop {
            let mut found_lookahead = false;
            let partial_rule = {
                let items = parser.tables.state_items(current_state);
                let mut partial_rule: Option<(u16, Tables::Node)> = None;
                for item in items {
                    let r_lookahead = item.lookahead;
                    let index = item.index;
                    if lookahead == r_lookahead {
                        found_lookahead = true;
                    }
                    if let Some((size, node)) = partial_rule {
                        if node != item.rule_idx.lhs || size != index {
                            return None;
                        }
                    } else {
                        partial_rule = Some((index, item.rule_idx.lhs));
                    }
                }
                partial_rule
            };
            if let Some((size, node)) = partial_rule {
                if found_lookahead {
                    break;
                }
                stack_index -= size;
                let state = match parser.stack.get(stack_index as usize) {
                    Some(StackElement { state, .. }) => *state,
                    None => return None,
                };
                stack_index += 1;
                match parser.tables.goto(state, node) {
                    Some(state) => {
                        current_state = state;
                    }
                    None => return None,
                }
            } else {
                return None;
            }
        }

        let items = parser.tables.state_items(original_current_state);
        let mut partial_rule: Option<(u16, RuleIdx<_>)> = None;
        let mut entire_rule = false;
        for item in items {
            let rhs_len = item.get_rhs_len(grammar);
            let index = item.index;
            entire_rule |= rhs_len == index;
            if let Some((size, rule_index)) = partial_rule {
                if rule_index != item.rule_idx || size != index {
                    return None;
                }
            } else {
                partial_rule = Some((index, item.rule_idx));
            }
        }
        partial_rule
            .map(|(rhs_size, rule_index)| (parser.reduce(rhs_size, rule_index), entire_rule))
    }
}

impl<'remap, Tokenizer, Tables, Silent, Remap> Strategy<'remap, Tokenizer, Tables, Silent, Remap>
    for CompleteRule
where
    Tokenizer: Iterator<Item = (TokenKind<Tables::Token>, Span)>,
    Tables: TransitionTable,
    Silent: FnMut(Tables::Node) -> bool,
    Remap: Fn(Tables::Token) -> RemappedToken<'remap, Tables::Token>,
    Tables::Node: Eq + Copy,
    Tables::Token: Copy + 'remap,
{
    /// [`apply`](Self::apply) returns :
    ///
    /// - `Some(result)` if a reduction was applied.
    /// - `None` else.
    type Result = Option<ReduceItem<Tables::Node>>;

    fn apply<G: Grammar<Token = Tables::Token, Node = Tables::Node>>(
        parser: &mut LR1Parser<Tokenizer, Tables, Silent, Remap>,
        _error: &IncorrectToken<Tables::Token, Tables::State>,
        grammar: &G,
    ) -> Self::Result {
        let current_state = parser.get_current_state()?;
        let items = parser.tables.state_items(current_state);
        let mut stored: Option<(u16, Option<RuleIdx<_>>)> = None;
        for item in items {
            let index = item.index;
            let rule_idx = Some(item.rule_idx);
            if let Some((stored_index, stored_idx)) = stored {
                if stored_index != index || stored_idx != rule_idx {
                    return None;
                }
            } else {
                let rhs_len = item.get_rhs_len(grammar);
                if index != rhs_len {
                    return None;
                }
                stored = Some((index, rule_idx));
            }
        }

        if let Some((index, rule_idx)) = stored {
            rule_idx.map(|rule_idx| parser.reduce(index, rule_idx))
        } else {
            None
        }
    }
}
