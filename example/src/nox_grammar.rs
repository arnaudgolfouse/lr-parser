use canonical_lr::CanonicalLR;
use logos::Logos;
use lr_parser::{
    compute::{ComputeLR1States as _, TransitionTable},
    make_grammar,
    parser::{RemappedToken, TokenKind},
    syntax_tree::SyntaxElement,
    text_ref::{Span, TextRef},
    LR1Parser,
};
use std::fmt::{self, Display};

/// Wrapper around a [`logos`] lexer.
///
/// This is an iterator that return `(T, Span, TokenKind)`, where the `Span` is
/// the span of the token in the source code.
pub struct Tokenizer<'source, T: logos::Logos<'source>> {
    lexer: logos::Lexer<'source, T>,
}

impl<'source, T: logos::Logos<'source>> Tokenizer<'source, T>
where
    T::Extras: Default,
{
    pub fn new(source: &'source T::Source) -> Self {
        Self {
            lexer: T::lexer(source),
        }
    }
}

impl<'source, T: logos::Logos<'source>> Iterator for Tokenizer<'source, T> {
    type Item = (TokenKind<T>, Span);

    fn next(&mut self) -> Option<Self::Item> {
        let next_token = self.lexer.next()?;
        let span = self.lexer.span();
        Some((TokenKind::Normal(next_token), span))
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Node {
    Start,
    Statements,
    Statement,
    ReturnStm,
    GlobalStm,
    IfStm,
    WhileStm,
    ForStm,
    FunctionStm,
    BreakStm,
    ContinueStm,
    Expr,
    ExprTail,
    FnArgs,
    PrimaryExpr,
    BinaryOp,
    UnaryOp,
    Assign,
    CallArgs,
    Constant,
    ParentExpr,
    LambdaExpr,
    AssignableExpr,
}

impl Node {
    fn skip(self) -> bool {
        matches!(self, Self::Statements)
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Logos)]
#[allow(non_camel_case_types)]
pub enum Token {
    #[token("and")]
    And,
    #[token("break")]
    Break,
    #[token("continue")]
    Continue,
    #[token("do")]
    Do,
    #[token("else")]
    Else,
    #[token("end")]
    End,
    #[token("false")]
    False,
    #[token("fn")]
    Fn,
    #[token("for")]
    For,
    #[token("global")]
    Global,
    #[token("if")]
    If,
    #[token("in")]
    In,
    #[token("nil")]
    Nil,
    #[token("not")]
    Not,
    #[token("or")]
    Or,
    #[token("return")]
    Return,
    #[token("then")]
    Then,
    #[token("true")]
    True,
    #[token("while")]
    While,
    #[token("xor")]
    Xor,
    #[token("(")]
    LPar,
    #[token(")")]
    RPar,
    #[token("[")]
    LBracket,
    #[token("]")]
    RBracket,
    #[token("{")]
    LBrace,
    #[token("}")]
    RBrace,
    #[token(".")]
    Dot,
    #[token(",")]
    Comma,
    #[token(";")]
    SemiColon,
    #[token("!")]
    Bang,
    #[token("?")]
    Question,
    #[token("=")]
    Equal,
    #[token("+=")]
    AddEqual,
    #[token("-=")]
    SubEqual,
    #[token("*=")]
    MulEqual,
    #[token("/=")]
    DivEqual,
    #[token("_")]
    Placeholder,
    #[token("==")]
    EqualEqual,
    #[token("!=")]
    NEqual,
    #[token("<")]
    Less,
    #[token("<=")]
    LessEq,
    #[token(">")]
    More,
    #[token(">=")]
    MoreEq,
    #[token("+")]
    Plus,
    #[token("-")]
    Minus,
    #[token("*")]
    Multiply,
    #[token("/")]
    Divide,
    #[token("%")]
    Modulo,
    #[token("^")]
    Pow,
    #[token("<<")]
    ShiftLeft,
    #[token(">>")]
    ShiftRight,

    #[regex("[0-9][0-9a-zA-Z_]*")]
    Int,
    #[regex(r"[0-9][0-9_]*\.[0-9_]*")]
    Float,
    // this complicated regex matches string with " and ' delimiters, and containing \" or \'.
    #[regex(r#"("(?:[^"\\]|\\.)*"|'(?:[^'\\]|\\.)*')"#)]
    String,
    #[regex("[a-zA-Z_][0-9a-zA-Z_]*")]
    Id,

    #[error]
    #[regex(r"[\s]+", logos::skip)]
    Error,
}

impl Display for Node {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        fmt::Debug::fmt(self, formatter)
    }
}

impl Display for Token {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        fmt::Debug::fmt(self, formatter)
    }
}

use Node::*;
use Token::*;

pub fn main() {
    let grammar = make_grammar!(
        Start -> { N(Statements) }
        Statements -> { }
            | { N(Statement) N(Statements) }
        Statement -> { N(ReturnStm) SemiColon }
            | { N(GlobalStm) SemiColon }
            | { N(IfStm) }
            | { N(WhileStm) }
            | { N(ForStm) }
            | { N(FunctionStm) }
            | { N(BreakStm) SemiColon }
            | { N(ContinueStm) SemiColon }
            | { N(AssignableExpr) N(Assign) N(Expr) SemiColon }
            | { N(Expr) SemiColon }
        ReturnStm -> { Return N(Expr) }
        GlobalStm -> { Global Id }
        IfStm -> { If N(Expr) Then N(Statements) End }
            | { If N(Expr) Then N(Statements) Else N(Statements) End }
        WhileStm -> { While N(Expr) Do N(Statements) End }
        ForStm -> { For Id In N(Expr) Do N(Statements) End }
        FunctionStm -> { Fn Id LPar N(FnArgs) RPar N(Statements) End }
        FnArgs -> { } | { Id } | { Id Comma N(FnArgs) }
        BreakStm -> { Break }
        ContinueStm -> { Continue }
        Expr -> { N(PrimaryExpr) } | { N(Expr) N(ExprTail) }
        ExprTail -> { N(BinaryOp) N(PrimaryExpr) }
            | { Dot Id }
            | { LBracket N(Expr) RBracket }
            | { LPar N(CallArgs) RPar }
        PrimaryExpr -> { N(Constant) }
            | { N(UnaryOp) N(PrimaryExpr) }
            | { Int }
            | { Float }
            | { String }
            | { N(ParentExpr) }
            | { N(LambdaExpr) }
            | { Id }
        Constant -> { True } | { False } | { Nil }
        BinaryOp -> { EqualEqual }
            | { NEqual }
            | { Less }
            | { LessEq }
            | { More }
            | { MoreEq }
            | { Plus }
            | { Minus }
            | { Multiply }
            | { Divide }
            | { Modulo }
            | { Pow }
            | { And }
            | { Or }
            | { Xor }
        UnaryOp -> { Minus } | { Not }
        Assign -> { Equal } | { AddEqual } | { SubEqual } | { MulEqual } | { DivEqual }
        ParentExpr -> { LPar N(Expr) RPar }
        LambdaExpr -> { Fn LPar N(FnArgs) RPar N(Statements) End }
        CallArgs -> { } | { N(Expr) } | { N(Expr) Comma N(CallArgs) }
        AssignableExpr -> { Id }
            | { N(Expr) Dot Id }
            | { N(Expr) LBracket N(Expr) RBracket }
    );

    let parse_table = CanonicalLR::new().compute_states(&grammar).unwrap();
    let start_state = parse_table.starting_state(Node::Start).unwrap();

    println!(
        "the parse table contains {} states",
        parse_table.states().len()
    );

    let source = TextRef::new_full(
        r#"
x = 5;
fn f(a)
    b = g(a);
    x += b;
end
"#,
    );
    let mut parser = LR1Parser::new(
        &parse_table,
        start_state,
        Tokenizer::new(source.as_ref()),
        Node::skip,
        |_| RemappedToken::None,
    );
    let (syntax_tree, errors) =
        SyntaxElement::from_parser(source.clone(), &mut parser, &grammar, Node::Start);
    println!("TREE 1 :");
    for error in errors {
        println!("  ERROR: {:?}", error);
    }
    println!(
        "{}",
        syntax_tree.display(
            &|f, node| std::fmt::Display::fmt(&node.lhs, f),
            &|f, token| std::fmt::Display::fmt(token, f)
        )
    );
    println!();

    let source = TextRef::new_full(
        r#"
x = 5;
fn f(a)
    b = g(a);
    x += b * 5 + x * 1;
"#,
    ); // note the missing 'end'
    let mut parser = LR1Parser::new(
        &parse_table,
        start_state,
        Tokenizer::new(source.as_ref()),
        Node::skip,
        |_| RemappedToken::None,
    );
    let (syntax_tree, errors) =
        SyntaxElement::from_parser(source.clone(), &mut parser, &grammar, Node::Start);
    println!("TREE 2 :");
    for error in errors {
        println!("  ERROR: {:?}", error);
    }
    println!(
        "{}",
        syntax_tree.display(
            &|f, node| std::fmt::Display::fmt(&node.lhs, f),
            &|f, token| std::fmt::Display::fmt(token, f)
        )
    );
    println!();
}

#[test]
fn nox_grammar() {
    main()
}
