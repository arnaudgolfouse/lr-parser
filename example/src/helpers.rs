//! Common functions

use lr_parser::grammar::Symbol;
use std::fmt;

/// Print the right-hand of a rule, with the `index` of the currently parsed symbol.
///
/// This will have a leading ' '.
pub(super) fn print_rule_rhs<T, N>(rhs: &[Symbol<T, N>], index: u16)
where
    T: fmt::Display,
    N: fmt::Display,
{
    for (i, s) in rhs.iter().enumerate() {
        if index as usize == i {
            print!("⋅")
        } else {
            print!(" ")
        }
        match s {
            Symbol::Token(t) => print!("{}", t),
            Symbol::Node(n) => print!("{}", n),
        }
    }
    if index as usize == rhs.len() {
        print!("⋅");
    }
}

pub mod conflicts {
    use canonical_lr::{ComputeError, ComputeErrorKind, Conflict};
    use lr_parser::grammar::{
        ConflictSolution, ConflictingAction, Grammar, Lookahead, RuleIdx, Symbol,
    };

    pub struct ComputeErrorDisplay<'grammar, PrintToken, PrintNode, PrintRuleLhs, G> {
        print_token: PrintToken,
        print_node: PrintNode,
        print_rule_lhs: PrintRuleLhs,
        grammar: &'grammar G,
    }

    impl<'grammar, T, N, PrintToken, PrintNode, PrintRuleLhs, G>
        ComputeErrorDisplay<'grammar, PrintToken, PrintNode, PrintRuleLhs, G>
    where
        T: Copy + PartialEq,
        N: Copy,
        PrintToken: for<'a> Fn(&'a mut String, T),
        PrintNode: for<'a> Fn(&'a mut String, N),
        PrintRuleLhs: for<'a> Fn(&'a mut String, RuleIdx<N>),
        G: Grammar<Token = T, Node = N>,
    {
        pub fn new(
            print_token: PrintToken,
            print_node: PrintNode,
            print_rule_lhs: PrintRuleLhs,
            grammar: &'grammar G,
        ) -> Self {
            Self {
                print_token,
                print_node,
                print_rule_lhs,
                grammar,
            }
        }

        pub fn print_compute_error(
            &self,
            buffer: &mut String,
            grammar: &G,
            error: ComputeError<T, N>,
        ) {
            match &error.kind {
                ComputeErrorKind::CyclicConflictResolution {
                    state: _,
                    solutions,
                } => {
                    buffer.push_str("The following solutions contradict each other:\n");
                    for sol_index in solutions {
                        if let Some(solution) = grammar.conflict_solutions().get(*sol_index) {
                            buffer.push_str("- ");
                            self.print_solution(buffer, solution);
                            buffer.push('\n');
                        }
                    }
                }
                ComputeErrorKind::Conflict(conflict) => {
                    self.print_conflict(buffer, &error, conflict)
                }
                ComputeErrorKind::TooManyStates(max) => {
                    buffer.push_str(&format!("too many states computed : maximum is {}\n", max));
                }
            }
        }

        /// Print a [`Conflict`]
        fn print_conflict(
            &self,
            buffer: &mut String,
            error: &ComputeError<T, N>,
            conflict: &Conflict<T, N>,
        ) {
            buffer.push_str("conflict detected !\n");
            buffer.push_str(&format!(
                "  - type : {}\n",
                match conflict {
                    Conflict::ShiftReduce { .. } => "shift/reduce",
                    Conflict::ReduceReduce { .. } => "reduce/reduce",
                }
            ));

            match conflict {
                Conflict::ShiftReduce {
                    letter,
                    state,
                    reduce,
                } => {
                    self.print_shift(buffer, error, *letter, *state);
                    buffer.push('\n');
                    self.print_reduce(buffer, *reduce, Lookahead::Token(*letter));
                }
                Conflict::ReduceReduce {
                    reduce1,
                    reduce2,
                    lookahead,
                } => {
                    self.print_reduce(buffer, *reduce1, *lookahead);
                    buffer.push('\n');
                    self.print_reduce(buffer, *reduce2, *lookahead);
                }
            }
            buffer.push('\n');
        }

        fn print_shift(
            &self,
            buffer: &mut String,
            error: &canonical_lr::ComputeError<T, N>,
            letter: T,
            state: usize,
        ) {
            for rule in error.shifts(self.grammar, state, letter).unwrap() {
                buffer.push_str("\nShift: ");
                (self.print_rule_lhs)(buffer, rule.rule_idx);
                buffer.push_str(" →");
                let rhs = self.grammar.get_rhs(rule.rule_idx).unwrap();
                for (idx, symbol) in rhs.iter().enumerate() {
                    if rule.index as usize == idx {
                        buffer.push('⋅')
                    } else {
                        buffer.push(' ')
                    }
                    match *symbol {
                        Symbol::Token(t) => (self.print_token)(buffer, t),
                        Symbol::Node(n) => (self.print_node)(buffer, n),
                    }
                }
                buffer.push_str("\n    LOOKAHEAD = ");
                match rule.lookahead {
                    Lookahead::Eof => buffer.push_str("EOF"),
                    Lookahead::Token(token) => (self.print_token)(buffer, token),
                }
            }
        }

        fn print_reduce(&self, buffer: &mut String, rule_idx: RuleIdx<N>, lookahead: Lookahead<T>) {
            buffer.push('\n');
            let rhs = self.grammar.get_rhs(rule_idx).unwrap();
            buffer.push_str("Reduce: ");
            (self.print_rule_lhs)(buffer, rule_idx);
            buffer.push_str(" →");
            for symbol in rhs {
                buffer.push(' ');
                match *symbol {
                    Symbol::Token(t) => (self.print_token)(buffer, t),
                    Symbol::Node(n) => (self.print_node)(buffer, n),
                }
            }
            buffer.push_str("\n    LOOKAHEAD = ");
            match lookahead {
                Lookahead::Eof => buffer.push_str("EOF"),
                Lookahead::Token(token) => (self.print_token)(buffer, token),
            }
        }

        fn print_solution(&self, buffer: &mut String, solution: &ConflictSolution<T, N>) {
            buffer.push_str("prefer: ");
            self.print_conflicting_action(buffer, &solution.prefer);
            buffer.push('\n');
            buffer.push_str("  over: ");
            self.print_conflicting_action(buffer, &solution.over);
            buffer.push('\n');
            if let Some(lookahead) = solution.lookahead {
                buffer.push_str("  lookahead: ");
                (self.print_token)(buffer, lookahead);
                buffer.push('\n');
            }
        }

        fn print_conflicting_action(&self, buffer: &mut String, action: &ConflictingAction<T, N>) {
            match action {
                ConflictingAction::Shift(token) => {
                    buffer.push_str("shift(");
                    (self.print_token)(buffer, *token);
                    buffer.push(')');
                }
                ConflictingAction::ShiftAny => buffer.push_str("shift(_)"),
                ConflictingAction::Reduce(rule_idx) => {
                    buffer.push_str("reduce(");
                    (self.print_rule_lhs)(buffer, *rule_idx);
                    buffer.push(')');
                }
                ConflictingAction::ReduceNode(node) => {
                    buffer.push_str("reduce(");
                    (self.print_node)(buffer, *node);
                    buffer.push(')');
                }
            }
        }
    }
}
