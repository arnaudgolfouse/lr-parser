use canonical_lr::{CanonicalLR, LR1Table};
use logos::Logos;
use lr_parser::{
    compute::{ComputeLR1States as _, TransitionTable},
    make_grammar,
    parser::{ParseAction, RemappedToken, TokenKind},
    text_ref::Span,
    LR1Parser,
};
use std::fmt;

#[derive(Clone, Copy, Debug, Ord, PartialOrd, Eq, PartialEq, Hash, logos::Logos)]
enum Token {
    #[token("->")]
    Arrow,
    #[token("*")]
    Star,
    #[token("+")]
    Plus,
    #[token("?")]
    Question,
    #[token("(")]
    LPar,
    #[token(")")]
    RPar,
    #[token("{")]
    LBrace,
    #[token("}")]
    RBrace,
    #[token("|")]
    Bar,
    #[regex(r"[a-zA-Z]+")]
    Id,
    #[regex(r#""(?:[^"\\]|\\.)*""#)]
    String,
    #[regex("//[^\n]*")]
    Comment,
    #[error]
    #[regex(r"[\s]+", logos::skip)]
    Error,
}

#[derive(Clone, Copy, Debug, Ord, PartialOrd, Eq, PartialEq, Hash)]
enum Node {
    Start,
    RuleStar,
    Rule,
    Rhs,
    BarBlockStar,
    BarBlock,
    Block,
    Items,
    ItemModifierOptStar,
    ModifierOpt,
    Item,
    Modifier,
}

impl Node {
    fn silent(self) -> bool {
        matches!(
            self,
            Self::RuleStar
                | Self::BarBlockStar
                | Self::BarBlock
                | Self::ItemModifierOptStar
                | Self::ModifierOpt
        )
    }
}

impl fmt::Display for Token {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Debug::fmt(self, f)
    }
}

impl fmt::Display for Node {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Debug::fmt(self, f)
    }
}

fn get_parse_table() -> LR1Table<Token, Node> {
    use Node::*;
    use Token::*;

    let grammar = make_grammar!(
        Start -> { N(RuleStar) }
        RuleStar -> { N(Rule) N(RuleStar) } | {}
        Rule -> { Id Arrow N(Rhs) }
        Rhs -> { N(Block) N(BarBlockStar) }
        BarBlockStar ->  { N(BarBlock) N(BarBlockStar) } | {}
        BarBlock -> { Bar N(Block) }
        Block -> { LBrace N(Items) RBrace }
        Items -> { N(ItemModifierOptStar) }
        ItemModifierOptStar -> { N(Item) N(ModifierOpt) N(ItemModifierOptStar) } | {}
        ModifierOpt -> { N(Modifier) } | {}
        Item -> { Id } | { String } | { LPar N(Items) RPar }
        Modifier -> { Star } | { Plus } | { Question }
    );

    CanonicalLR::new().compute_states(&grammar).unwrap()
}

pub fn main() {
    let source = Token::lexer(
        r#"
Start -> { Rule* }
Rule -> { Id "->" Rhs } // comment
Rhs -> { Block ("|" Block)* }
Block -> { "{" Items "}" }
Items -> { (Item Modifier?)* }
Item -> { Id } | { "(" Items ")" }
Modifier -> { "*" } | { "+" } | { "?" }
    "#,
    );

    let table = get_parse_table();
    let start_state = table.starting_state(Node::Start).unwrap();
    let parser = LR1Parser::new(
        &table,
        start_state,
        source.map(|t| {
            (
                match t {
                    Token::Comment => TokenKind::Skip(t),
                    _ => TokenKind::Normal(t),
                },
                Span::default(),
            )
        }),
        Node::silent,
        |_| RemappedToken::None,
    );
    for res in parser {
        match res.unwrap() {
            ParseAction::Shift { kind, .. } => println!("Shift({})", kind),
            ParseAction::Skip { kind, .. } => println!("Skip({})", kind),
            ParseAction::Reduce {
                rule_index,
                silent_size,
                ..
            } => println!("Reduce({}, {})", rule_index.lhs, silent_size),
            _ => {}
        }
    }
}
