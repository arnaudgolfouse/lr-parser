use canonical_lr::CanonicalLR;
use lr_parser::{compute::ComputeLR1States as _, make_grammar};
use std::fmt::{self, Display};

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum N {
    Start,
    Sum,
    Prod,
    Value,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[allow(non_camel_case_types)]
pub enum T {
    plus,
    times,
    int,
    id,
}

impl Display for N {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        fmt::Debug::fmt(self, formatter)
    }
}

impl Display for T {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str(match self {
            T::plus => "+",
            T::times => "×",
            T::int => "int",
            T::id => "id",
        })
    }
}

pub fn main() {
    use N::*;
    use T::*;

    is_conflict();

    let grammar = make_grammar!(
        Start -> { N(Value) }
        Value -> { N(Value) plus N(Value) }
               | { N(Value) times N(Value) }
               | { id }
               | { int }
    );

    let err = CanonicalLR::new().compute_states(&grammar).unwrap_err();
    // shift/reduce
    let mut message = String::new();
    crate::helpers::conflicts::ComputeErrorDisplay::new(
        |buffer, t| buffer.push_str(&t.to_string()),
        |buffer, n| buffer.push_str(&n.to_string()),
        |buffer, rule_idx| buffer.push_str(&rule_idx.lhs.to_string()),
        &grammar,
    )
    .print_compute_error(&mut message, &grammar, err);
    println!("{}", message);

    let grammar = make_grammar!(
        Start -> { N(Value) }
        Value -> { N(Sum) plus N(Value) }
               | { N(Prod) times N(Value) }
               | { id }
               | { int }
        Sum -> { N(Prod) }
        Prod -> { N(Value) }
    );

    let err = CanonicalLR::new().compute_states(&grammar).unwrap_err();
    // reduce/reduce
    let mut message = String::new();
    crate::helpers::conflicts::ComputeErrorDisplay::new(
        |buffer, t| buffer.push_str(&t.to_string()),
        |buffer, n| buffer.push_str(&n.to_string()),
        |buffer, rule_idx| buffer.push_str(&rule_idx.lhs.to_string()),
        &grammar,
    )
    .print_compute_error(&mut message, &grammar, err);
    println!("{}", message);
}

fn is_conflict() {
    #[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
    #[allow(dead_code)]
    pub enum Node {
        Start,
        PlusSep,
        Repeat,
        Opt,
        Rule,
    }

    #[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
    #[allow(non_camel_case_types, dead_code)]
    pub enum Token {
        a,
        b,
        c,
        f,
    }

    impl Display for Node {
        fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
            fmt::Debug::fmt(self, formatter)
        }
    }

    impl Display for Token {
        fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
            fmt::Debug::fmt(self, formatter)
        }
    }

    use Node::*;
    use Token::*;

    // Start -> Rule (b Rule)*
    // Rule -> a
    let grammar = make_grammar!(
        Start -> { N(Rule) N(Repeat) }
        Rule -> { a }
        Repeat -> { N(Rule) b N(Repeat) } | {}
        Opt -> {} | { N(Rule) }
    );

    if let Err(err) = CanonicalLR::new().compute_states(&grammar) {
        let displayer = crate::helpers::conflicts::ComputeErrorDisplay::new(
            |buffer, t| buffer.push_str(&t.to_string()),
            |buffer, n| buffer.push_str(&n.to_string()),
            |buffer, rule_idx| buffer.push_str(&rule_idx.lhs.to_string()),
            &grammar,
        );
        let mut message = String::new();
        displayer.print_compute_error(&mut message, &grammar, err);
        println!("{}", message);
        println!("======================");
        //panic!("{}", err);
        panic!()
    }
}
