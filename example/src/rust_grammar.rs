//! Incomplete grammar of the Rust Language.
//!
//! This is only intended as a test for the parser generator.

use canonical_lr::CanonicalLR;
use logos::Logos;
use lr_parser::{
    compute::{ComputeLR1States as _, TransitionTable},
    grammar::{ConflictSolution, ConflictingAction, RuntimeGrammar, Symbol},
    make_grammar,
    parser::{RemappedToken, TokenKind},
    syntax_tree::SyntaxElement,
    text_ref::{Span, TextRef},
    LR1Parser,
};
use std::{
    collections::HashMap,
    fmt::{self, Display},
};

/// Wrapper around a [`logos`] lexer.
///
/// This is an iterator that return `(T, Span, TokenKind)`, where the `Span` is
/// the span of the token in the source code.
pub struct Tokenizer<'source, T: logos::Logos<'source>, F: Fn(T) -> TokenKind<T>> {
    lexer: logos::Lexer<'source, T>,
    token_kind: F,
}

impl<'source, T, F> Tokenizer<'source, T, F>
where
    T: logos::Logos<'source>,
    T::Extras: Default,
    F: Fn(T) -> TokenKind<T>,
{
    pub fn new(source: &'source T::Source, token_kind: F) -> Self {
        Self {
            lexer: T::lexer(source),
            token_kind,
        }
    }
}

impl<'source, T, F> Iterator for Tokenizer<'source, T, F>
where
    T: logos::Logos<'source> + Copy,
    F: Fn(T) -> TokenKind<T>,
{
    type Item = (TokenKind<T>, Span);

    fn next(&mut self) -> Option<Self::Item> {
        let next_token = self.lexer.next()?;
        let span = self.lexer.span();
        Some(((self.token_kind)(next_token), span))
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Node {
    /// Start of the grammar
    Start,
    /// Statements at the file's top-level
    TopLevelStatements,
    /// Statement at the file's top-level
    TopLevelStatement,
    /// `mod ...;`
    ModStatement,
    /// `use ...;`
    UseStatement,
    /// UseBlock or UsePath
    UseObject,
    /// `{ mod1::{...}, mod2::{...}, ... }`
    UseBlock,
    /// `module::object`, or `module::{...}`
    UsePath,
    /// UseObject separated by commas.
    UseObjectRepeat,
    /// Function definition
    Function,
    /// Structure definition
    StructDef,
    /// Enumeration definition
    EnumDef,
    /// Type definition
    TypeDef,
    /// Type
    Type,
    /// Optional type annotation
    TypeAnnotationOpt,
    /// Types separated by commas
    TypeList,
    /// visibility modifier (`pub`, `pub(super)`, ...)
    Visibility,
    /// Code block (delimited by `{`, `}`)
    Block,

    /// `[pub] [const] fn <id>(<params>) [-> <Type>]`
    FunctionHead,
    /// Function parameter
    FnParameter,
    /// Function parameter
    FnParameters,
    /// List of function parameters
    FnParameterList,
    /// Function return type
    FnReturnType,
    /// Optional `const` keyword
    ConstOpt,

    /// List of struct members
    StructMemberList,
    /// struct members
    StructMember,

    /// Variants of an enum
    EnumVariants,
    /// Variant of an enum
    EnumVariant,

    /// Statements inside a code block.
    Statements,
    /// Statement inside a code block.
    Statement,
    /// `let ...;`
    Binding,
    /// `x = ...`
    Assignment,
    /// `=`, `+=`, `-=`, ...
    AssignSymbol,
    /// `return ...`
    ReturnStm,

    /// Expression
    Expression,
    /// Optional expression
    ExpressionOpt,
    /// Expression on the left of a binary operator.
    PrimaryExpr,
    /// Binary expression
    BinaryOp,

    /// `if` block
    IfBlock,
    /// Condition for a `if` or `while` block.
    Condition,
    /// `else` block
    ElseBlock,
    /// Optional `else` block
    ElseBlockOpt,
    /// `while` block
    WhileBlock,
    /// `for` block
    ForBlock,
    /// `loop` block
    LoopBlock,

    /// Matchable pattern
    Pattern,
}

impl Node {
    fn skip(self) -> bool {
        matches!(
            self,
            Node::TopLevelStatements
                | Node::TopLevelStatement
                | Node::Visibility
                | Node::ConstOpt
                | Node::FnParameterList
                | Node::StructMemberList
                | Node::Statements
                | Node::Statement
                | Node::AssignSymbol
                | Node::ElseBlockOpt
                | Node::PrimaryExpr
                | Node::ExpressionOpt
                | Node::TypeAnnotationOpt
        )
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Logos)]
#[allow(non_camel_case_types)]
pub enum Token {
    /// `mod`
    #[token("mod")]
    ModKw,
    /// `use`
    #[token("use")]
    UseKw,
    /// `fn`
    #[token("fn")]
    FnKw,
    /// `const`
    #[token("const")]
    ConstKw,
    /// `struct`
    #[token("struct")]
    StructKw,
    /// `enum`
    #[token("enum")]
    EnumKw,
    /// `pub`
    #[token("pub")]
    PubKw,
    /// `crate`
    #[token("crate")]
    CrateKw,
    /// `super`
    #[token("super")]
    SuperKw,
    /// `type`
    #[token("type")]
    TypeKw,
    /// `let`
    #[token("let")]
    LetKw,
    /// `mut`
    #[token("mut")]
    MutKw,
    /// `return`
    #[token("return")]
    ReturnKw,
    /// `if`
    #[token("if")]
    IfKw,
    /// `else`
    #[token("else")]
    ElseKw,
    /// `while`
    #[token("while")]
    WhileKw,
    /// `for`
    #[token("for")]
    ForKw,
    /// `in`
    #[token("in")]
    InKw,
    /// `loop`
    #[token("loop")]
    LoopKw,
    /// `;`
    #[token(";")]
    SemiColon,
    /// `:`
    #[token(":")]
    Colon,
    /// `.`
    #[token(".")]
    Dot,
    /// `,`
    #[token(",")]
    Comma,
    /// `?`
    #[token("?")]
    Question,
    /// `!`
    #[token("!")]
    Bang,
    /// `(`
    #[token("(")]
    LPar,
    /// `)`
    #[token(")")]
    RPar,
    /// `[`
    #[token("[")]
    LBracket,
    /// `]`
    #[token("]")]
    RBracket,
    /// `{`
    #[token("{")]
    LBrace,
    /// `}`
    #[token("}")]
    RBrace,
    /// `=`
    #[token("=")]
    Equal,
    /// `==`
    #[token("==")]
    EqualEqual,
    /// `!=`
    #[token("!=")]
    NotEqual,
    /// `+=`
    #[token("+=")]
    AddEqual,
    /// `-=`
    #[token("-=")]
    SubEqual,
    /// `*=`
    #[token("*=")]
    MulEqual,
    /// `/=`
    #[token("/=")]
    DivEqual,
    /// `<`
    #[token("<")]
    Less,
    /// `>`
    #[token(">")]
    More,
    /// `<=`
    #[token("<=")]
    LessEq,
    /// `>=`
    #[token(">=")]
    MoreEq,
    /// `+`
    #[token("+")]
    Plus,
    /// `-`
    #[token("-")]
    Minus,
    /// `*`
    #[token("*")]
    Star,
    /// `|`
    #[token("|")]
    Bar,
    /// `&`
    #[token("&")]
    Ampersand,
    /// `||`
    #[token("||")]
    LogicOr,
    /// `&&`
    #[token("&&")]
    LogicAnd,
    /// `^`
    #[token("^")]
    Hat,
    /// `/`
    #[token("/")]
    Slash,
    /// `<<`
    #[token("<<")]
    ShiftLeft,
    /// `>>`
    #[token(">>")]
    ShiftRight,
    /// `\`
    #[token("\\")]
    AntiSlash,
    /// `::`
    #[token("::")]
    ColonColon,
    /// `->`
    #[token("->")]
    Arrow,
    /// Identifier
    #[regex("[a-zA-Z_][0-9a-zA-Z_]*")]
    Id,
    /// Integer
    #[regex("[0-9][0-9a-zA-Z_]*")]
    Int,
    /// FLoating-point number
    #[regex("[0-9][0-9_]*\\.[0-9_]*")]
    Float,
    /// String in quotes `"`
    #[regex(r#""(?:[^"\\]|\\.)*""#)]
    String,
    /// Char in quotes `'`
    #[regex(r#"'(?:[^'\\]|\\.)*'"#)]
    Char,
    #[regex("//[^\n]*")]
    Comment,
    #[error]
    #[regex(r"[\s]+", logos::skip)]
    Error,
}

impl Display for Node {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        fmt::Debug::fmt(self, formatter)
    }
}

impl Display for Token {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::ModKw => formatter.write_str("mod"),
            Self::UseKw => formatter.write_str("use"),
            Self::FnKw => formatter.write_str("fn"),
            Self::ConstKw => formatter.write_str("const"),
            Self::StructKw => formatter.write_str("struct"),
            Self::EnumKw => formatter.write_str("enum"),
            Self::PubKw => formatter.write_str("pub"),
            Self::CrateKw => formatter.write_str("crate"),
            Self::SuperKw => formatter.write_str("super"),
            Self::TypeKw => formatter.write_str("type"),
            Self::LetKw => formatter.write_str("let"),
            Self::MutKw => formatter.write_str("mut"),
            Self::ReturnKw => formatter.write_str("return"),
            Self::IfKw => formatter.write_str("if"),
            Self::ElseKw => formatter.write_str("else"),
            Self::WhileKw => formatter.write_str("while"),
            Self::ForKw => formatter.write_str("for"),
            Self::InKw => formatter.write_str("in"),
            Self::LoopKw => formatter.write_str("loop"),
            Self::SemiColon => formatter.write_str(";"),
            Self::Colon => formatter.write_str(":"),
            Self::Dot => formatter.write_str("."),
            Self::Comma => formatter.write_str(","),
            Self::Question => formatter.write_str("?"),
            Self::Bang => formatter.write_str("!"),
            Self::LPar => formatter.write_str("("),
            Self::RPar => formatter.write_str(")"),
            Self::LBracket => formatter.write_str("["),
            Self::RBracket => formatter.write_str("]"),
            Self::LBrace => formatter.write_str("{"),
            Self::RBrace => formatter.write_str("}"),
            Self::Equal => formatter.write_str("="),
            Self::EqualEqual => formatter.write_str("=="),
            Self::NotEqual => formatter.write_str("!="),
            Self::Less => formatter.write_str("<"),
            Self::More => formatter.write_str(">"),
            Self::LessEq => formatter.write_str("<="),
            Self::MoreEq => formatter.write_str(">="),
            Self::Plus => formatter.write_str("+"),
            Self::Minus => formatter.write_str("-"),
            Self::Star => formatter.write_str("*"),
            Self::Slash => formatter.write_str("/"),
            Self::Bar => formatter.write_str("|"),
            Self::Ampersand => formatter.write_str("&"),
            Self::LogicOr => formatter.write_str("||"),
            Self::LogicAnd => formatter.write_str("&&"),
            Self::Hat => formatter.write_str("^"),
            Self::ShiftLeft => formatter.write_str("<<"),
            Self::ShiftRight => formatter.write_str(">>"),
            Self::AntiSlash => formatter.write_str("\\"),
            Self::ColonColon => formatter.write_str(","),
            Self::Arrow => formatter.write_str("->"),
            _ => fmt::Debug::fmt(self, formatter),
        }
    }
}

impl Token {
    fn remap(self) -> RemappedToken<'static, Self> {
        match self {
            // Vec<Vec<_>>
            Self::ShiftLeft => RemappedToken::Split(&[(Self::Less, 1), (Self::Less, 1)]),
            // Uhhh, IDK
            Self::ShiftRight => RemappedToken::Split(&[(Self::More, 1), (Self::More, 1)]),
            // Uhhh, IDK
            Self::MoreEq => RemappedToken::Split(&[(Self::More, 1), (Self::Equal, 1)]),
            // Uhhh, IDK
            Self::NotEqual => RemappedToken::Split(&[(Self::Bang, 1), (Self::Equal, 1)]),
            // let closure = || return 1;
            Self::LogicOr => RemappedToken::Split(&[(Self::Bar, 1), (Self::Bar, 1)]),
            // let ref_ref_x = &&x;
            Self::LogicAnd => RemappedToken::Split(&[(Self::Ampersand, 1), (Self::Ampersand, 1)]),
            _ => RemappedToken::None,
        }
    }

    fn kind(self) -> TokenKind<Self> {
        match self {
            Token::Comment => TokenKind::Skip(self),
            _ => TokenKind::Normal(self),
        }
    }
}

pub fn main() {
    let mut grammar = {
        use Node::*;
        use Token::*;
        make_grammar!(
            Start -> { N(TopLevelStatements) }
            TopLevelStatements -> { N(TopLevelStatement) N(TopLevelStatements) } | {}
            TopLevelStatement -> { N(ModStatement) }
                | { N(UseStatement) }
                | { N(Function) }
                | { N(StructDef) }
                | { N(EnumDef) }

            // type
            TypeDef -> { N(Visibility) TypeKw N(Type) SemiColon }
            TypeAnnotationOpt -> { Colon N(Type) } | {}
            Type -> { Id }
                | { Bang }
                | { LPar N(TypeList) RPar }
                | { Id Less N(TypeList) More }
            TypeList -> { N(Type) Comma N(TypeList) } | { N(Type) } | {}

            // Visibility
            Visibility -> { PubKw }
                | { PubKw LPar CrateKw RPar }
                | { PubKw LPar SuperKw RPar }
                | { PubKw LPar N(UsePath) RPar }
                | {}

            // `use` and `mod`
            ModStatement -> { N(Visibility) ModKw Id SemiColon }
            UseStatement -> { N(Visibility) UseKw N(UseObject) SemiColon }
            UseObject -> { N(UsePath) } | { N(UseBlock) }
            UsePath -> { Id ColonColon N(UseObject) } | { Id }
            UseObjectRepeat ->  { N(UseObject) } | { N(UseObject) Comma } | { N(UseObject) Comma N(UseObjectRepeat) }
            UseBlock -> { LBrace N(UseObjectRepeat) RBrace }

            // function
            Function -> { N(Visibility) N(FunctionHead) N(Block) }
            FunctionHead -> { N(ConstOpt) FnKw Id N(FnParameters) N(FnReturnType) }
            ConstOpt -> { ConstKw } | {}
            FnParameters -> { LPar N(FnParameterList) RPar }
            FnParameterList -> { N(FnParameter) Comma N(FnParameterList) }
                | { N(FnParameter) }
                | {}
            FnParameter -> { Id Colon N(Type) }
            FnReturnType -> { Arrow N(Type) } | {}

            // struct
            StructDef -> { N(Visibility) StructKw Id LBrace N(StructMemberList) RBrace }
            StructMemberList -> { N(StructMember) Comma N(StructMemberList) }
                | { N(StructMember) }
                | {}
            StructMember -> { Id Colon N(Type) }

            // enum
            EnumDef -> { N(Visibility) EnumKw Id LBrace N(EnumVariants) RBrace }
            EnumVariants -> { N(EnumVariant) Comma N(EnumVariants) }
                | { N(EnumVariant) }
                | {}
            EnumVariant -> { Id } | { Id LPar N(TypeList) RPar }

            // block
            Block -> { LBrace N(Statements) RBrace }
            Statements -> { N(Statement) N(Statements) } | {}
            Statement -> { N(Binding) }
                | { N(Assignment) }
                | { N(ReturnStm) }
                | { N(Expression) }
                | { SemiColon }
            Binding -> { LetKw N(Pattern) N(TypeAnnotationOpt) Equal N(Expression) }
            ReturnStm -> { ReturnKw N(Expression) }
            AssignSymbol -> { Equal } | { AddEqual } | { SubEqual } | { MulEqual } | { DivEqual }
            Assignment -> { Id N(AssignSymbol) N(Expression) }

            // expression
            ExpressionOpt -> { N(Expression) } | {}
            Expression -> { N(PrimaryExpr) } | { N(BinaryOp) }
            PrimaryExpr -> { Id } | { String } | { Int } | { Float }
                | { N(Block) }
                | { N(IfBlock) }
                | { N(WhileBlock) }
                | { N(ForBlock) }
                | { N(LoopBlock) }
                | { N(PrimaryExpr) LBracket N(Expression) RBracket }

            // looping blocks
            IfBlock -> { IfKw N(Condition) N(Block) N(ElseBlockOpt) }
            ElseBlockOpt -> { N(ElseBlock) } | {}
            ElseBlock -> { ElseKw N(Block) } | { ElseKw N(IfBlock) }
            Condition -> { N(Expression) } | { LetKw N(Pattern) Equal N(Expression) }

            WhileBlock -> { WhileKw N(Condition) N(Block) }

            ForBlock -> { ForKw N(Pattern) InKw N(Expression) N(Block) }

            LoopBlock -> { LoopKw N(Block) }

            // pattern
            Pattern -> { Id }
        )
    };

    precedence(
        &mut grammar,
        vec![
            (Token::LogicOr, 2),
            (Token::Hat, 2),
            (Token::LogicAnd, 3),
            (Token::EqualEqual, 4),
            (Token::NotEqual, 4),
            (Token::Less, 5),
            (Token::LessEq, 5),
            (Token::More, 5),
            (Token::MoreEq, 5),
            (Token::ShiftLeft, 6),
            (Token::ShiftRight, 6),
            (Token::Plus, 7),
            (Token::Minus, 7),
            (Token::Star, 8),
            (Token::Slash, 8),
        ]
        .into_iter()
        .collect(),
    );

    let parse_table = CanonicalLR::new().compute_states(&grammar).unwrap();
    let start_state = parse_table.starting_state(Node::Start).unwrap();

    println!(
        "the parse table contains {} states",
        parse_table.states().len()
    );

    let source = TextRef::new_full(
        r#"
fn f(x: Vec<i32>) -> i32 {
    let y = {
        x[0] - x[2] + x[1]
    };
    return y;
}

struct S {
    x: i32
}
"#,
    );
    let mut parser = LR1Parser::new(
        &parse_table,
        start_state,
        Tokenizer::new(source.as_ref(), Token::kind),
        Node::skip,
        Token::remap,
    );

    let (syntax_tree, errors) =
        SyntaxElement::from_parser(source.clone(), &mut parser, &grammar, Node::Start);
    println!("TREE 1 :");
    for error in errors {
        println!("  ERROR: {:?}", error);
    }
    println!(
        "{}",
        syntax_tree.display(
            &|f, node| std::fmt::Display::fmt(&node.lhs, f),
            &|f, token| std::fmt::Display::fmt(token, f)
        )
    );
    println!();

    let source = TextRef::new_full(
        r#"
fn foo(

struct S {
    f: f32,
}
"#,
    );
    let mut parser = LR1Parser::new(
        &parse_table,
        start_state,
        Tokenizer::new(source.as_ref(), Token::kind),
        Node::skip,
        Token::remap,
    );
    let (syntax_tree, errors) =
        SyntaxElement::from_parser(source.clone(), &mut parser, &grammar, Node::Start);
    println!("TREE 2 :");
    for error in errors {
        println!("  ERROR: {:?}", error);
    }
    println!(
        "{}",
        syntax_tree.display(
            &|f, node| std::fmt::Display::fmt(&node.lhs, f),
            &|f, token| std::fmt::Display::fmt(token, f)
        )
    );
    println!();

    let source = TextRef::new_full(
        r#"
fn f(x: Vec<Vec<?>>) -> i32 {
    let y = {
        x[0] - x[2] * x[1] << x[6]
    };
    return y + + 3;
}

struct S {
    x: i32
}
"#,
    );
    let mut parser = LR1Parser::new(
        &parse_table,
        start_state,
        Tokenizer::new(source.as_ref(), Token::kind),
        Node::skip,
        Token::remap,
    );

    let (syntax_tree, errors) =
        SyntaxElement::from_parser(source.clone(), &mut parser, &grammar, Node::Start);
    println!("TREE 3 :");
    for error in errors {
        println!("  ERROR: {:?}", error);
    }
    println!(
        "{}",
        syntax_tree.display(
            &|f, node| std::fmt::Display::fmt(&node.lhs, f),
            &|f, token| std::fmt::Display::fmt(token, f)
        )
    );

    println!("START");
    let start_time = std::time::Instant::now();
    let source = r#"
fn f(x: Vec<Vec<u32>>) -> i32 {
    let y = {
        x[0] - x[2] * x[1] << x[6]
    };
    return y + 3;
}

struct S {
    x: i32
}
"#
    .repeat(50);
    let source = TextRef::new_full(&source);
    //for _ in 0..10_000 {
    let mut parser = LR1Parser::new(
        &parse_table,
        start_state,
        Tokenizer::new(source.as_ref(), Token::kind),
        Node::skip,
        Token::remap,
    );

    let _ = SyntaxElement::from_parser(source.clone(), &mut parser, &grammar, Node::Start);
    //}
    let end_time = std::time::Instant::now();
    println!("END: {} seconds", (end_time - start_time).as_secs_f64());
}

fn precedence(grammar: &mut RuntimeGrammar<Token, Node>, precedence_table: HashMap<Token, u32>) {
    let mut rules = HashMap::new();
    for (token, priority) in &precedence_table {
        let rule = grammar
            .add_rule(
                Node::BinaryOp,
                vec![
                    Symbol::Node(Node::Expression),
                    Symbol::Token(*token),
                    Symbol::Node(Node::Expression),
                ],
            )
            .unwrap();
        rules.insert(*token, (rule, *priority));
    }

    for (token, (rule, priority)) in &rules {
        // left-associativity
        grammar.add_conflict_resolution(ConflictSolution {
            prefer: ConflictingAction::Reduce(*rule),
            over: ConflictingAction::Shift(*token),
            lookahead: None,
        });
        for (token2, (_, priority2)) in &rules {
            if token == token2 {
                continue;
            }

            #[allow(clippy::comparison_chain)]
            if *priority > *priority2 {
                grammar.add_conflict_resolution(ConflictSolution {
                    prefer: ConflictingAction::Reduce(*rule),
                    over: ConflictingAction::Shift(*token2),
                    lookahead: None,
                });
            } else if *priority < *priority2 {
                grammar.add_conflict_resolution(ConflictSolution {
                    prefer: ConflictingAction::Shift(*token2),
                    over: ConflictingAction::Reduce(*rule),
                    lookahead: None,
                });
            } else {
                grammar.add_conflict_resolution(ConflictSolution {
                    prefer: ConflictingAction::Reduce(*rule),
                    over: ConflictingAction::Shift(*token2),
                    lookahead: None,
                });
            }
        }
    }
}
