# Theory

## Grammars

This document will not introduce the theory of grammars. If you are unfamiliar with the subject, you can look at the [wikipedia article on formal grammars](https://en.wikipedia.org/wiki/Formal_grammar), or [this](https://suif.stanford.edu/dragonbook/lecture-notes/Stanford-CS143/06-Formal-Grammars.pdf) introduction, based on the famous 'Dragon book'. The [dragon book](<http://www.cs.nthu.edu.tw/~ychung/slides/CSC4180/Alfred%20V.%20Aho,%20Monica%20S.%20Lam,%20Ravi%20Sethi,%20Jeffrey%20D.%20Ullman-Compilers%20-%20Principles,%20Techniques,%20and%20Tools-Pearson_Addison%20Wesley%20(2006).pdf>) in itself is an absolute reference, although it covers stuff way beyond parsing.

We do have conventions to specify however:

- We say **tokens** to mean what is often called _terminal_ in the theory, and **node** to mean _non-terminal_.
- Nodes will usually use `CamelCase`, and tokens `SCREAMING_CASE`.
- We will write grammar rules as follow (taking a pseudo-python grammar for example):

  ```
  Function → DEF_KW Name LPAR Args RPAR FunctionBody
  Statement → Assignment | ReturnStmt
  Assignment → ID EQUAL Expr
  ReturnStmt → RETURN_KW Expr
  ...
  ```

  Note that we write `X → x | y` as a shorthand for `X → x` and `X → y`.

In this context, a **parser** is a program that can read a text and produce a tree structure corresponding to the grammar.

## LR parsers: Shift and reduce

In the LR family of parsers, a parser keep a _stack_ of parsed objects, and issues two types of actions:

- shift: put the next token on the stack, and advance the tokenizer.
- reduce(n, Node): pop the n last parsed objects (tokens or nodes) to create a new Node, and push it on the stack (the tokenizer is unchanged).

In a LR(1) parser (which is the kind of parser in this library), the parser can look at the next token in order to decide what to do next (shift or reduce), without advancing the tokenizer. It keeps an internal state to know what node(s) it is currently parsing.

### Example

Assume we have the following grammar:

```
Start → TOKEN1 Node TOKEN3
Node → TOKEN2
```

Then upon seeing the text `"TOKEN1 TOKEN2 TOKEN3"`, a LR parser should emit the following actions:

- shift
- shift
- reduce(1, Node)
- shift
- reduce(3, Start)

Producing the tree:

```
Start
├ TOKEN1
├ Node
│ └ TOKEN2
└ TOKEN3
```

## LR parsers: Internals

The way the parser knows what action to do next is usually by using

- A stack of internal states,
- A transition table, that maps
  - A pair `(state, TOKEN)` to a `shift(state)`, `reduce(length, Node)` or `accept` action. We call this the _action table_.
  - A pair `(state, Node)` to a state. We call this the _goto table_.

Then, it does the following in loop:

- Let `state` be the state at the top of the stack. Look at the next `TOKEN` (called the `lookahead`), and `ACTION(state, TOKEN)`:
  - If it is `shift(new_state)`, push `new_state` on the stack, and consume the token.
  - If it is `reduce(length, Node)`, pop `length` states from the stack: then combine the `state` at the top of the stack and `Node`, and push `GOTO(state, Node)`.
  - If it is `accept`, the parsing can successfully be stopped.

(Note: if any of the table lookup fail, this is a parsing error)

### Example

For example, using the grammar above, the parser's transition table might look like
(using `S` for `shift`, `R` for `reduce`, and `A` for `accept`):

```
┌───────┬──────────────────────────────────────╥──────────────┐
│       │             action table             ║  goto table  │
├───────┼────────┬──────────┬────────────┬─────╫───────┬──────┤
│ state │ TOKEN1 ∣  TOKEN2  │   TOKEN3   │ EOF ║ Start │ Node │
╞═══════╪════════╪══════════╪════════════╪═════╬═══════╪══════╡
│ 0     │  S(1)  │          │            │     ║       │      │
├───────┼────────┼──────────┼────────────┼─────╫───────┼──────┤
│ 1     │        │   S(2)   │            │     ║       │ 3    │
├───────┼────────┼──────────┼────────────┼─────╫───────┼──────┤
│ 2     │        │          │ R(1, Node) │     ║       │      │
├───────┼────────┼──────────┼────────────┼─────╫───────┼──────┤
│ 3     │        │          │    S(4)    │     ║       │      │
├───────┼────────┼──────────┼────────────┼─────╫───────┼──────┤
│ 4     │        │          │            │  A  ║       │      │
└───────┴────────┴──────────┴────────────┴─────╨───────┴──────┘
```

Then, the parser's execution become:

```
On input "TOKEN1 TOKEN2 TOKEN3":
┌─────────┬───────────┬──────────────────────┐
│ stack   │ lookahead │ action               │
╞═════════╪═══════════╪══════════════════════╡
│ 0       │ TOKEN1    │ S(1)                 │
├─────────┼───────────┼──────────────────────┤
│ 0 1     │ TOKEN2    │ S(2)                 │
├─────────┼───────────┼──────────────────────┤
│ 0 1 2   │ TOKEN3    │ R(1, Node) → GOTO(3) │
├─────────┼───────────┼──────────────────────┤
│ 0 1 3   │ TOKEN3    │ S(4)                 │
├─────────┼───────────┼──────────────────────┤
│ 0 1 3 4 │ EOF       │ A                    │
└─────────┴───────────┴──────────────────────┘
```

## Building the transition table

Now we know what we want: an action + goto table describing the behavior of our
parser! So we need a way to build this. There are multiple things to note:

- Multiple, different transition tables may be suitable for a given grammar
- Sometime a grammar has _no_ suitable transition table. Which grammars exactly are
  suitable depend on the algorithm used to compute the table. Usually these
  algorithms fall into either LR, LALR or SLR. This crates targets LR.
- Two algorithm may accept the same grammars, but still produce different tables.
- None of this has any effect on the behavior of the parser itself: this only
  impacts the building time/size of the tables, and the performances of the parser.
