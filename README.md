# Parser generator

This is an experimental LR(1) parser generator.

## Examples

To run an example, run

```
cargo run -p example -- <example-name>
```

Example grammar:

```groovy
tokens {
	#[into(parse_int, "u64")]
	INT: regex("[0-9]+"),
	PLUS: '+',
	TIMES: '*',
	LPAR: '(',
	RPAR: ')',
	#[skip]
	WHITESPACE: regex(r"[\s]+")

	verbatim r"
fn parse_int(source: &str) -> Result<u64, Range<usize>> {
    source.parse().map_err(|_| 0..source.len())
}
"
}

#[start]
#[binary_op]
node Expression {
	| Int => 0: INT
	| Parent => '(' 0: Expression ')'
	| Add => left: Expression #[infix(1, left)] '+' right: Expression
	| Mul => left: Expression #[infix(2, left)] '*' right: Expression
}
```

generates (approximately, and not implemented at the moment) the following AST structures:

```rust
struct Ast<T> {
   span: Range<usize>,
   item: T,
}

enum Expression {
    Int(Ast<u64>),
    Parent(Ast<Box<Expression>>),
    Add { left: Ast<Box<Expression>>, right: Ast<Box<Expression>> },
    Mul { left: Ast<Box<Expression>>, right: Ast<Box<Expression>> },
}
```
